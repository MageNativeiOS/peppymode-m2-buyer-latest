//
//  Appsetup.swift
//  MageNative Magento Platinum
//
//  Created by Manohar Singh Rawat on 26/05/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import Foundation
struct Settings{
    static var baseUrl = "https://peppymode.com/rest/V1/";
    static var sellerUrl = "https://peppymode.com/";
    static var headerKey = "mobiconnectsecrethash";
    static var messageTopic = "peppymodeios";
    static var themeColor: UIColor{
        if #available(iOS 13.0, *) {
        
          return UIColor { (UITraitCollection) -> UIColor in
            if UITraitCollection.userInterfaceStyle == .dark { return UIColor(hexString: "#000000")! }
            else { return UIColor(hexString: "#000000")! }
          }
        } else {
            return UIColor(hexString: "#000000")!
        }
    } //= "#001A27";
    static var themeTextColor: UIColor{
        if #available(iOS 13.0, *) {
        
          return UIColor { (UITraitCollection) -> UIColor in
            if UITraitCollection.userInterfaceStyle == .dark { return UIColor(hexString: "#FFFFFF")! }
            else { return UIColor(hexString: "#FFFFFF")! }
          }
        } else {
            return UIColor(hexString: "#FFFFFF")!
        }
    } //= "#001A27";//#001A27
    static var secondaryColor: UIColor{
           if #available(iOS 13.0, *) {
           
             return UIColor { (UITraitCollection) -> UIColor in
               if UITraitCollection.userInterfaceStyle == .dark { return UIColor(hexString: "#063852")! }
               else { return UIColor(hexString: "#063852")! }
             }
           } else {
               return UIColor(hexString: "#063852")!
           }
       }
    static var textColor = "#000000";
    static var debugMode = true;
    static var layoutDebug = true;
}
