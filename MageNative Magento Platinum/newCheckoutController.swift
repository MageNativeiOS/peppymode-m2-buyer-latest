//
//  newCheckoutController.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 28/09/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class newCheckoutController: MagenativeUIViewController {
    var checkoutAs = "USER";
    var email_id = "test@test.com";
    
    var shippingMethods = [String: String]();
    var paymentMethods = [String: String]();
    var walletArrayMethod = [String:String]();
    var payment_method = "";
    var shipping_method = "";
    var cart_id = "0";
    var all_pro_qty=[String]()
    var products = [[String: String]]();
    //Native Payments
    private var nativePayments = ["Pay Online (RazorPay)":"Razorpay","CCAvenue (Credit/Debit/Wallet/Netbanking)":"CCAvenue"]//,"PAYTM (Credit/Debit/Wallet/Netbanking)":"Paytm"
    
    //Order data
    var totalOrderData = [String:String]()
    let shippingAndPaymentView = checkoutMethodsView();
    var shippingMethodButtons = [UIButton]();
    var paymentMethodButtons = [UIButton]();
    var walletButton = [UIButton]();
    var partialPayment = false
    var amountLeftForPay = ""
    var checkWallet = false
    var fromWalletPay = false
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // Do any additional setup after loading the view.
        if(defaults.object(forKey: "cartId") != nil){
            cart_id = (defaults.object(forKey: "cartId") as? String)!;
        }else{
            cart_id = "0"
        }
        if(defaults.object(forKey: "cartId") != nil){
            cart_id = (defaults.object(forKey: "cartId") as? String)!;
            self.sendRequest(url:"mobiconnect/checkout/viewcart",params: ["cart_id":cart_id]);
        }
        else{
            self.sendRequest(url:"mobiconnect/checkout/viewcart",params: ["cart_id":cart_id]);
        }
        self.basicFoundationToRenderView(bottomMargin: CGFloat(0.0));
        self.makeRequestToAPI("mobiconnect/checkout/getshippingpayament",dataToPost: ["email":email_id, "Role":checkoutAs, "cart_id":cart_id]);
        //getShiping()
    }
    
    
    func getShiping(){
        //   var semaphore = DispatchSemaphore (value: 0)
        
        let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
        let  customerId = userInfoDict["customerId"]!;
        let hash = userInfoDict["hashKey"]!
        
        let parameters = "{\n  \"parameters\" : {\n    \"hashkey\" : \"\(hash)\",\n    \"customer_id\" : \"\(customerId)\",\n    \"email\" : \"z1@gmail.com\",\n    \"cart_id\" : \"1325\",\n    \"Role\" : \"USER\"\n  }\n}"
        let postData = parameters.data(using: .utf8)
        
        var request = URLRequest(url: URL(string: "https://peppymode.com/rest/V1/mobiconnect/checkout/getshippingpayament")!,timeoutInterval: Double.infinity)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("PHPSESSID=c9c80b635adaf0197e62fd739bc76e6b", forHTTPHeaderField: "Cookie")
        
        request.httpMethod = "POST"
        request.httpBody = postData
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data else {
                print(String(describing: error))
                return
            }
            print(String(data: data, encoding: .utf8)!)
            // semaphore.signal()
        }
        
        task.resume()
        //semaphore.wait()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // self.tabBarController?.tabBar.isHidden = true
        
    }
    override func parseResponseReceivedFromAPI(_ jsonResponse:JSON){
        print("PAYMENT");
        print(jsonResponse);
        let jsonResponse = jsonResponse[0]
        
        
        if(jsonResponse["success"].stringValue == "true")
        {
            
            let shipMethods = jsonResponse["shipping"]["methods"].arrayValue;
            for d in 0..<shipMethods.count{
                let value = shipMethods[d]["value"].stringValue;
                let key = shipMethods[d]["label"].stringValue;
                self.shippingMethods[key] = value;
            }
            
            
            
            let payMethods = jsonResponse["payments"]["methods"]//.arrayValue;
            print(payMethods)
            if payMethods.stringValue.lowercased() != "No Payment Method Availabile.".lowercased() {
                for (_,result) in payMethods{
                    let value = result["value"].stringValue;
                    let key = result["label"].stringValue;
                    
                    
                    if value == "wallet"{
                        for (k,v) in result{
                            self.walletArrayMethod[k] = v.stringValue
                        }
                    }else{
                        self.paymentMethods[key] = value;
                    }
                    
                }
            }
            if nativePayments.count > 0 {
                for (key,val) in nativePayments {
                    
                    self.paymentMethods[key] = val
                    
                }
                
            }
            
            
            
        }
        print(paymentMethods);
        print(shippingMethods);
        self.renderCheckoutMethodsView();
    }
    func renderCheckoutMethodsView(){
        self.makeSomeSpaceInStackView(height: CGFloat(0.0));
        shippingAndPaymentView.translatesAutoresizingMaskIntoConstraints = false;
        stackView.addArrangedSubview(shippingAndPaymentView);
        
        
        
        if(shippingMethods.count == 0)
        {
            shippingAndPaymentView.shippingMethodLbl.isHidden = true;
        }
        else{
            
        }
        
        var heightConstant = 0
        var shippingTags = 3000
        var initialCheck = 0;
        
        for(key,_) in shippingMethods
        {
            let radioButtonView = RadioButtonView()
            
            radioButtonView.radioButton.imageView?.contentMode = .scaleAspectFit
            shippingAndPaymentView.shippingStack.addArrangedSubview(radioButtonView);
            shippingAndPaymentView.shippingMethodHeight.constant += 50;
            radioButtonView.radioButton.tag = shippingTags;
            
            radioButtonView.radioButton.setTitle(key, for: .normal);
            shippingMethodButtons.append(radioButtonView.radioButton);
            radioButtonView.radioButton.isEnabled = true
            radioButtonView.radioButton.addTarget(self, action: #selector(shippingMethodsClicked(_:)), for: .touchUpInside);
            
            radioButtonView.radioButton.setImage(UIImage(named: "uncheckedRadio"), for: .normal);
            if(initialCheck == 0)
            {
                
                radioButtonView.radioButton.setImage(UIImage(named: "checkedRadio"), for: .normal);
                
                shipping_method = key;
            }
            heightConstant += 50;
            shippingTags += 1;
            initialCheck += 1;
        }
        
        
        var paymenttags = 4500
        initialCheck = 0;
        for(key,_) in paymentMethods
        {
            let radioButtonView = RadioButtonView()
            
            radioButtonView.radioButton.imageView?.contentMode = .scaleAspectFit
            shippingAndPaymentView.paymentStack.addArrangedSubview(radioButtonView);
            shippingAndPaymentView.paymentMethodHeight.constant += 50;
            radioButtonView.radioButton.tag = paymenttags;
            
            radioButtonView.radioButton.setTitle(key, for: .normal);
            paymentMethodButtons.append(radioButtonView.radioButton);
            radioButtonView.radioButton.isEnabled = true
            radioButtonView.radioButton.addTarget(self, action: #selector(deliveryMethodsTapped(_:)), for: .touchUpInside);
            
            radioButtonView.radioButton.setImage(UIImage(named: "uncheckedRadio"), for: .normal);
            if(initialCheck == 0)
            {
                
                radioButtonView.radioButton.setImage(UIImage(named: "checkedRadio"), for: .normal);
                
                payment_method = key;
            }
            
            heightConstant += 50;
            paymenttags += 1;
            initialCheck += 1;
            
        }
        
        if walletArrayMethod.count > 0{
        //MARK:-Wallet System:
        let wallettags = 5500
        
        let radioButtonView = RadioButtonView()
        
        radioButtonView.radioButton.imageView?.contentMode = .scaleAspectFit
        
        shippingAndPaymentView.walletView.addArrangedSubview(radioButtonView);
        shippingAndPaymentView.walletHeight.constant += 50;
        radioButtonView.radioButton.tag = wallettags;
        
        radioButtonView.radioButton.setTitle("\(walletArrayMethod["label"] ?? "") (\(walletArrayMethod["amount_in_wallet"] ?? ""))", for: .normal);
        walletButton.append(radioButtonView.radioButton);
        radioButtonView.radioButton.isEnabled = true
        radioButtonView.radioButton.addTarget(self, action: #selector(walletMethodTapped(_:)), for: .touchUpInside);
        
        radioButtonView.radioButton.setImage(UIImage(named: "newUnchecked"), for: .normal);
        //            if(initialCheck == 0)
        //            {
        //
        //                radioButtonView.radioButton.setImage(UIImage(named: "newChecked"), for: .normal);
        //
        //                payment_method = key;
        //            }
        
        heightConstant += 50;
        }
        //Wallet Ends
        var shippingAndPaymentViewHeight = 0.0
        if UIDevice.current.userInterfaceIdiom == .pad{
            shippingAndPaymentViewHeight = Double(translateAccordingToDevice(CGFloat(heightConstant + 00)));
        }else{
            shippingAndPaymentViewHeight = Double(translateAccordingToDevice(CGFloat(heightConstant + 120)));
        }
        shippingAndPaymentView.heightAnchor.constraint(equalToConstant: CGFloat(shippingAndPaymentViewHeight)).isActive = true;
       
        self.setLeadingAndTralingSpaceFormParentView(shippingAndPaymentView,parentView:stackView);
        
        //MARK:-OrderSummarySection
        let orderSummaryView = OrderSummaryView()
        orderSummaryView.translatesAutoresizingMaskIntoConstraints = false;
        var mainViewHeight = 0.0
        
        stackView.addArrangedSubview(orderSummaryView);
        
        orderSummaryView.totalItemLbl.text = "TOTAL ITEM(S) : \(self.totalOrderData["itemsQty"] ?? "")"
        orderSummaryView.amountToPayLbl.text = self.totalOrderData["amounttopay"]
        orderSummaryView.shippingChargesLbl.text = self.totalOrderData["shipping_amount"]
        orderSummaryView.discountLbl.text = self.totalOrderData["discount_amount"]
        orderSummaryView.taxPriceLbl.text = self.totalOrderData["tax_amount"]
        orderSummaryView.totalPrice.text = self.totalOrderData["grandtotal"]
        
        
        for product in products {
            let orderSumProductListView = productListView();
            orderSummaryView.productListingStack.addArrangedSubview(orderSumProductListView);
            orderSummaryView.productListingStackHeight.constant += 150;
            orderSumProductListView.outerView.cardView()
            orderSumProductListView.productName.text = product["product-name"] ?? ""
            if product["product-name"] == "Wallet Pay"{
                self.fromWalletPay = true
            }
            orderSumProductListView.productName.numberOfLines = 2
            orderSumProductListView.subTotalLbl.text = "Subtotal : \(product["sub-total"] ?? "")"
            orderSumProductListView.qtyLbl.text =  "Quantity : \(product["quantity"] ?? "")"
            
            if(product["product_image"] != nil){
                orderSumProductListView.productImage.sd_setImage(with: URL(string: product["product_image"]!), placeholderImage: UIImage(named: "placeholder"))
            }
            
            mainViewHeight += 150.0
        }
        
        orderSummaryView.proceedBtn.addTarget(self, action: #selector(savePaymentData(_:)), for: .touchUpInside)
        let orderSummaryViewHeight = translateAccordingToDevice(CGFloat(mainViewHeight + 300));
        orderSummaryView.heightAnchor.constraint(equalToConstant: orderSummaryViewHeight).isActive = true;
        self.setLeadingAndTralingSpaceFormParentView(orderSummaryView,parentView:stackView);
        
        self.makeSomeSpaceInStackView(height: CGFloat(0.0));
    }
    
    
    @objc func savePaymentData(_ sender:UIButton){
        
        var urlToRequest = "mobiconnect/checkout/saveshippingpayament";
        let requestHeader = cedMage.getInfoPlist(fileName:"cedMage",indexString: "requestheader") as! String
        let baseURL = cedMage.getInfoPlist(fileName:"cedMage",indexString: "cedBaseUrl") as! String;
        urlToRequest = baseURL+urlToRequest;
        if shipping_method == "" && !self.fromWalletPay{
            self.view.makeToast("Select Shipping Method!", duration: 2.0, position: .center)
            return;
        }
        var postString = "";
        var postData = [String:String]()
        postData["cart_id"] = cart_id;
        if self.payment_method.lowercased() == "Pay Online (RazorPay)".lowercased(){
            postData["payment_method"] = "apppayment";
        }else if self.payment_method.lowercased() == "PAYTM (Credit/Debit/Wallet/Netbanking)".lowercased(){
            postData["payment_method"] = paymentMethods[payment_method];
        }else if self.payment_method.lowercased() == "CCAvenue (Credit/Debit/Wallet/Netbanking)".lowercased(){
            postData["payment_method"] =  paymentMethods[payment_method];
        }else if self.payment_method.lowercased() == "wallet"{
            postData["payment_method"] = "wallet"
        }
        else{
            postData["payment_method"] = paymentMethods[payment_method];
        }
        
        if shipping_method != "" {
            postData["shipping_method"] =  shippingMethods[shipping_method];
        }else{
            postData["shipping_method"] = "false"
        }
       
        postData["Role"] = checkoutAs;
        postData["email"] = email_id;
        
        if(defaults.object(forKey: "userInfoDict") != nil){
            postData["hashkey"] = userInfoDict["hashKey"]!
            postData["customer_id"] = userInfoDict["customerId"]!;
        }
        postString = ["parameters":postData].convtToJson() as String
        print(postString);
        var request = URLRequest(url: URL(string: "\(urlToRequest)")!);
        request.httpMethod = "POST";
        request.httpBody = postString.data(using: String.Encoding.utf8);
        request.setValue(requestHeader, forHTTPHeaderField: "Mobiconnectheader")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        cedMageLoaders.addDefaultLoader(me: self);
        let task = URLSession.shared.dataTask(with: request){
            
            // check for fundamental networking error
            data, response, error in
            guard error == nil && data != nil else{
                print("error=\(error)")
                DispatchQueue.main.async{
                    print(error?.localizedDescription as Any);
                    cedMageLoaders.removeLoadingIndicator(me: self);
                }
                return;
            }
            
            // check for http errors
            if let httpStatus = response as? HTTPURLResponse , httpStatus.statusCode != 200{
                
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
                DispatchQueue.main.async{
                    cedMageLoaders.removeLoadingIndicator(me: self);
                }
                return;
            }
            
            // code to fetch values from response :: start
            guard var jsonResponse = try? JSON(data: data!) else{return;}
            jsonResponse = jsonResponse[0]
            if(jsonResponse != nil){
                
                DispatchQueue.main.async{
                    cedMageLoaders.removeLoadingIndicator(me: self);
                    print(jsonResponse);
                    
                    let storyboard = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil);
                    let viewController = storyboard.instantiateViewController(withIdentifier: "paymentView") as! cedMagePayementView;
                    viewController.isOrderReview = true;
                    viewController.selectedPaymentMethod = self.payment_method
                    viewController.orderEmail = self.email_id;
                    if self.partialPayment{
                        viewController.checkWallet = self.checkWallet
                        self.totalOrderData["grandtotal"] = self.amountLeftForPay
                    }
                    viewController.total = self.totalOrderData
                    self.navigationController?.pushViewController(viewController, animated: true);
                    
                }
            }
        }
        
        task.resume();
        
    }
    
    
    
    //custom make order
    override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
        if let data  = data {
            guard var json = try? JSON(data:data)else{return;}
            json = json[0]
            if requestUrl ==  "mobiconnect/checkout/saveorder" {
                if(json["success"].stringValue == "true"){
                    self.defaults.removeObject(forKey: "guestEmail");
                    self.defaults.removeObject(forKey: "cartId");
                    self.defaults.setValue("0", forKey: "items_count")
                    let order_id = json["order_id"].stringValue;
                    
                    let storyboard = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil);
                    let viewController = storyboard.instantiateViewController(withIdentifier: "orderPlacedViewController") as! OrderPlacedViewController;
                    viewController.order_id = order_id;
                    self.navigationController?.viewControllers = [viewController];
                }
            }
            else if requestUrl == "mobiconnect/checkout/viewcart" {
                
                let jsonResponse = json
                print(jsonResponse)
                if(jsonResponse["success"].stringValue.lowercased() == "false".lowercased()){
                    self.defaults.removeObject(forKey: "cartId")
                }
                self.totalOrderData["currency_code"] = jsonResponse["data"]["currency_code"].stringValue
                self.totalOrderData["currency_symbol"] = jsonResponse["data"]["currency_symbol"].stringValue
                self.totalOrderData["amounttopay"] = jsonResponse["data"]["total"][0]["amounttopay"].stringValue;
                self.totalOrderData["tax_amount"] = jsonResponse["data"]["total"][0]["tax_amount"].stringValue;
                self.totalOrderData["shipping_amount"] = jsonResponse["data"]["total"][0]["shipping_amount"].stringValue;
                self.totalOrderData["discount_amount"] = jsonResponse["data"]["total"][0]["discount_amount"].stringValue;
                self.totalOrderData["appliedcoupon"] = jsonResponse["data"]["total"][0]["coupon"].stringValue
                self.totalOrderData["grandtotal"] = jsonResponse["data"]["grandtotal"].stringValue;
                self.totalOrderData["itemsQty"] = jsonResponse["data"]["items_qty"].stringValue;
                print(self.totalOrderData)
                
                for cartProInfo in jsonResponse["data"]["products"].arrayValue{
                    var tempData = [String:String]();
                    
                    tempData["product_type"] = cartProInfo["product_type"].stringValue;
                    tempData["quantity"] = cartProInfo["quantity"].stringValue;
                    let temp=cartProInfo["quantity"].stringValue;
                    all_pro_qty.append(temp)
                    tempData["product-name"] = cartProInfo["product-name"].stringValue;
                    tempData["sub-total"] = cartProInfo["sub-total"].stringValue;
                    tempData["product_image"] = cartProInfo["product_image"].stringValue;
                    tempData["product_id"] = cartProInfo["product_id"].stringValue;
                    tempData["item_id"] = cartProInfo["item_id"].stringValue;
                    
                    
                    
                    self.products.append(tempData);
                }
            }else{
                print(json)
            }
        }
    }
    @objc func walletMethodTapped(_ sender:UIButton){
        let senderTag = sender.tag
        var tags = 5500;
        for index in walletButton
        {
            if(index.currentTitle == sender.currentTitle)
            {
                if sender.currentImage == UIImage(named: "newUnchecked")
                {
                    sender.setImage(UIImage(named: "newChecked"), for: .normal)
                    self.checkWallet = true
                    if walletArrayMethod.keys.contains("amount_left_for_pay"){
                        
                        self.partialPayment = true
                        amountLeftForPay = walletArrayMethod["amount_left_for_pay"] ?? ""
                        for i in paymentMethodButtons {
                            i.isEnabled = true
                        }
                    }else{
                        self.partialPayment = false
                        for i in paymentMethodButtons {
                            i.isEnabled = false
                            i.setImage(UIImage(named:"uncheckedRadio"), for: .normal)
                        }
                        self.payment_method = "wallet"
                    }
                }
                else
                {
                    self.partialPayment = false
                    index.setImage(UIImage(named: "newUnchecked"), for: .normal)
                    for i in paymentMethodButtons {
                        i.isEnabled = true
                    }
                }
            }
            
            tags += 1;
        }
        // print("selectedPaymentMethod = \(payment_method)")
    }
    @objc func deliveryMethodsTapped(_ sender: UIButton)
    {
        let senderTag = sender.tag
        var tags = 4500;
        for index in paymentMethodButtons
        {
            if(index.currentTitle == sender.currentTitle)
            {
                if sender.currentImage == UIImage(named: "uncheckedRadio")
                {
                    sender.setImage(UIImage(named: "checkedRadio"), for: .normal)
                    payment_method = sender.currentTitle!;
                }
            }
            else
            {
                index.setImage(UIImage(named: "uncheckedRadio"), for: .normal)
            }
            tags += 1;
        }
        print("selectedPaymentMethod = \(payment_method)")
    }
    @objc func shippingMethodsClicked(_ sender: UIButton)
    {
        let senderTag = sender.tag
        
        var tags = 3000;
        for index in shippingMethodButtons
        {
            if(index.currentTitle == sender.currentTitle)
            {
                if sender.currentImage == UIImage(named: "uncheckedRadio")
                {
                    sender.setImage(UIImage(named: "checkedRadio"), for: .normal)
                    shipping_method = sender.currentTitle!;
                }
                
            }
            else
            {
                index.setImage(UIImage(named: "uncheckedRadio"), for: .normal)
            }
            tags += 1;
        }
    }
}

