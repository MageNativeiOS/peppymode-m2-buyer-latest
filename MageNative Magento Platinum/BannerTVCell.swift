//
//  BannerTVCell.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 16/06/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class BannerTVCell: UITableViewCell {

    @IBOutlet weak var bannerImage: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle        = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

    var feed: HomepageBannerData??{
        didSet{
            bannerImage.sd_setImage(with: URL(string: feed??.banner_image ?? String()), placeholderImage: UIImage(named: "placeholder"))
        }
    }
}
