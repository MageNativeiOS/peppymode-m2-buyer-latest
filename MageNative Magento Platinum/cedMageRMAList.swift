//
//  cedMageRMAList.swift
//  MageNative Magento Platinum
//
//  Created by CEDCOSS Technologies Private Limited on 04/07/17.
//  Copyright © 2017 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class cedMageRMAList: UITableViewCell {
    @IBOutlet weak var rmaId: UILabel!

    @IBOutlet weak var vendor: UILabel!
    
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var orderId: UILabel!
    
    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var billTo: UILabel!
    
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var deleteBtnHeight: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
