//
//  AppDelegate+Pushnotifications.swift
//  MageNative Magento Platinum
//
//  Created by Manohar Singh Rawat on 04/11/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//
import FirebaseMessaging
import Firebase
extension AppDelegate:UNUserNotificationCenterDelegate, MessagingDelegate {
    func registerForPushNotification(application:UIApplication){
        Messaging.messaging().delegate = self
        if Messaging.messaging().fcmToken != nil {
            Messaging.messaging().subscribe(toTopic: Settings.messageTopic);
        }
        if #available(iOS 10, *) {
            UNUserNotificationCenter.current().requestAuthorization(options:[.badge, .alert, .sound]){ (granted, error) in }
            application.registerForRemoteNotifications()
        }
               UNUserNotificationCenter.current().delegate = self
        
    }
    
    
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        /*let state = UIApplication.shared.applicationState
        let test=userInfo["aps"] as! Dictionary<String,Any>
        let main=test["alert"] as! Dictionary<String,Any>
        if state == .active {
            
            
            let alert=UIAlertController(title: main["title"] as? String, message: main["body"] as? String, preferredStyle: .alert)
            let action=UIAlertAction(title: "Ok".localized, style: .default, handler: { (action: UIAlertAction!) in
                self.openPages(main: main, active: true)
                
            })
            let action_cancel=UIAlertAction(title: "Cancel".localized, style: .destructive, handler: { (action: UIAlertAction!) in
                return
            })
            alert.addAction(action)
            alert.addAction(action_cancel)
            self.window?.rootViewController?.present(alert, animated: true, completion: nil)
            return
        }
        else if state == .background{
            self.openPages(main: main, active: true)
        }
        else if state == .inactive{
            self.openPages(main: main)
        }
        else{
            self.openPages(main: main)
        }*/
        
        
        
        
    }
    
    func openPages(main:Dictionary<String,String>, active: Bool = false){
        print(main)
        //var tempMain=main
        //tempMain.removeValue(forKey: "expired_on")
        UserDefaults.standard.set(main, forKey: "MageNotificationData")
        
        //let notType=main["link_type"]
        //let notification_id=main["notification_id"]
        
        reloadHome()
        return
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .sound])
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {

        Messaging.messaging().subscribe(toTopic: Settings.messageTopic){ error in
            if error == nil{
                print("Subscribed to topic")
            }
            else{
                print("Not Subscribed to topic")
            }
        }
        
        
    }
    
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("hello")
        if response.notification.request.identifier == "magenative_cart_notification_center" {
            let cartSelect = ["link_type":"cart"]
            self.openPages(main: cartSelect)
            
        }
        else{
            let state = UIApplication.shared.applicationState
            //let test=userInfo["aps"] as! Dictionary<String,Any>
            
            //print(response.notification.request.content.userInfo)
            //let userInfo = response.notification.request.content.userInfo
            //    let test=userInfo["gcm.notification.data"] as? Dictionary<String,Any>
            let userInfo = response.notification.request.content.userInfo
            print(userInfo)
            if let linkId = userInfo["link_id"] as? String, let linkType = userInfo["link_type"] as? String{
                if(state == .active){
                    self.openPages(main:["link_id":linkId,"link_type":linkType], active: true)
                }
                else{
                    self.openPages(main:["link_id":linkId,"link_type":linkType])
                }
                
            }
            //let main=userInfo["gcm.notification.data"] as? String
            
            /*if let data = main?.data(using: .utf8) {
              guard let data = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] else {return;}
              
              //let main=test["alert"] as! Dictionary<String,Any>
              if state == .active {
                /*print(data)
                print(userInfo)
                let aps = userInfo["aps"] as? String
                if let apsdata = aps?.data(using: .utf8) {
                    guard let apsdata = try? JSONSerialization.jsonObject(with: apsdata, options: []) as? [String: Any] else {return;}
                    if let alert = apsdata["alert"] as? [String:String]{
                        let title = alert["title"]!
                        let body = alert["body"]!
                        let alert=UIAlertController(title: title, message: body, preferredStyle: .alert)
                          let action=UIAlertAction(title: "Ok".localized, style: .default, handler: { (action: UIAlertAction!) in
                              self.openPages(main:data)
                              
                          })
                          let action_cancel=UIAlertAction(title: "Cancel".localized, style: .destructive, handler: { (action: UIAlertAction!) in
                              return
                          })
                          alert.addAction(action)
                          alert.addAction(action_cancel)
                          self.window?.rootViewController?.present(alert, animated: true, completion: nil)
                          return
                    }
                }*/
                self.openPages(main:data, active: true)
                
              }
              else if state == .background{
                  self.openPages(main: data, active: true)
              }
              else if state == .inactive{
                  self.openPages(main: data)
              }
              else{
                  self.openPages(main: data)
              }
            }*/
        }
        
        completionHandler()
        
    }
}
