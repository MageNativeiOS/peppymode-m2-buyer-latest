//
//  cedMageRMAView.swift
//  MageNative Magento Platinum
//
//  Created by CEDCOSS Technologies Private Limited on 04/07/17.
//  Copyright © 2017 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class cedMageRMAView: MagenativeUIViewController,UITableViewDelegate,UITableViewDataSource {
  
  @IBOutlet weak var rmaId: UILabel!
  @IBOutlet weak var tableView: UITableView!
  
  
  @IBOutlet weak var submitRma: UIButton!
  
  var rmaItemData = [[String:String]]()
  var orderData = [String:String]()
  var customerData=[String:String]()
  var billingData = [String:String]()
  var rma_id = String()
  var orderId = ""
  var reason = [String:String]()
  var packageCondition = [String:String]()
  var resolution = [String:String]()
  
  var reasonStr = ""
  var pk_condition = ""
  var resolutionStr = ""
  var rma_qty = [String]()
  var itemIdArray = [String]()
  
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    tableView.delegate = self
    tableView.dataSource = self
    
    if defaults.bool(forKey: "isLogin") {
      userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
      if let custId = userInfoDict["customerId"] {
        if orderId != ""{
          self.sendRequest(url: "mobiconnect/mobirma/generaterma", params: ["customer_id":custId,"order_id":orderId],store:false)
        }else{
          self.sendRequest(url: "mobiconnect/mobirma/viewrma", params: ["customer_id":custId,"rma_id":rma_id],store:false)
        }
      }
    }
    if orderId == ""{
      submitRma.isHidden = true
    }
    submitRma.setThemeColor()
    submitRma.addTarget(self, action: #selector(setUpRma(_:)), for: .touchUpInside)
    // Do any additional setup after loading the view.
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return 5
  }
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if section==0{
      return rmaItemData.count
    }
    if orderId != "" {
      if section==3{
        return 0
      }
      if section==2{
        return 2
      }
    }
    return 1
  }
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    if indexPath.section==0
    {
      return 200
    }
    else if indexPath.section==1
    {
      return 120
    }
    else if indexPath.section==2
    {
      return 235
    }
    else if indexPath.section==3
    {
      return 120
    }
    else
    {
      return 160
    }
  }
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    if indexPath.section==0
    {
      if let cell = tableView.dequeueReusableCell(withIdentifier: "productInfo") as? ceMageRmaProductView {
        cell.productName.text = rmaItemData[indexPath.row]["name"]
        cell.sku.text = rmaItemData[indexPath.row]["sku"]
        cell.rmaQty.text = rmaItemData[indexPath.row]["rma_qty"]
        cell.orderedQty.text = rmaItemData[indexPath.row]["ordered_qty"]
        cell.price.text = rmaItemData[indexPath.row]["price"]
        return cell
      }
    }
    else if indexPath.section==1
    {
      if let cell = tableView.dequeueReusableCell(withIdentifier: "orderinfo") as? ceMageRmaProductView {
        cell.orderId.text=self.orderData["order_id"]
        cell.orderStatus.text=self.orderData["status"]
        
        return cell
      }
    }
    else if indexPath.section==2
    {
      if indexPath.row == 0{
        if let cell = tableView.dequeueReusableCell(withIdentifier: "shippingaddress") as? ceMageRmaProductView {
          //self.customerData=["customer_name":customer_name,"region":region,"street":street,"city":city,"postcode":postCode,"telephone":telephone,"country_id":country,"customer_email":customer_email]
          cell.customerName.text=self.customerData["customer_name"]
          cell.street.text=self.customerData["street"]
          cell.city.text=self.customerData["city"]
          cell.region.text=self.customerData["region"]
          cell.postcode.text=self.customerData["postcode"]
          cell.country_id.text=self.customerData["country_id"]
          cell.telephone.text=self.customerData["telephone"]
          return cell
        }
      }else{
        if let cell = tableView.dequeueReusableCell(withIdentifier: "shippingaddress") as? ceMageRmaProductView {
          (cell.viewWithTag(899) as? UILabel)?.text = "Billing Address"
          cell.customerName.text=self.billingData["customer_name"]
          cell.street.text=self.billingData["street"]
          cell.city.text=self.billingData["city"]
          cell.region.text=self.billingData["region"]
          cell.postcode.text=self.billingData["postcode"]
          cell.country_id.text=self.billingData["country_id"]
          cell.telephone.text=self.billingData["telephone"]
          return cell
        }
      }
    }
    else if indexPath.section==3
    {
      if let cell = tableView.dequeueReusableCell(withIdentifier: "customerinfo") as? ceMageRmaProductView {
        cell.customer_email.text=self.customerData["customer_email"]
        cell.customer_name.text=self.customerData["customer_name"]
        return cell
      }
    }
    else if indexPath.section==4
    {
      if let cell = tableView.dequeueReusableCell(withIdentifier: "generalinformation") as? ceMageRmaProductView {
        if orderId == ""{
          cell.reason.setTitle(self.orderData["reason"], for: .normal)
          cell.packageCondition.setTitle(self.orderData["condition"], for: .normal)
          cell.resolution.setTitle(self.orderData["resolution"], for: .normal)
        }else{
          cell.reason.setTitle("--Select--", for: .normal)
          cell.packageCondition.setTitle("--Select--", for: .normal)
          cell.resolution.setTitle("--Select--", for: .normal)
          //                    cell.resolution.backgroundColor = .red
          cell.reason.addTarget(self, action: #selector(setupDropDownView(_:)), for: .touchUpInside)
          cell.packageCondition.addTarget(self, action: #selector(setupDropDownView(_:)), for: .touchUpInside)
          cell.resolution.addTarget(self, action: #selector(setupDropDownView(_:)), for: .touchUpInside)
            
          cell.reason.tag = 101
          cell.packageCondition.tag = 102
          cell.resolution.tag = 103
        }
        return cell
      }
    }
    return UITableViewCell()
  }
  
  
  
  @objc func setupDropDownView(_ sender:UIButton){
    
    var dataSource = [String]()
    if sender.tag == 101{
      dataSource = Array(reason.values)
    }
    if sender.tag == 102{
      dataSource = Array(packageCondition.values)
    }
    if sender.tag == 103{
      dataSource = Array(resolution.values)
    }
    dropDown.dataSource = dataSource;
    dropDown.selectionAction = {(index, item) in
      sender.setTitle(item, for: .normal)
      if sender.tag == 101{
        self.reasonStr = item
      }
      if sender.tag == 102{
        self.pk_condition = item
      }
      if sender.tag == 103{
        self.resolutionStr = item
      }
    }
    
    dropDown.anchorView = sender
    dropDown.bottomOffset = CGPoint(x: 0, y:sender.bounds.height)
    
    if dropDown.isHidden {
      _=dropDown.show();
    } else {
      dropDown.hide();
    }
  }
  
  
  override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
    
    if let data = data {
      if requestUrl?.contains("saverma") ?? false{
        guard let json = try? JSON(data:data) else {return;}
        self.view?.makeToast(json[0]["data"]["message"].stringValue, duration: 2.0, position: .center)
        continueShopping(UIButton())
      }
      else if requestUrl?.contains("generaterma") ?? false{
        guard let json = try? JSON(data:data) else {return;}
        rmaId.text = json[0]["data"]["increment_id"].stringValue
        
        for rmaItemInfo in json[0]["data"]["order_data"]["order_items"].arrayValue {
          rma_qty.append(rmaItemInfo["qty_ordered"].stringValue)
          itemIdArray.append(rmaItemInfo["item_id"].stringValue)
          rmaItemData.append(["rma_qty":rmaItemInfo["qty_ordered"].stringValue,"row_total":rmaItemInfo["row_total"].stringValue,"ordered_qty":rmaItemInfo["ordered_qty"].stringValue,"name":rmaItemInfo["name"].stringValue,"price":rmaItemInfo["price"].stringValue,"sku":rmaItemInfo["product_sku"].stringValue])
        }
        //                ShippING ADDRESSS
        var customer_name=json[0]["data"]["order_data"]["shipping"]["firstname"].stringValue
        var streetArray=json[0]["data"]["order_data"]["shipping"]["street"].arrayValue
        var street=""
        for index in streetArray
        {
          street+=index.stringValue+" "
        }
        var region=json[0]["data"]["order_data"]["shipping"]["region"].stringValue
        var city=json[0]["data"]["order_data"]["shipping"]["city"].stringValue
        var postCode=json[0]["data"]["order_data"]["shipping"]["postcode"].stringValue
        var telephone=json[0]["data"]["order_data"]["shipping"]["telephone"].stringValue
        var country=json[0]["data"]["order_data"]["shipping"]["country_id"].stringValue
        var customer_email=json[0]["data"]["order_data"]["shipping"]["customer_email"].stringValue
        self.customerData=["customer_name":customer_name,"region":region,"street":street,"city":city,"postcode":postCode,"telephone":telephone,"country_id":country,"customer_email":customer_email]
        
        
        //                BILLING ADDRESSS
        customer_name=json[0]["data"]["order_data"]["billing"]["firstname"].stringValue
        streetArray=json[0]["data"]["order_data"]["billing"]["street"].arrayValue
        street=""
        for index in streetArray
        {
          street+=index.stringValue+" "
        }
        region=json[0]["data"]["order_data"]["billing"]["region"].stringValue
        city=json[0]["data"]["order_data"]["billing"]["city"].stringValue
        postCode=json[0]["data"]["order_data"]["billing"]["postcode"].stringValue
        telephone=json[0]["data"]["order_data"]["billing"]["telephone"].stringValue
        country=json[0]["data"]["order_data"]["billing"]["country_id"].stringValue
        customer_email=json[0]["data"]["order_data"]["billing"]["customer_email"].stringValue
        self.billingData=["customer_name":customer_name,"region":region,"street":street,"city":city,"postcode":postCode,"telephone":telephone,"country_id":country,"customer_email":customer_email]
        
        
        
        //                 NEW SELECTION DATA
        for items in json[0]["data"]["reason"].arrayValue{
          reason.updateValue(items["value"].stringValue, forKey: items["label"].stringValue)
        }
        for items in json[0]["data"]["packageCondition"].arrayValue{
          packageCondition.updateValue(items["value"].stringValue, forKey: items["label"].stringValue)
        }
        for items in json[0]["data"]["resolution"].arrayValue{
          resolution.updateValue(items["value"].stringValue, forKey: items["label"].stringValue)
        }
        tableView.dataSource = self
        tableView.delegate = self
        tableView.reloadData()
      }else{
        guard let json = try? JSON(data:data)[0] else {return;}
        if json["data"]["success"].stringValue == "true" {
          for rmaItemInfo in json["data"]["rma_data"]["rma_item_info"].arrayValue {
            rmaItemData.append(["rma_qty":rmaItemInfo["rma_qty"].stringValue,"row_total":rmaItemInfo["row_total"].stringValue,"ordered_qty":rmaItemInfo["ordered_qty"].stringValue,"name":rmaItemInfo["name"].stringValue,"price":rmaItemInfo["price"].stringValue,"sku":rmaItemInfo["sku"].stringValue])
          }
          
          let order_id=json["data"]["rma_data"]["rma_order_info"]["order_id"].stringValue
          let status=json["data"]["rma_data"]["rma_order_info"]["status"].stringValue
          let condition=json["data"]["rma_data"]["rma_order_info"]["pk_condition"].stringValue
          
          let reson=json["data"]["rma_data"]["rma_order_info"]["reason"].stringValue
          let resolution=json["data"]["rma_data"]["rma_order_info"]["resolution"].stringValue
          let increment_id=json["data"]["rma_data"]["rma_order_info"]["rma_increment_id"].stringValue
          rmaId.text = increment_id;
          self.orderData=["order_id":order_id,"status":status,"condition":condition,"resolution":resolution,"increment_id":increment_id]
          
          
          let customer_name=json["data"]["rma_data"]["rma_customer_info"]["customer_name"].stringValue
          let streetArray=json["data"]["rma_data"]["rma_customer_info"]["billing_address"]["street"].arrayValue
          var street=""
          for index in streetArray
          {
            street+=index.stringValue+" "
          }
          let region=json["data"]["rma_data"]["rma_customer_info"]["billing_address"]["region"].stringValue
          let city=json["data"]["rma_data"]["rma_customer_info"]["billing_address"]["city"].stringValue
          let postCode=json["data"]["rma_data"]["rma_customer_info"]["billing_address"]["postcode"].stringValue
          let telephone=json["data"]["rma_data"]["rma_customer_info"]["billing_address"]["telephone"].stringValue
          let country=json["data"]["rma_data"]["rma_customer_info"]["billing_address"]["country_id"].stringValue
          let customer_email=json["data"]["rma_data"]["rma_customer_info"]["customer_email"].stringValue
          self.customerData=["customer_name":customer_name,"region":region,"street":street,"city":city,"postcode":postCode,"telephone":telephone,"country_id":country,"customer_email":customer_email]
        }
        print(rmaItemData)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.reloadData()
      }
    }
  }
  
  
  @objc func setUpRma(_ sender :UIButton){
    var param = [String:String]()
    userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
    if let custId = userInfoDict["customerId"] {
      if orderId != ""{
        param = ["customer_id":custId,"order_id":orderId]
        param.updateValue(reasonStr, forKey: "reason")
        param.updateValue(pk_condition, forKey: "pk_condition")
        param.updateValue(resolutionStr, forKey: "resolution")
        param.updateValue(rma_qty.convtToJson() as String, forKey: "rma_qty")
        param.updateValue(itemIdArray.convtToJson() as String, forKey: "item_id")
        self.sendRequest(url: "mobiconnect/mobirma/saverma", params: param,store:false)
      }
    }
  }
}

extension Array{
  func convtToJson() -> NSString {
    do {
      let json = try JSONSerialization.data(withJSONObject: self, options: JSONSerialization.WritingOptions.prettyPrinted)
      return NSString(data: json, encoding: String.Encoding.utf8.rawValue)!
    }catch {
      return ""
    }
  }
}
