/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */

import UIKit

class cedMagethemetwotable: UITableViewCell,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var collectionView: UICollectionView!
    var datasource = [[String:String]]()
    var sbounds = CGRect()
    var parent = UIViewController()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        collectionView.dataSource = self
        collectionView.delegate = self
        print(datasource)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return datasource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionCelltwo", for: indexPath) as! cedMageThemeTwoCell
       
        cedMageImageLoader.shared.loadImgFromUrl(urlString: datasource[indexPath.row]["banner_image"]!,completionHandler: { (image: UIImage?, url: String) in
            
            DispatchQueue.main.async {
                cell.imageView.image = image
            }
            
        })
        cell.cardView()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if(UIDevice().model.lowercased() == "ipad".lowercased()){
            return CGSize(width: sbounds.width/2-2, height: sbounds.height/3)
        }
        return CGSize(width:sbounds.width/2 , height: sbounds.width/2 )
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let banner = datasource[indexPath.row]
        print(banner)
        if(banner["link_to"] == "category"){
            
            let viewcontrol = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "defaultCategory") as! cedMageDefaultCategory
            viewcontrol.selectedCategory = banner["product_id"]!
            parent.navigationController?.pushViewController(viewcontrol, animated: true)
        }else if (banner["link_to"] == "product"){
            
            let productview = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "rootPageViewController") as! cedMageProductViewRoot
            productview.pageData = [["product_id":banner["product_id"]!]]
            let instance = cedMage.singletonInstance
            instance.storeParameterInteger(parameter: 0)
            parent.navigationController?.pushViewController(productview
                , animated: true)
        }else if (banner["link_to"] == "website"){
            let url = banner["product_id"]
            let viewcontrol = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "cmsWebView") as! cedMageCmsWebView
            viewcontrol.pageUrl = url!
            parent.navigationController?.pushViewController(viewcontrol, animated: true)
            
            
        }
    }
    
}
