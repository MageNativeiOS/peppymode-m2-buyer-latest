//
//  peppyModeHomeVC.swift
//  MageNative Magento Platinum
//
//  Created by Hamza Usmani on 14/06/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class peppyModeHomeVC: MagenativeUIViewController {

    @IBOutlet weak var homeTable: UITableView!
    
    var storeID:String? {
        return UserDefaults.standard.value(forKey: "storeId") as? String
    }
    var homepageData: HomepageData? = nil{
        didSet{
            homeTable.reloadData()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        if UserDefaults.standard.bool(forKey: "skipLogin"){
            UserDefaults.standard.set(false, forKey: "skipLogin")
        }
        sendApiRequest()
        setupTable()
    }
//    override func viewWillAppear(_ animated: Bool) {
//            self.tabBarController?.tabBar.isHidden = false
//        }
    func setupTable(){
        homeTable.dataSource        = self
        homeTable.delegate          = self
        homeTable.separatorStyle    = .none
    }
    
    private func sendApiRequest() {
        guard let storeId = storeID else { print("Store id is nil"); return }
        self.sendRequest(url: "gethomepagedata", params: ["store_id":storeId])
    }

    override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
        guard let data = data else { return }
        do {
            var json = try JSON(data: data)
            json = json[0]
            print(json)
            let finalData   = try json["data"].rawData(options: [])
            homepageData    = try JSONDecoder().decode(HomepageData.self, from: finalData)
            self.showData()
        } catch {
            print(error.localizedDescription)
        }
    }
    
    private func showData() {
        print(homepageData?.banner1?.banner_image ?? "")
        print(homepageData?.banner1?.link_to ?? "")
    }
}

extension peppyModeHomeVC: UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        if homepageData != nil{
            return 11
        }else{
            return 0
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section{
        case 4:     return homepageData?.category3?.sub_cateory?.count == 0 ? 0 : 1
        case 6:     return homepageData?.category1?.sub_cateory?.count == 0 ? 0 : 1
        case 7:     return 3
        case 9:     return homepageData?.category4?.sub_cateory?.count == 0 ? 0 : 1
        case 10:     return homepageData?.category5?.sub_cateory?.count == 0 ? 0 : 1
        case 8: return homepageData?.category2?.sub_cateory?.count == 0 ? 0 : 1
        case 0:     return 0
        default:    return 1
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section{
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SearchTVCell", for: indexPath) as! SearchTVCell
            cell.parent         = self
            return cell
        case  1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SliderCategoryTVCell", for: indexPath) as! SliderCategoryTVCell
            cell.categories     = homepageData?.slider_categories
            cell.parent         = self
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "BannerTVCell", for: indexPath) as! BannerTVCell
            let feed            = homepageData?.banner1
            cell.feed           = feed
            return cell
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SlidingBannerTVCell", for: indexPath) as! SlidingBannerTVCell
            cell.slidingBanners = homepageData?.slider_banners
            cell.parent         = self
            return cell
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CatGridTVCell", for: indexPath) as! CatGridTVCell
            cell.changeLayout(.horizontal)
            let categories  = homepageData?.category3
            cell.threeGrid = 0
            cell.categories = categories
            cell.parent     = self
            return cell
        case 5:
            let cell = tableView.dequeueReusableCell(withIdentifier: "BannerTVCell", for: indexPath) as! BannerTVCell
            let feed            = homepageData?.banner2
            cell.feed           = feed
            return cell
        case 6:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CatGridTVCell", for: indexPath) as! CatGridTVCell
            let categories  = homepageData?.category1
             cell.threeGrid = 0
            cell.categories = categories
            cell.parent     = self
            return cell
            
        case 7:
            switch indexPath.row{
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: "BannerTVCell", for: indexPath) as! BannerTVCell
                let feed        = homepageData?.banner3
                cell.feed       = feed
                return cell
            case 1:
                let cell = tableView.dequeueReusableCell(withIdentifier: "BannerTVCell", for: indexPath) as! BannerTVCell
                let feed        = homepageData?.banner4
                cell.feed       = feed
                return cell
            case 2:
                let cell = tableView.dequeueReusableCell(withIdentifier: "BannerTVCell", for: indexPath) as! BannerTVCell
                let feed        = homepageData?.banner5
                cell.feed       = feed
                return cell
            default: return UITableViewCell()
            }
        case 8:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CatGridTVCell", for: indexPath) as! CatGridTVCell
            let categories  = homepageData?.category2
             cell.threeGrid = 0
            cell.categories = categories
            cell.parent     = self
            return cell
        case 9:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CatGridTVCell", for: indexPath) as! CatGridTVCell
            cell.changeLayout(.horizontal)
           cell.threeGrid = 0
            let categories  = homepageData?.category4
            cell.categories = categories
            cell.parent     = self
            return cell
        case 10:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CatGridTVCell", for: indexPath) as! CatGridTVCell
            let categories  = homepageData?.category5
            cell.categories = categories
            cell.threeGrid = 5
            cell.parent     = self
            return cell
        default: return UITableViewCell()
        }
    }

    func returnHeight(_ product:[HomepageSubcategoryData]) -> Int {
      if product.count % 2 != 0{
        return product.count / 2 + 1
      }
      return product.count / 2
    }
    func returnHeightForCat5(_ product:[HomepageSubcategoryData]) -> Int {
         if product.count % 3 != 0{
           return product.count / 3 + 1
         }
         return product.count / 3
       }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section{
        case 0:     return 65
        case 1:     return 150
        case 5:     return 150
        case 2:   return 220
        case 6:
            guard let categories  = homepageData?.category1?.sub_cateory else {return CGFloat()}
            let staticHeight = 70
            return CGFloat((returnHeight(categories) * 175) + staticHeight)
        case 8:
            guard let categories  = homepageData?.category2?.sub_cateory else {return CGFloat()}
            let staticHeight = 70
            return CGFloat((returnHeight(categories) * 175) + staticHeight)
        case 10:
            guard let categories  = homepageData?.category5?.sub_cateory else {return CGFloat()}
            let staticHeight = 70
            return CGFloat((returnHeightForCat5(categories) * 175) + staticHeight)
        case 4,9: return 220
        default:    return 300
        }
    }
}

extension peppyModeHomeVC: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section{
        case 2: //Banner Cell
            homepageData?.banner1?.link_to == "product" ? goToProductPage(homepageData?.banner1?.product_id ?? ""):print("Not linked to product")
        case 5: //Banner Cell
            homepageData?.banner2?.link_to == "product" ? goToProductPage(homepageData?.banner2?.product_id ?? ""):print("Not linked to product")
        case 7: //Banner Cell
            switch indexPath.row{
            case 0:
                goToProductPage(homepageData?.banner3?.product_id ?? "")
            case 1:
                goToProductPage(homepageData?.banner4?.product_id ?? "")
            case 2:
                goToProductPage(homepageData?.banner5?.product_id ?? "")
            default:
                print("Default")
            }
        default: print("Default")
        }
    }

    func goToProductPage(_ id: String){
        let vc = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "productSinglePageViewController") as! ProductSinglePageViewController
        vc.product_id = id
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
