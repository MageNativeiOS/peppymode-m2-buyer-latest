//
//  homeFeaturedBrandTC.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 01/10/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class homeFeaturedBrandTC: UITableViewCell {

      
        //MARK: - Properties
        
        static var reuseID:String = "homeFeaturedBrandTC"
        var brands = [HomeCategory]() { didSet { brandCollection.reloadData() } }
        weak var parent: UIViewController?
        
        //MARK: - Views
        
        lazy var brandHeading:UILabel = {
            let label = UILabel()
            label.text = "Featured Brands"
            label.textAlignment = .center
             label.font = UIFont.init(fontName: "TrajanPro-Regular", fontSize: 14)
            //label.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
            return label
        }()
        
        lazy var brandCollection:UICollectionView = {
            let layout = UICollectionViewFlowLayout()
            layout.minimumInteritemSpacing = 0
            layout.minimumLineSpacing = 0
            layout.scrollDirection = .horizontal
            let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
            collectionView.backgroundColor = UIColor.clear
            collectionView.register(homeFeaturedBrandCC.self, forCellWithReuseIdentifier: homeFeaturedBrandCC.reuseID)
            collectionView.delegate = self
            collectionView.dataSource = self
            return collectionView
        }()
        

        //MARK: - Life Cycle
        
        override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
            super.init(style: style, reuseIdentifier: reuseIdentifier)
            selectionStyle = .none
            backgroundColor = .clear
            
            addSubview(brandHeading)
            brandHeading.anchor(top: topAnchor, left: leadingAnchor, right: trailingAnchor, paddingTop: 8, paddingLeft: 8)
            
            addSubview(brandCollection)
            brandCollection.anchor(top: brandHeading.bottomAnchor, left: leadingAnchor, bottom: bottomAnchor, right: trailingAnchor, paddingTop: 8, paddingLeft: 8, paddingBottom: 8, paddingRight: 8)
            Timer.scheduledTimer(timeInterval: 2.5, target: self, selector: #selector(self.scrollAutomatically), userInfo: nil, repeats: true)
        }
        
        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        var x = 1
         @objc func scrollAutomatically() {
             if self.x < self.brands.count {
               let indexPath = IndexPath(item: x, section: 0)
               self.brandCollection.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
               self.x = self.x + 2
             }else{
               self.x = 0
               self.brandCollection.scrollToItem(at: IndexPath(item: 0, section: 0), at: .centeredHorizontally, animated: true)
             }
         }
    }

    //MARK: - UICollectionViewDataSource

    extension homeFeaturedBrandTC: UICollectionViewDataSource {
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return brands.count
        }
        
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: homeFeaturedBrandCC.reuseID, for: indexPath) as! homeFeaturedBrandCC
            
            cell.populate(with: brands[indexPath.item])
            return cell
        }
    }

    //MARK: - UICollectionViewDelegate / UICollectionViewDelegateFlowLayout

    extension homeFeaturedBrandTC: UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            if(UIDevice().model.lowercased() == "ipad".lowercased()){
                return .init(width: collectionView.frame.width/3, height: 180)
            }else{
            return .init(width: collectionView.frame.width/3, height: 150)
            }
        }
        
        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            let vc = UIStoryboard(name: "categorylayouts", bundle: nil).instantiateViewController(withIdentifier: "cedMageDefaultCollection") as!  cedMageDefaultCollection
            vc.brandID = brands[indexPath.item].id ?? ""
            
            parent?.navigationController?.pushViewController(vc, animated: true)
        }
          

    }
