//
//  ceMageRmaProductView.swift
//  MageNative Magento Platinum
//
//  Created by CEDCOSS Technologies Private Limited on 13/07/17.
//  Copyright © 2017 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class ceMageRmaProductView: UITableViewCell {

    @IBOutlet weak var orderedQty: UILabel!
    @IBOutlet weak var sku: UILabel!
    @IBOutlet weak var rmaQty: UILabel!
    
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var productName: UILabel!
   
    @IBOutlet weak var orderId: UILabel!
    
    @IBOutlet weak var orderStatus: UILabel!
    
    @IBOutlet weak var customerName: UILabel!
    
    
    @IBOutlet weak var street: UILabel!
    
    
    @IBOutlet weak var city: UILabel!
    
    @IBOutlet weak var region: UILabel!
    
    @IBOutlet weak var postcode: UILabel!
    
    
    @IBOutlet weak var country_id: UILabel!
    
    @IBOutlet weak var telephone: UILabel!
    
    
    @IBOutlet weak var customer_name: UILabel!
    
    @IBOutlet weak var customer_email: UILabel!
    
    
    @IBOutlet weak var reason: UIButton!
    
    @IBOutlet weak var packageCondition: UIButton!
    
    
    @IBOutlet weak var resolution: UIButton!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
