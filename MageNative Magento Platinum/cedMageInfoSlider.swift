/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */


import UIKit
class cedMageInfoSlider: cedMageViewController {
    
    @IBOutlet weak var gotItButton: UIButton!
    @IBOutlet weak var skipNow: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    var dataObject = [String:String]()
    var demoArray = [[String:String]]()
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var sp1: UIImageView!
    @IBOutlet weak var sp3: UIImageView!
    @IBOutlet weak var sp2: UIImageView!
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var textView: UITextView!
    var currentIndex = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        skipNow.setThemeColor()
        nextButton.setThemeColor()
        gotItButton.setThemeColor()
        demoArray = cedMage.getInfoPlist(fileName: "cedMage",indexString: "AppDemo" )as! [[String : String]]
        
        nextButton.addTarget(self, action: #selector(cedMageInfoSlider.nextView(sender: )), for: .touchUpInside)
        if(currentIndex == 0){
            sp1.setThemeColor()
            gotItButton.isHidden = true
            skipNow.isHidden = false
            nextButton.isHidden = false
            sp2.backgroundColor = UIColor.lightGray
            sp3.backgroundColor = UIColor.lightGray
        }else if(currentIndex == 1){
            sp2.setThemeColor()
            gotItButton.isHidden = true
            skipNow.isHidden = false
            nextButton.isHidden = false
            sp1.backgroundColor = UIColor.lightGray
            sp3.backgroundColor = UIColor.lightGray
        }else{
            sp3.setThemeColor()
            gotItButton.isHidden = false
            gotItButton.addTarget(self, action: #selector(cedMageInfoSlider.goToRootView(sender:)), for: .touchUpInside)
            skipNow.isHidden = true
            nextButton.isHidden = true
            sp1.backgroundColor = UIColor.lightGray
            sp2.backgroundColor = UIColor.lightGray
        }
        imageView.image = UIImage(named: dataObject["img"]!)
        textView.text = dataObject["text"]
        headingLabel.text = dataObject["head"]
        textView.fontColorTool()
        headingLabel.fontColorTool()
        if(dataObject["img"] == "sp-3"){
            
        }
        // skipNow.addTarget(self, action: #selector(cedm), for: .touchUpInside)
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @objc func nextView(sender:UIButton){
        if(currentIndex+1 < demoArray.count){
            let data = demoArray[currentIndex + 1]
            dataObject = data
            imageView.image = UIImage(named: data["img"]!)
            textView.text = data["text"]
            headingLabel.text = data["head"]
            textView.fontColorTool()
            headingLabel.fontColorTool()
            if(currentIndex+1 == 0){
                sp1.setThemeColor()
                gotItButton.isHidden = true
                skipNow.isHidden = false
                nextButton.isHidden = false
                sp2.backgroundColor = UIColor.lightGray
                sp3.backgroundColor = UIColor.lightGray
            }else if(currentIndex+1 == 1){
                sp2.setThemeColor()
                gotItButton.isHidden = true
                skipNow.isHidden = false
                nextButton.isHidden = false
                sp1.backgroundColor = UIColor.lightGray
                sp3.backgroundColor = UIColor.lightGray
            }else if(currentIndex+1 == 2){
                sp3.setThemeColor()
                gotItButton.isHidden = false
                gotItButton.addTarget(self, action: #selector(cedMageInfoSlider.goToRootView(sender:)), for: .touchUpInside)
                skipNow.isHidden = true
                nextButton.isHidden = true
                sp1.backgroundColor = UIColor.lightGray
                sp2.backgroundColor = UIColor.lightGray
            }
        }
        currentIndex = currentIndex + 1
    }
    
    @objc func goToRootView(sender:UIButton){
        let view = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "mainView") as? cedMageSideDrawer
        view?.modalPresentationStyle = .fullScreen;
        self.present(view!, animated: true, completion: nil)
    }
    
    func prevView(sender:UIButton){
        if(currentIndex-1 > 0){
            let data = demoArray[currentIndex - 1]
            imageView.image = UIImage(named: data["img"]!)
            textView.text = data["text"]
            headingLabel.text = data["head"]
            textView.fontColorTool()
            headingLabel.fontColorTool()
            
        }
        currentIndex = currentIndex - 1
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
