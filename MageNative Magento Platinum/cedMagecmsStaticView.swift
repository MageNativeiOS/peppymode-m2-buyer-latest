//
//  cedMagecmsStaticView.swift
//  MageNative Magento Platinum
//
//  Created by CEDCOSS Technologies Private Limited on 24/07/17.
//  Copyright © 2017 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class cedMagecmsStaticView: MagenativeUIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    var dataArray = [[String:String]]()
    var page = 0
    var pageData = [String:String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        // Do any additional setup after loading the view.
        dataArray = cedMage.getInfoPlist(fileName: "cedMage",indexString: "AppStaticPages" ) as! [[String : String]]
        pageData = dataArray[page]
        tableView.estimatedRowHeight = 1200
        tableView.rowHeight = UITableView.automaticDimension
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
             if let cell  = tableView.dequeueReusableCell(withIdentifier: "bannerCell")   {
                return cell
            }
            
        }
        if indexPath.row == 1 {
        if let cell  = tableView.dequeueReusableCell(withIdentifier: "headingCell")   {
            let label = cell.contentView.viewWithTag(1249) as? UILabel
            label?.fontColorTool()
            label?.text = pageData["head"]
            return cell
      
        }
        }else if indexPath.row == 2 {
            if let cell  = tableView.dequeueReusableCell(withIdentifier: "contentCell")   {
                let label = cell.contentView.viewWithTag(1250) as? UILabel
                label?.text = pageData["text"]
                return cell
                
            }
        }
        return UITableViewCell()
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 100
        }
        return UITableView.automaticDimension
    }

  
   

}
