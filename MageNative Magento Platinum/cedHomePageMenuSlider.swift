//
//  cedHomePageMenuSlider.swift
//  MageNative Magento Platinum
//
//  Created by CEDCOSS Technologies Private Limited on 25/01/17.
//  Copyright © 2017 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

//class cedHomePageMenuSlider: cedMageViewController {
//    
//    var pageMenu : CAPSPageMenu?
//    override func viewDidLoad() {
//        self.navigationController?.navigationBar.isHidden = false
//        super.viewDidLoad()
//        
//        NotificationCenter.default.addObserver(self, selector: #selector(cedHomePageMenuSlider.loadFrmNotif(_:)), name: NSNotification.Name(rawValue: "loadHomePageAgain"), object: nil);
//        
//        addFeatureProduct()
//        
//        
//        
//        // MARK: - UI Setup
//        
//        // self.title = "Alavenue"
//        
//        // MARK: - Scroll menu setup
//        
//        // Initialize view controllers to display and place in array
//        
//        
//        
//        // Customize menu (Optional)
//        
//        
//        
//    }
//    
//    @objc func loadFrmNotif(_ notification: NSNotification){
//        //        accountArray = [[String:String]]()
//        //        Stores = [[String:String]]()
//        //        cmsPages = [[String:String]]()
//        //        dataSource = expandingCells()
//        //
//        //        loadDrawerData()
//        
//        addFeatureProduct()
//        if let cartCount = self.defaults.value(forKey: "items_count") as? String {
//             self.setCartCount(view: self, items: cartCount)
//        }else{
//             self.setCartCount(view: self, items: "0")
//        }
//        
//    }
//    func addFeatureProduct()
//    {
//        var controllerArray : [UIViewController] = []
//        
//        if let controller1 : cedMageHomeThemeCode1 = UIStoryboard(name: "homeLayouts", bundle: nil).instantiateViewController(withIdentifier: "themeCode1") as? cedMageHomeThemeCode1 {
//            controller1.title = "HAPPY DEAL OFFERS".uppercased()           
//            controllerArray.append(controller1)
//        }
//        /*if let controller1 : homeMapViewController = UIStoryboard(name: "homeLayouts", bundle: nil).instantiateViewController(withIdentifier: "homeMapViewController") as? homeMapViewController {
//         controller1.title = "Home".uppercased()
//         controllerArray.append(controller1)
//         }*/
//        
//        if let isFeatured = UserDefaults.standard.value(forKey: "featureEnabled") as? String {
//            print("isFeaturedisFeaturedisFeatured")
//            print(isFeatured)
//            if isFeatured=="true"{
//                if let controller2 : cedFeaturedProductListing = UIStoryboard(name: "homeLayouts", bundle: nil).instantiateViewController(withIdentifier: "cedFeaturedProducts") as? cedFeaturedProductListing {
//                    controller2.homePage = true
//                    
//                    
//                    self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.black]
//                    controller2.title = "OUR PRODUCTS".localized.uppercased()
//                    controllerArray.append(controller2)
//                }
//            }
//        }
//         let color = cedMage.getInfoPlist(fileName:"cedMage",indexString: "fontColor") as! String
//        let parameters: [CAPSPageMenuOption] = [
//            .scrollMenuBackgroundColor(cedMage.UIColorFromRGB(colorCode: "#ffffff")),
//            .viewBackgroundColor(cedMage.UIColorFromRGB(colorCode: "#ffffff")),
//            .selectionIndicatorColor(UIColor.black),
//            .bottomMenuHairlineColor(UIColor.black),
//            .menuItemFont(UIFont(name: "HelveticaNeue", size: 14.0)!),
//            .menuHeight(40.0),
//            .menuItemWidth(160),
//            .centerMenuItems(true),
//            .addBottomMenuShadow(true),
//            .menuShadowColor(UIColor.black),
//            .menuShadowRadius(4),
//            .selectedMenuItemLabelColor(cedMage.UIColorFromRGB(colorCode: color)),
//            .unselectedMenuItemLabelColor(cedMage.UIColorFromRGB(colorCode: color))
//        ]
//        
//        // Initialize scroll menu
//        let rect = CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: self.view.frame.width, height: self.view.frame.height))
//        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: rect, pageMenuOptions: parameters)
//        
//        self.addChildViewController(pageMenu!)
//        self.view.addSubview(pageMenu!.view)
//        // cedMageLoaders.removeLoadingIndicator(me: self)
//        pageMenu!.didMove(toParentViewController: self)
//        
//    }
//    func didTapGoToLeft() {
//        let currentIndex = pageMenu!.currentPageIndex
//        
//        if currentIndex > 0 {
//            pageMenu!.moveToPage(currentIndex - 1)
//        }
//    }
//    
//    func didTapGoToRight() {
//        let currentIndex = pageMenu!.currentPageIndex
//        
//        if currentIndex < pageMenu!.controllerArray.count {
//            pageMenu!.moveToPage(currentIndex + 1)
//        }
//    }
//    
//    // MARK: - Container View Controller
//    //    override func shouldAutomaticallyForwardAppearanceMethods() -> Bool {
//    //        return true
//    //    }
//    
//    override func shouldAutomaticallyForwardRotationMethods() -> Bool {
//        return true
//    }
//}
