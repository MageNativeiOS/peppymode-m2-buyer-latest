/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */


import UIKit

class testView: UIView {

    var view : UIView?
    
    @IBOutlet weak var productOne: UIView!
    @IBOutlet weak var productTwo: UIView!
    
    @IBOutlet weak var productThree: UIView!
    
    @IBOutlet weak var productImg1: UIImageView!
    
    @IBOutlet weak var productname1: UILabel!
    
    @IBOutlet weak var productImage2: UIImageView!
    
    @IBOutlet weak var productName2: UILabel!
    
    @IBOutlet weak var productImage3: UIImageView!
    
    @IBOutlet weak var productName3: UILabel!
    override init(frame: CGRect)
    {
        // 1. setup any properties here
        
        // 2. call super.init(frame:)
        super.init(frame: frame)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        // 1. setup any properties here
        
        // 2. call super.init(coder:)
        super.init(coder: aDecoder)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    func xibSetup()
    {
        view = loadViewFromNib()
        
        productname1.fontColorTool()
        productName2.fontColorTool()
        productName3.fontColorTool()

        // use bounds not frame or it'll be offset
        view?.frame = bounds
        
        // Make the view stretch with containing view
        view?.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        
//       productImg1.layer.cornerRadius = productImg1.frame.width/2
//        productImg1.layer.masksToBounds = true
//        
//         productImage2.layer.cornerRadius = productImage2.frame.width/2
//        productImage2.layer.masksToBounds = true
//         productImage3.layer.cornerRadius = productImage3.frame.width/2
//        productImage3.layer.masksToBounds = true
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view!)
       
    }
    
    func loadViewFromNib() -> UIView
    {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "testView", bundle: bundle)
        
        // Assumes UIView is top level and only object in CustomView.xib file
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }}
