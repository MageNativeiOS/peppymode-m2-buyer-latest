/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */


import UIKit

class ProductQtyView: UIView {

    // Our custom view from the XIB file
    var view: UIView!
    
    //outlets
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var decrementButon: UIButton!
    @IBOutlet weak var incrementButton: UIButton!
    @IBOutlet weak var productQty: UITextField!
    
    override init(frame: CGRect)
    {
        // 1. setup any properties here
        
        // 2. call super.init(frame:)
        super.init(frame: frame)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        // 1. setup any properties here
        
        // 2. call super.init(coder:)
        super.init(coder: aDecoder)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    func xibSetup()
    {
        view = loadViewFromNib()
        
        // use bounds not frame or it'll be offset
        view.frame = bounds
        
        // Make the view stretch with containing view
        view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        
        topLabel.fontColorTool()
        decrementButon.fontColorTool()
        incrementButton.fontColorTool()
        productQty.fontColorTool()
        
        
        //extra setup
        topLabel.font = UIFont(fontName: "HelveticaNeue-Medium", fontSize: CGFloat(17.0));
        productQty.font = UIFont(fontName: "", fontSize: CGFloat(17.0));
        topLabel.text = "QUANTITY".localized;
        productQty.text = "1";
        incrementButton.addTarget(self, action: #selector(ProductQtyView.incrementProductQty(_:)), for: UIControl.Event.touchUpInside);
        decrementButon.addTarget(self, action: #selector(ProductQtyView.decrementProductQty(_:)), for: UIControl.Event.touchUpInside);
        
        self.makeCard(self, cornerRadius: 2, color: UIColor.gray, shadowOpacity: 0.0);
        //extra setup
        
        
        
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView
    {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "ProductQtyView", bundle: bundle)
        
        // Assumes UIView is top level and only object in CustomView.xib file
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    
    @objc func incrementProductQty(_ sender:UIButton){
        
        if(productQty.text == ""){
            productQty.text = String("1");
            return;
        }
        if(productQty.text != ""){
            var currentQty = Int(productQty.text!)!;
            currentQty = currentQty+1;
            productQty.text = String(currentQty);
        }
        
    }
    
    @objc func decrementProductQty(_ sender:UIButton){
        
        if(productQty.text != "" && productQty.text != "1"){
            var currentQty = Int(productQty.text!)!;
            currentQty = currentQty-1;
            productQty.text = String(currentQty);
        }
        
    }

}
