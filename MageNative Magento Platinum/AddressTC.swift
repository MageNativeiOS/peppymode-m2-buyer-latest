//
//  AddressTC.swift
//  MageNative Magento Platinum
//
//  Created by Hamza Usmani on 22/08/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class AddressTC: UITableViewCell {

    //MARK:- Properties
    
    static var reuseID:String = "AddressTC"
    var address: Address? {
        didSet { configureUI() }
    }
    
    
    lazy var addressLabel:UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black
        label.font = UIFont.systemFont(ofSize: 15, weight: .medium)
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    
    
    //MARK:- Lifecycles
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        addSubview(addressLabel)
        addressLabel.topAnchor.constraint(equalTo: topAnchor, constant: 8).isActive = true
        addressLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16).isActive = true
        addressLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16).isActive = true
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK:- Helpers

    private func configureUI() {
        var addressString = String()
        
        guard let address = self.address else { return }
        
        addressString += address.firstname + " " + address.lastname + " \n"
        addressString += address.street + " \n"
        addressString += address.city + " \n"
        addressString += address.region + " \n"
        addressString += address.postcode + " \n"
        addressString += address.country_id + " \n"
        addressString += address.telephone + " \n"
        
        self.addressLabel.text = addressString
    }
    
}
