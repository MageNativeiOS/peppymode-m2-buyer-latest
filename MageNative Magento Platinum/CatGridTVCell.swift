//
//  CatGridTVCell.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 16/06/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class CatGridTVCell: UITableViewCell {

    @IBOutlet weak var header: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    var parent: UIViewController!
    var threeGrid = 0
    var categories: HomepageCategoryData?{
        didSet{
            header.text = categories?.name
            collectionView.reloadData()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        collectionView.delegate    = self
        collectionView.dataSource  = self
        self.selectionStyle        = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

    func changeLayout(_ scrollDirection: UICollectionView.ScrollDirection){
        if let layout = self.collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = scrollDirection
        }
    }
}

extension CatGridTVCell: UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categories?.sub_cateory?.count ?? 0
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CatGridCVCell", for: indexPath) as! CatGridCVCell
        let category  = categories?.sub_cateory?[indexPath.row]
        cell.gridCheck = self.threeGrid
        cell.category = category
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if threeGrid == 5{
            return CGSize(width: collectionView.frame.width/3 - 5, height: 170)
        }else{
        return CGSize(width: collectionView.frame.width/2 - 10, height: 170)
        }
    }
}

extension CatGridTVCell: UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let category  = categories?.sub_cateory?[indexPath.row]
        let vc = UIStoryboard(name: "categorylayouts", bundle: nil).instantiateViewController(withIdentifier: "cedMageDefaultCollection") as! cedMageDefaultCollection
        vc.selectedCategory = category?.category_id ?? ""
        parent.navigationController?.pushViewController(vc, animated: true)
    }
}


