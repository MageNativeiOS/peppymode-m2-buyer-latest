//
//  HomeCircularCategorySlider.swift
//  MageNative Magento Platinum
//
//  Created by Hamza Usmani on 22/09/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class HomeCircularCategorySliderTC: UITableViewCell {
    
    //MARK: - Properties
    
    static var reuseID:String = "HomeCircularCategorySliderTC"
    var categories = [HomeCategory]() { didSet { categorySlider.reloadData() } }
    weak var parent: UIViewController?
    var grid = false
    //MARK: - Views
    
    lazy var categoryHeading:UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.text = "Category Slider"
        label.font = UIFont.init(fontName: "TrajanPro-Regular", fontSize: 14)
        //label.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
        return label
    }()
    
    lazy var categorySlider:UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = UIColor.clear
        collectionView.register(HomeCircularCategorySliderCC.self, forCellWithReuseIdentifier: HomeCircularCategorySliderCC.reuseID)
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
    
        collectionView.delegate = self
        collectionView.dataSource = self
        return collectionView
    }()
    func changeLayout(_ scrollDirection: UICollectionView.ScrollDirection){
        if let layout = self.categorySlider.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = scrollDirection
        }
    }
    //MARK: - Life Cycle
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        backgroundColor = .clear
        
        addSubview(categoryHeading)
        categoryHeading.anchor(top: topAnchor, left: leadingAnchor, right: trailingAnchor, paddingTop: 8, paddingLeft: 8)
        
        addSubview(categorySlider)
        categorySlider.anchor(top: categoryHeading.bottomAnchor, left: leadingAnchor, bottom: bottomAnchor, right: trailingAnchor, paddingTop: 8, paddingLeft: 8, paddingBottom: 8, paddingRight: 8)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

//MARK: - UICollectionViewDataSource

extension HomeCircularCategorySliderTC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HomeCircularCategorySliderCC.reuseID, for: indexPath) as! HomeCircularCategorySliderCC
       
        cell.populate(with: categories[indexPath.item])
        cell.imageOuterView.layer.cornerRadius = cell.imageOuterView.frame.height/2
        cell.imageOuterView.cardView2()
        cell.imageOuterView.clipsToBounds = true
        return cell
    }
}

//MARK: - UICollectionViewDelegate / UICollectionViewDelegateFlowLayout

extension HomeCircularCategorySliderTC: UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch grid{
        case true:
            return .init(width: collectionView.frame.width/3, height: 140)//collectionView.frame.height)
        default:
        return .init(width: collectionView.frame.width/4, height: collectionView.frame.height)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = UIStoryboard(name: "categorylayouts", bundle: nil).instantiateViewController(withIdentifier: "cedMageDefaultCollection") as!  cedMageDefaultCollection
        vc.selectedCategory = categories[indexPath.item].id ?? ""
        parent?.navigationController?.pushViewController(vc, animated: true)
    }
}
