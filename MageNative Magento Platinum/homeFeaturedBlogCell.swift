//
//  homeFeaturedBlogCell.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 01/10/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class homeFeaturedBlogCell: UITableViewCell {

    //MARK: - Properties
          
          static var reuseID:String = "homeFeaturedBlogCell"
          var blogData = [FeaturedBlog]() { didSet { blogCollection.reloadData() } }
          weak var parent: UIViewController?
          
          //MARK: - Views
          
          lazy var blogHeading:UILabel = {
              let label = UILabel()
              label.text = "Blogs"
             label.textAlignment = .center
             label.font = UIFont.init(fontName: "TrajanPro-Regular", fontSize: 14)
              //label.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
              return label
          }()
          
          lazy var blogCollection:UICollectionView = {
              let layout = UICollectionViewFlowLayout()
              layout.minimumInteritemSpacing = 2
              layout.minimumLineSpacing = 0
              layout.scrollDirection = .horizontal
              let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
           collectionView.showsVerticalScrollIndicator = false
           collectionView.showsHorizontalScrollIndicator = false
                collectionView.backgroundColor = UIColor.clear
            
              collectionView.register(homeFeaturedBlogCC.self, forCellWithReuseIdentifier: homeFeaturedBlogCC.reuseID)
              collectionView.delegate = self
              collectionView.dataSource = self
              return collectionView
          }()
          

          //MARK: - Life Cycle
          
          override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
              super.init(style: style, reuseIdentifier: reuseIdentifier)
              selectionStyle = .none
              backgroundColor = .clear
              
              addSubview(blogHeading)
              blogHeading.anchor(top: topAnchor, left: leadingAnchor, right: trailingAnchor, paddingTop: 8, paddingLeft: 8)
              
              addSubview(blogCollection)
              blogCollection.anchor(top: blogHeading.bottomAnchor, left: leadingAnchor, bottom: bottomAnchor, right: trailingAnchor, paddingTop: 8, paddingLeft: 8, paddingBottom: 8, paddingRight: 8)
              
            Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(self.scrollAutomatically), userInfo: nil, repeats: true)
                }
                
                required init?(coder: NSCoder) {
                    fatalError("init(coder:) has not been implemented")
                }
               
              

               var x = 1
               @objc func scrollAutomatically() {
                   if self.x < self.blogData.count {
                     let indexPath = IndexPath(item: x, section: 0)
                     self.blogCollection.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
                     self.x = self.x + 1
                   }else{
                     self.x = 0
                     self.blogCollection.scrollToItem(at: IndexPath(item: 0, section: 0), at: .centeredHorizontally, animated: true)
                   }
               }
          
      }

      //MARK: - UICollectionViewDataSource

      extension homeFeaturedBlogCell: UICollectionViewDataSource {
          func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
              return blogData.count
          }
          
          func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
              let cell = collectionView.dequeueReusableCell(withReuseIdentifier: homeFeaturedBlogCC.reuseID, for: indexPath) as! homeFeaturedBlogCC
              cell.populate(with: blogData[indexPath.item])
            if #available(iOS 13.0, *) {
                cell.contentView.backgroundColor = .systemBackground
            } else {
                cell.contentView.backgroundColor = .white
            }
            cell.contentView.cardView2()
              return cell
          }
      }

      //MARK: - UICollectionViewDelegate / UICollectionViewDelegateFlowLayout

      extension homeFeaturedBlogCell: UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {
          func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            return .init(width: collectionView.frame.width/1.2 - 2, height: 300)
          }
          
          func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
                let view = UIStoryboard(name: "cedMageAccounts", bundle: nil)
                 let vc = view.instantiateViewController(withIdentifier: "cmsWebView") as! cedMageCmsWebView
            let url = blogData[indexPath.item].url_key ?? ""
            vc.pageUrl = url
            parent?.navigationController?.pushViewController(vc, animated: true)
             }
      }

