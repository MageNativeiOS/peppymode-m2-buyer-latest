//
//  SlidingBannerTVCell.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 16/06/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class SlidingBannerTVCell: UITableViewCell {
    var parent: UIViewController!
    var slidingBanners: [HomepageBannerData]? = []{
    didSet{
        self.bannerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
        self.bannerView.itemSize = CGSize.zero
        bannerView.delegate = self
        bannerView.dataSource = self
        bannerView.automaticSlidingInterval = 3.0
        bannerView.isInfinite = true
        }
    }
    @IBOutlet weak var bannerView: FSPagerView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}

extension SlidingBannerTVCell: FSPagerViewDelegate,FSPagerViewDataSource {

   public func numberOfItems(in pagerView: FSPagerView) -> Int {
    return slidingBanners?.count ?? 0
    }

    public func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
        cell.imageView?.contentMode = .scaleAspectFit
        cell.imageView?.clipsToBounds = true
        if let url = URL(string: slidingBanners?[index].banner_image ?? String()){
            cell.imageView?.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "login-logo"))
            cell.imageView?.contentMode = .scaleToFill
        }
        return cell
    }

    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {

        switch slidingBanners?[index].link_to {
        case "category":
            let vc = UIStoryboard(name: "categorylayouts", bundle: nil).instantiateViewController(withIdentifier: "cedMageDefaultCollection") as! cedMageDefaultCollection
            vc.selectedCategory = slidingBanners?[index].product_id ?? ""
            parent.navigationController?.pushViewController(vc, animated: true)
        case "product":
            let vc = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "productSinglePageViewController") as! ProductSinglePageViewController
            vc.product_id = slidingBanners?[index].product_id ?? ""
            parent.navigationController?.pushViewController(vc, animated: true)
        default:
            let view = UIStoryboard(name: "cedMageAccounts", bundle: nil)
            let vc = view.instantiateViewController(withIdentifier: "cmsWebView") as! cedMageCmsWebView
            let url = slidingBanners?[index].product_id ?? ""
            vc.pageUrl = url
            parent.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
