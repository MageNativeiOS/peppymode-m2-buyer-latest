//
//  HomeService.swift
//  MageNative Magento Platinum
//
//  Created by Hamza Usmani on 02/09/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import Foundation

class HomeService {
    
    var storeID: String {
        return UserDefaults.standard.value(forKey: "storeId") as? String ?? ""
    }
    var dealSlider = [HomeDealProduct]()
    
    static let shared = HomeService()
    
    func getHomepageData(controller: UIViewController, page: String, completion: @escaping ([HomeDataModel]?)-> Void) {
        let params = ["store_id": storeID, "page":page]
        ApiHandler.handle.request(with: "getNewHomepage", params: params, requestType: .POST, controller: controller, completion: { data, error in
            guard let data = data else { return }
            print("homepageData")
            print(NSString(data: data, encoding: String.Encoding.utf8.rawValue))
            guard let json = try? JSON(data: data) else { return }
            print(json)
            
            do{
                let resultJson = try json[0]["data"]["design"].rawData(options: [])
                let result = try! JSONDecoder().decode([HomeDataModel].self, from: resultJson)
                print("DEBUG: Result Count is \(result.count)")
                print(result)
                completion(result)
            } catch {
                completion(nil)
                print(error.localizedDescription)
            }
        })
    }
}
