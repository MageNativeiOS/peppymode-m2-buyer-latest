//
//  loginLogoCell.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 24/07/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class loginLogoCell: UITableViewCell {
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var loginLogoImg: UIImageView!
    
    @IBOutlet weak var pagerView: FSPagerView!
    
    var parent: UIViewController!
    var slidingBanners = [String]()
    {
        didSet{
           self.pagerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
           self.pagerView.itemSize = CGSize.zero
           pagerView.delegate = self
           pagerView.dataSource = self
           pagerView.automaticSlidingInterval = 3.0
           pagerView.isInfinite = true
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
     
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
extension loginLogoCell : FSPagerViewDelegate,FSPagerViewDataSource {

   public func numberOfItems(in pagerView: FSPagerView) -> Int {
    return slidingBanners.count
    }

    public func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
        cell.imageView?.contentMode = .scaleAspectFit
        cell.imageView?.clipsToBounds = true
        cell.imageView?.tintColor = .white
        //        if let url = URL(string: slidingBanners?[index].banner_image ?? String()){
//            cell.imageView?.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "login-logo"))
//            cell.imageView?.contentMode = .scaleToFill
//        }
        cell.imageView?.image = UIImage(named: slidingBanners[index])
         cell.imageView?.contentMode = .scaleToFill
        return cell
    }

}

