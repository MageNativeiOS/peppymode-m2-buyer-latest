//
//  SliderCategoryCVCell.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 16/06/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class SliderCategoryCVCell: UICollectionViewCell {
    
    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var categoryName: UILabel!
    @IBOutlet weak var categoryImage: UIImageView!

    func setupView(){
        borderView.cardView2()
        borderView.layer.cornerRadius       = borderView.frame.width * 0.5
        categoryImage.layer.cornerRadius    = categoryImage.frame.width * 0.5
    }

    var category: HomepageSliderCategoryData?{
        didSet{
            setupView()
            categoryImage.sd_setImage(with: URL(string: (category?.image)!), placeholderImage: UIImage(named: "placeholder"))
            categoryName.text = category?.name
        }
    }
}
