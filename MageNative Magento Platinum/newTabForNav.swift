//
//  newTabForNav.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 29/07/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class newTabForNav: homedefaultNavigation {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        
//        let defaults = UserDefaults.standard
//        self.popToRootViewController(animated: false)
//        let view = UIStoryboard(name: "cedMageAccounts", bundle: nil)
//        let vc = view.instantiateViewController(withIdentifier: "cmsWebView") as! cedMageCmsWebView
//        let url = "https://peppymode.com/what-s-new.html"
//        vc.pageUrl = url
//        self.setViewControllers([vc], animated: true)
   // let vc = UIStoryboard(name: "categorylayouts", bundle: nil).instantiateViewController(withIdentifier: "cedMageDefaultCollection") as! cedMageDefaultCollection
          //vc.selectedCategory = "6"

        let vc = UIStoryboard(name: "categorylayouts", bundle: nil).instantiateViewController(withIdentifier: "cedMageDefaultCollection") as! cedMageDefaultCollection
        vc.selectedCategory = "6"
        vc.toggleCheck = false
        self.setViewControllers([vc], animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
