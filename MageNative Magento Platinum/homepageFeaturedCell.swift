//
//  homepageFeaturedCell.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 16/11/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit





class homepageFeaturedCell: UITableViewCell {
  
  @IBOutlet weak var productCollection: UICollectionView!
  
  var products: [homepageProduct] = [] {
    didSet{
      productCollection.delegate = self
      productCollection.dataSource = self
      productCollection.reloadData()
    }
  }
  var parent: UIViewController!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
  }
}

extension homepageFeaturedCell: UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource{
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return products.count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "homepageFeauredCollCell", for: indexPath) as! homepageFeauredCollCell
    
    cell.insideView.cardView()
 
    cell.singleProduct = self.products[indexPath.row]
    return cell
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    return CGSize(width: collectionView.frame.width/2 , height: 250)
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
    return 0
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
    return 0
  }
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    let storyboard = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil);
    let viewController = storyboard.instantiateViewController(withIdentifier: "productSinglePageViewController") as! ProductSinglePageViewController;
    viewController.product_id = products[indexPath.row].product_id!
    parent.navigationController?.pushViewController(viewController, animated: true)
    
  }
}
