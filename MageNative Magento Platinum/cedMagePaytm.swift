//
//  cedMagePaytm.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 21/12/17.
//  Copyright © 2017 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit
/*
extension cedMagePayementView: PGTransactionDelegate {
    func didCancelTrasaction(_ controller: PGTransactionViewController!) {
        self.navigationController?.view.makeToast("Transaction cancelled", duration: 2.0, position: .center, title: nil, image: nil, style: nil, completion: nil)
        cedMage.delay(delay: 2, closure: {
            self.afterPayment(payment_id: "", failure: "true")
        })
    }
    
    func errorMisssingParameter(_ controller: PGTransactionViewController!, error: Error!) {
        print("---error---")
        self.view.makeToast("\(error.localizedDescription)", duration: 2, position: .center)
        self.afterPayment(payment_id: "", failure: "true")
    }
    
    
    func loadPaytm()
    {
        var params = "";
        params="order_id/"+self.orderStatusData["orderId"]!+"/tax_amt/"+self.total["grandtotal"]!+"/user_agent/ios";
        if defaults.bool(forKey: "isLogin") {
            if let customId = userInfoDict["customerId"] {
                params += "/customer_id/"+customId
            }
        }
        let url = "/mobipaytm/index/generatechecksum/"
        cedMageLoaders.addDefaultLoader(me: self)
        var httpUrl = "";
        httpUrl = cedMage.getInfoPlist(fileName:"cedMage",indexString: "cedAppBaseUrl") as! String
        let reqUrl = httpUrl+url+params
        var makeRequest = URLRequest(url: URL(string: reqUrl)!)
        makeRequest.httpMethod = "GET"
        
        print(reqUrl)
        let task = URLSession.shared.dataTask(with: makeRequest, completionHandler: {data,response,error in
            
            
            // check for http errors
            if let httpStatus = response as? HTTPURLResponse , httpStatus.statusCode != 200
            {
                
                DispatchQueue.main.async
                    {
                        cedMageLoaders.removeLoadingIndicator(me: self)
                        self.renderNoDataImage(view: self, imageName: "no_module")
                        
                }
                return;
            }
            
            // code to fetch values from response :: start
            
            guard error == nil && data != nil else
            {
                DispatchQueue.main.async
                    {
                        cedMageLoaders.removeLoadingIndicator(me: self)
                        cedMageHttpException.showAlertView(me: self, msg: error?.localizedDescription, title: "Error".localized )
                }
                return ;
            }
            
            DispatchQueue.main.async
                {
                    cedMageLoaders.removeLoadingIndicator(me: self)
                    
                    guard let json = try? JSON(data: data!) else{return;}
                    print(json)
                    self.mid = json["MID"].stringValue;//"vtHeMn96736813085482"//
                    self.callbackUrl = json["CALLBACK_URL"].stringValue;//"https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=?ORDER_ID=000003500"//
                    self.channelId = json["CHANNEL_ID"].stringValue;//"WAP"//
                    self.environment = json["Env"].stringValue;//"production"//
                    self.website = json["WEBSITE"].stringValue//"DEFAULT"//
                    self.retail = json["INDUSTRY_TYPE_ID"].stringValue//"Retail"//
                    self.checksum = json["CHECKSUMHASH"].stringValue//"D9NUXV9gD//qPdnIYBB/t/yum6EWAGi/DgNfhmS8uoshwOTOUSlRsKe9oo5sPjnugQFTyPyKbpC5orO2TdO/xZv2pl4bAFsbFDOO1nFrFRQ="//
                    self.paytm()
            }
        })
        
        task.resume()
        
        // Do any additional setup after loading the view.
    }
    
    
    func show(_ controller: PGTransactionViewController) {
        if navigationController != nil {
            navigationController?.pushViewController(controller as UIViewController , animated: true)
        }
        else {
            controller.modalPresentationStyle = .fullScreen
            present(controller as UIViewController, animated: true, completion: {() -> Void in
            })
        }
    }
    
    
    
    func remove(_ controller: PGTransactionViewController) {
        if navigationController != nil {
            navigationController?.popViewController(animated: true)
            //self.afterPayment(payment_id: "", failure: "true")
        }
        else {
            controller.dismiss(animated: true, completion: {() -> Void in
                //self.afterPayment(payment_id: "", failure: "true")
            })
        }
    }
    
    func didFinishedResponse(_ controller: PGTransactionViewController, response responseString: String) {
        print("ViewController::didFinishedResponse:response = %@", responseString)
        let title = "Response"
        
        
        
        if responseString.contains("TXN_FAILURE"){
            self.afterPayment(payment_id: "", failure: "true")
            return;
        }
        self.navigationController?.view.makeToast("Transaction successful", duration: 2.0, position: .center, title: nil, image: nil, style: nil, completion: nil)
        cedMage.delay(delay: 2, closure: {
            self.afterPayment(payment_id:responseString,failure:"false",order_id: self.orderStatusData["orderId"]!)
            //showAlert(title: title, message: responseString)
            self.remove(controller)
        })
        
    }
    
    func showAlert(title:String,message:String)  {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            self.dismiss(animated: true, completion: nil)
        }))
        alert.modalPresentationStyle = .fullScreen
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func didCancelTransaction(_ controller: PGTransactionViewController, error: Error?, response: [AnyHashable: Any]) {
        print("ViewController::didCancelTransaction error = %@ response= %@", error!, response)
        var msg: String? = nil
        if error == nil {
            msg = "Successful"
        }
        else {
            msg = "UnSuccessful"
        }
        showAlert(title:"didCancelTransaction" , message: msg!)
        remove(controller)
    }
    
    func didFinishCASTransaction(_ controller: PGTransactionViewController, response: [AnyHashable: Any]) {
        print("ViewController::didFinishCASTransaction:response = %@", response)
    }
    
    func paytm()
    {
        let mc = PGMerchantConfiguration.default()
        var orderDict = [AnyHashable: Any]()
        orderDict["MID"] = mid
        orderDict["ORDER_ID"] = self.orderStatusData["orderId"]!
        if defaults.bool(forKey: "isLogin") {
            if let customId = userInfoDict["customerId"] {
                orderDict["CUST_ID"] = customId;
            }
        }
        orderDict["INDUSTRY_TYPE_ID"] = retail;
        orderDict["CHANNEL_ID"] = channelId
        orderDict["TXN_AMOUNT"] = self.total["grandtotal"]!
        orderDict["WEBSITE"] = website
        orderDict["CALLBACK_URL"] = callbackUrl
        orderDict["CHECKSUMHASH"] = checksum
        
//     var orderDict =   [
//          "WEBSITE" : "DEFAULT",
//          "CALLBACK_URL" : "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=?ORDER_ID=000003471",
//          "CUST_ID" : "5038",
//          "Env" : "production",
//          "CHECKSUMHASH" : "JmrqfS9dmAAlixPi0xCaGcf7nDzOCEfqeSVcWs8rmFOcJKgeWXNv+bCulXRTSwPD+WtyVZWMgyvG2quu4YdlLpR4NAYgdpfY1MVkmdJtfLE=",
//          "INDUSTRY_TYPE_ID" : "Retail",
//          "CHANNEL_ID" : "WAP",
//          "ORDER_ID" : "000003471",
//          "TXN_AMOUNT" : "161.8",
//          "MID" : "vtHeMn96736813085482"
//        ]
        
        let order = PGOrder(params: orderDict)
        let txnController = PGTransactionViewController.init(transactionFor: order)
        if(self.environment == "sandbox")
        {
            txnController!.serverType = eServerTypeStaging
        }
        else
        {
            txnController!.serverType = eServerTypeProduction
        }
        txnController!.merchant = mc
        txnController!.delegate = self
        txnController!.loggingEnabled = true
        self.show(txnController!)
    }
    
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
*/
