//
//  trackOrderView.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 03/11/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import Foundation

class trackOrderView:UIView{
    var view:UIView!
    @IBOutlet weak var shipmentID: UILabel!
    
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var infoTitle: UILabel!
    
    @IBOutlet weak var infoNumber: UILabel!
    
    @IBOutlet weak var trackWebView: WKWebView!
    override init(frame: CGRect)
        {
            // 1. setup any properties here
            
            // 2. call super.init(frame:)
            super.init(frame: frame)
            
            // 3. Setup view from .xib file
            xibSetup()
        }
        
        required init?(coder aDecoder: NSCoder)
        {
            // 1. setup any properties here
            
            // 2. call super.init(coder:)
            super.init(coder: aDecoder)
            
            // 3. Setup view from .xib file
            xibSetup()
        }
        
        func xibSetup()
        {
            view = loadViewFromNib()
            // use bounds not frame or it'll be offset
            view.frame = bounds
            
            // Make the view stretch with containing view
            view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
            
            
            
            
            // Adding custom subview on top of our view (over any custom drawing > see note below)
            addSubview(view)
        }
        
        func loadViewFromNib() -> UIView
        {
            let bundle = Bundle(for: type(of: self))
            let nib = UINib(nibName: "trackOrderView", bundle: bundle)
            
            // Assumes UIView is top level and only object in CustomView.xib file
            let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
            return view
        }

    }
