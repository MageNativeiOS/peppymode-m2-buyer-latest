//
//  CancelOrderView.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 26/02/21.
//  Copyright © 2021 CEDCOSS Technologies Private Limited. All rights reserved.
//

import Foundation
class CancelOrderView : UIView
{
    @IBOutlet weak var cancelReasonsHeading: UILabel!
    
    @IBOutlet weak var commentsHeading: UILabel!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var resetButton: UIButton!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var submitButton: UIButton!
    
    @IBOutlet weak var headingLabel: UILabel!
    
    @IBOutlet weak var commentsTextView: UITextView!
    @IBOutlet weak var reasonsButton: UIButton!
    
    var view : UIView!
    override init(frame: CGRect)
    {
    
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    func xibSetup()
    {
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        addSubview(view)
        
    }
    
    func loadViewFromNib() -> UIView{
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "CancelOrderView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
}
