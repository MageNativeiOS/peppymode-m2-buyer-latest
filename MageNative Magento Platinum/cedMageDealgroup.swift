//
//  cedMageDealgroup.swift
//  MageNative Magento Platinum
//
//  Created by CEDCOSS Technologies Private Limited on 24/10/16.
//  Copyright © 2016 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class cedMageDealgroup: UIView {

    @IBOutlet weak var dealGroupTitle: UILabel!
    @IBOutlet weak var timerTitle: UILabel!
    
    var view:UIView!
    var dealtime = 100
    var data = true
    var timer = Timer()
    var startTime = TimeInterval()
    var timerStatus = String()
    var prevtext:String?
    override init(frame: CGRect)
    {
        // 1. setup any properties here
        
        // 2. call super.init(frame:)
        super.init(frame: frame)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        // 1. setup any properties here
        
        // 2. call super.init(coder:)
        super.init(coder: aDecoder)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    func xibSetup()
    {
        view = loadViewFromNib()
        dealGroupTitle.fontColorTool()
        timerTitle.fontColorTool()
        
        
        // use bounds not frame or it'll be offset
        view?.frame = bounds
        
        // Make the view stretch with containing view
        view?.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        //view.setThemeColor()
    
        if(timerStatus == "2"){
            
        }else{
            Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(HomepageDealsCell.update), userInfo: nil, repeats: true)
        }
        
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view!)
    }
    
    func loadViewFromNib() -> UIView
    {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "cedMageDealgroup", bundle: bundle)
        
        // Assumes UIView is top level and only object in CustomView.xib file
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
  @objc  func update() {
        if(startTime>0){
            var time = Int64(startTime)
            time = time / 1000
            let seconds = time % 60
            let minutes = (time / 60) % 60
            let hours = (time / 3600)
            
            timerTitle.text =  String(format: "%0.2d H:%0.2d M:%0.2d S",hours,minutes,seconds)
            startTime = startTime - 1000
        }
        
    }
   
}
