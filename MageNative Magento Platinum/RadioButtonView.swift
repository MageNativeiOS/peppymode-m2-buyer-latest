//
//  RadioButtonView.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 11/10/18.
//  Copyright © 2018 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class RadioButtonView: UIView {

    // Our custom view from the XIB file
    var view: UIView!
    
    @IBOutlet weak var radioButton: UIButton!
    
  
    
    
    
    
    override init(frame: CGRect)
    {
        // 1. setup any properties here
        
        // 2. call super.init(frame:)
        super.init(frame: frame)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        // 1. setup any properties here
        
        // 2. call super.init(coder:)
        super.init(coder: aDecoder)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    func xibSetup()
    {
        view = loadViewFromNib()
        
        // use bounds not frame or it'll be offset
        view.frame = bounds
        
        // Make the view stretch with containing view
        view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        
        //extra setup
        //childOptionLabel.font = UIFont(fontName: "HelveticaNeue-Medium", fontSize: CGFloat(12.0));
        //childOptionButton.layer.cornerRadius = 0.5 * childOptionButton.bounds.height;
        
        //let color = cedMage.getInfoPlist(fileName:"cedMage",indexString: "themeColor") as! String
        //let theme_color = cedMage.UIColorFromRGB(colorCode: color);
        //childOptionButton.backgroundColor = UIColor.blue;
        //childOptionButton.layer.borderWidth =  CGFloat(2);
        //childOptionButton.layer.borderColor = theme_color.cgColor;
        
        
        //extra setup
        
        radioButton.setImage(UIImage(named: "uncheckedRadio"), for: .normal)
        self.view.layer.cornerRadius = 5.0
        self.view.clipsToBounds = true
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView
    {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "RadioButtonView", bundle: bundle)
        
        // Assumes UIView is top level and only object in CustomView.xib file
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }

}
