//
//  productListView.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 30/09/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import Foundation

class productListView: UIView {
    var view: UIView!
    
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var subTotalLbl: UILabel!
    @IBOutlet weak var qtyLbl: UILabel!
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        xibSetup()
    }
    func xibSetup()
       {
           view = loadViewFromNib()
           
           // use bounds not frame or it'll be offset
           view.frame = bounds
           
           // Make the view stretch with containing view
           
        
           addSubview(view)
       }
       
       
       
        func loadViewFromNib() -> UIView
          {
              let bundle = Bundle(for: type(of: self))
              let nib = UINib(nibName: "productListView", bundle: bundle)
              
              // Assumes UIView is top level and only object in CustomView.xib file
              let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
              return view
          }
}
