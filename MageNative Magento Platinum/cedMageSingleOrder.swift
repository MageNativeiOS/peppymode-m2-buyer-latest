/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */

import UIKit


class cedMageSingleOrder: cedMageViewController,UITableViewDelegate,UITableViewDataSource,WKNavigationDelegate {

    @IBOutlet weak var orderStatusLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    var orderId:String?
    var otherData = [String:String]()
    var cancelOrderData = [String:String]()
    var cancelOrderReasons = [String]()
    var orderItemsDict = [[String:String]]()
    var optionArray = [Int:[Int:[String:String]]]();
    var bgView = UIView()
    var commentText = String()
    var optionsText = String()
    let cancelOrderView = CancelOrderView()
    
    let popUpView = trackOrderView()
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
        let hashKey = userInfoDict["hashKey"]!;
        let  customerId = userInfoDict["customerId"]!;
         let postString = ["hashkey":hashKey,"customer_id":customerId,"order_id":orderId!];
        print(postString)
        tableView.register(ReturnButtonTC.self, forCellReuseIdentifier: ReturnButtonTC.trackOrderId)
        tableView.register(ReturnButtonTC.self, forCellReuseIdentifier: ReturnButtonTC.reuseID)
        tableView.register(ReturnButtonTC.self, forCellReuseIdentifier: ReturnButtonTC.cancelId)
        tableView.register(StorePickupTC.self, forCellReuseIdentifier: StorePickupTC.reuseID)
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 250
        self.sendRequest(url: "mobiconnect/customer/orderview", params: postString)
        popUpView.trackWebView.navigationDelegate = self
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
        if let data = data {
        var json = try? JSON(data: data);
            json = json?[0]
        print(json);
        //print(json["data"]["address"]);
        
            if requestUrl == "mobiconnect/mobirma/generaterma" {
                let vc = RmaReasonController()
                vc.json = json
                vc.orderID = self.orderId ?? ""
                navigationController?.pushViewController(vc, animated: true)
                return
            }
            else if requestUrl == "mobiconnect/customer/cancelOrder"
            {
                self.view.makeToast(json!["data"]["message"].stringValue, duration: 2.0, position: .center)
                cedMage.delay(delay: 2.0) {
                let viewcontroll = UIStoryboard(name: "cedMageAccounts", bundle: nil).instantiateViewController(withIdentifier: "cedMageOrders") as! cedMageMyOrders
                self.navigationController?.pushViewController(viewcontroll, animated: true)
                }
            }
            else if requestUrl == "mobiconnect/customer/trackOrder/"{
                if (json?["data"]["status"].boolValue)!{
                    if let trackInfo = json?["data"]["tracking_info"].arrayValue{
                    for i in trackInfo{
                        self.popUpView.infoTitle.text = i["info"]["carrier"].stringValue
                        self.popUpView.shipmentID.text = "Shipment #"+i["shipment_id"].stringValue
                        self.popUpView.infoNumber.text = i["info"]["tracking_number"].stringValue
                        
                        let prodDes = """
                        <html><head><meta name='viewport' content='width=device-width,
                        initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0'><link rel="stylesheet" type="text/css" href="webfile.css">
                        <span style="font-family: 'Roboto-Regular'; font-size: 16px;"><font color="black">\(i["info"]["summary"].stringValue)</span></head></html>
                        """
                        self.popUpView.trackWebView.loadHTMLString(prodDes, baseURL: URL(fileURLWithPath: Bundle.main.path(forResource: "webfile", ofType: "css") ?? "" ))
                      //  self.popUpView.trackWebView
                    }
                    }
                    self.openTrackOrder()
                }
            }else{
            otherData["nameOnCard"] = json?["data"]["orderview"][0]["name_on_card"].stringValue;
            otherData["country"] = json?["data"]["orderview"][0]["country"].stringValue;
            otherData["shipping"] = json?["data"]["orderview"][0]["shipping"].stringValue;
            otherData["orderDate"] = json?["data"]["orderview"][0]["orderdate"].stringValue;
            otherData["street"] = json?["data"]["orderview"][0]["street"].stringValue;
            otherData["methodTitle"] = json?["data"]["orderview"][0]["method_title"].stringValue;
            otherData["discount"] = json?["data"]["orderview"][0]["discount"].stringValue;
            otherData["shippingMethod"] = json?["data"]["orderview"][0]["shipping_method"].stringValue;
            otherData["city"] = json?["data"]["orderview"][0]["city"].stringValue;
            otherData["state"] = json?["data"]["orderview"][0]["state"].stringValue;
            otherData["taxAmount"] = json?["data"]["orderview"][0]["tax_amount"].stringValue;
            otherData["grandtotal"] = json?["data"]["orderview"][0]["grandtotal"].stringValue;
            otherData["pincode"] = json?["data"]["orderview"][0]["pincode"].stringValue
                otherData["show_rma"] = json?["data"]["orderview"][0]["show_rma"].stringValue
            otherData["subtotal"] = json?["data"]["orderview"][0]["subtotal"].stringValue;
            otherData["showButton"] = json?["data"]["orderview"][0]["show_rma_btn"].stringValue;
            
            otherData["mobile"] = json?["data"]["orderview"][0]["mobile"].stringValue;
            otherData["method_code"] = json?["data"]["orderview"][0]["method_code"].stringValue;
            otherData["show_tracking"] = json?["data"]["orderview"][0]["show_tracking"].stringValue;
            otherData["credit_card_type"] = json?["data"]["orderview"][0]["credit_card_type"].stringValue;
            
            
            otherData["ship_to"] = json?["data"]["orderview"][0]["ship_to"].stringValue;
            
            
            otherData["credit_card_number"] = json?["data"]["orderview"][0]["credit_card_number"].stringValue;
            
            
            otherData["orderlabel"] = json?["data"]["orderview"][0]["orderlabel"].stringValue;
            
           self.title =  otherData["orderlabel"]
                
                cancelOrderData["label"] = json?["data"]["orderview"][0]["cancel_order"]["label"].stringValue
                cancelOrderData["note"] = json?["data"]["orderview"][0]["cancel_order"]["note"].stringValue
                cancelOrderData["show_button"] = json?["data"]["orderview"][0]["cancel_order"]["show_button"].stringValue
        //        cancelOrderData["show_comments"] = "false"
                cancelOrderData["show_comments"] = json?["data"]["orderview"][0]["cancel_order"]["show_comments"].stringValue
      //          json?["data"]["orderview"][0]["cancel_order"]["reasons"]
                for i in 0..<(json?["data"]["orderview"][0]["cancel_order"]["reasons"].count)!
                {
                    print(json?["data"]["orderview"][0]["cancel_order"]["reasons"][i]["label"])
                    cancelOrderReasons.append((json?["data"]["orderview"][0]["cancel_order"]["reasons"][i]["label"].stringValue)!)
                }
                print(cancelOrderData)
                print(cancelOrderReasons)
                
        
            let orderedItemsJSON = json?["data"]["orderview"][0]["ordered_items"];
             print(orderedItemsJSON)
            var tempOption = [String:String]();
            var indexedTempOption = [Int:[String:String]]();
            var inrCounter = 0;
        
            for c in (0..<orderedItemsJSON!.count) {
                print("source");
                tempOption = [String:String]();
            
                print(orderedItemsJSON?[c]["option"])
                
                inrCounter = 0;
                for val in orderedItemsJSON?[c]["option"].arrayValue ?? []
                {
                
                    for (inrKey,inrVal) in val
                    {
                        print(inrKey);
                        print(inrVal);
                        tempOption[inrKey] = inrVal.stringValue;
                    }
                    
                    indexedTempOption[inrCounter] = tempOption;
                    inrCounter += 1;
                }
                self.optionArray[c] = indexedTempOption;
                let product_id=orderedItemsJSON?[c]["product_id"].stringValue;
                let product_qty=orderedItemsJSON?[c]["product_qty"].stringValue;
                let rowsubtotal=orderedItemsJSON?[c]["rowsubtotal"].stringValue;
                let phone=orderedItemsJSON?[c]["phone"].stringValue;
                let item_gift_detail=orderedItemsJSON?[c]["item-gift-detail"].stringValue;
                let product_image=orderedItemsJSON?[c]["product_image"].stringValue;
                let product_price=orderedItemsJSON?[c]["product_price"].stringValue;
                let product_name=orderedItemsJSON?[c]["product_name"].stringValue;
                let product_type=orderedItemsJSON?[c]["product_type"].stringValue;
                let v = ["product_id":product_id,"product_qty":product_qty,"rowsubtotal":rowsubtotal,"phone":phone,"item_gift_detail":item_gift_detail,"product_image":product_image,"product_price":product_price,"product_name":product_name,"product_type":product_type];
                self.orderItemsDict.append(v as! [String : String]);
                
            }
              
        orderStatusLabel.text =  otherData["orderlabel"]
        tableView.reloadData()
        }
        }
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
        case 0:
            if (orderItemsDict.count != 0) { return 5+orderItemsDict.count }
            return 0;
        case 2:
            return 1
        default:
            if otherData["show_tracking"] == "yes"{
                return 2
            }else if otherData["show_rma"] == "true"{
                return 1
            }else{
                return 0
            }
        }
        
        
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            if(indexPath.row == 0){
                let cell = tableView.dequeueReusableCell(withIdentifier: "orderCell") as! cedMageSingleOrderCell
                cell.headingLabel.text = "ORDER DATE".localized
                cell.label.text = otherData["orderDate"]!
                
                return cell
            }
            else if(indexPath.row == 1)
            {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "orderCell") as! cedMageSingleOrderCell
                
                var shippingAddress = otherData["ship_to"]!+"\n";
                shippingAddress += otherData["street"]!+"\n";
                shippingAddress += otherData["city"]!+"\n";
                shippingAddress += otherData["state"]!+"\n";
                shippingAddress += otherData["pincode"]!+"\n";
                shippingAddress += otherData["country"]!+"\n";
                shippingAddress += otherData["mobile"]!+"\n";
                
                cell.headingLabel.text = "SHIPPING ADDRESS".localized
                cell.label.text = shippingAddress
                cell.contentView.cardView()
                return cell
            }
            else if(indexPath.row == 2)
            {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "orderCell") as! cedMageSingleOrderCell
                cell.headingLabel.text = "SHIPPING METHOD".localized
                
                cell.label.text  = self.otherData["shippingMethod"]!;
                cell.contentView.cardView()
                return cell
            }
            else if(indexPath.row == 3)
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "orderCell") as! cedMageSingleOrderCell
                
                cell.headingLabel.text = "PAYMENT METHOD".localized
                cell.label.text  = self.otherData["methodTitle"]!;
                cell.contentView.cardView()
                return cell
                
            }
            else if(indexPath.row == 4+orderItemsDict.count)
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "orderCell") as! cedMageSingleOrderCell
                cell.headingLabel.text = "Total Price".localized
                var stringToShow = "Sub Total :".localized+"\t"
                stringToShow  += self.otherData["subtotal"]!+"\n";
                stringToShow += "Tax Price : ".localized+"\t"
                stringToShow  += self.otherData["taxAmount"]!+"\n";
                stringToShow += "Shipping : ".localized+"\t"
                stringToShow  += self.otherData["shipping"]!+"\n";
                stringToShow += "Discount : ".localized+"\t"
                stringToShow  += self.otherData["discount"]!+"\n";
                stringToShow += "Grand Total:".localized+"\t"
                stringToShow  += self.otherData["grandtotal"]!;
                cell.label.text = stringToShow
                
                return cell
            }
            else
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "productCell") as! cedMageOrdrProductCell
                print(optionArray )
                let title = orderItemsDict[indexPath.row-4]["product_name"]!+"\n";
                var proInfo = "Price : ".localized
                proInfo += orderItemsDict[indexPath.row-4]["product_price"]!+"\n";
                proInfo += "Qty : ".localized+orderItemsDict[indexPath.row-4]["product_qty"]!+"\n";
                proInfo += "Amount : ".localized+orderItemsDict[indexPath.row-4]["rowsubtotal"]!+"\n";
                
                if(orderItemsDict[indexPath.row-4]["product_type"]! == "bundle")
                {
                    proInfo += "\n";
                    if(self.optionArray[indexPath.row-4] != nil)
                    {
                        let loopArr = self.optionArray[indexPath.row-4]!;
                        print(loopArr);
                        for (key,val) in loopArr
                        {
                            print(key);
                            print(val);
                            proInfo += val["option_title"]!+" : ";
                            proInfo += val["option_value"]!+"\n";
                            proInfo += "\n";
                        }
                        
                    }
                }
                if(orderItemsDict[indexPath.row-4]["product_type"]! == "configurable")
                {
                    proInfo += "\n";
                    if(self.optionArray[indexPath.row-4] != nil)
                    {
                        let loopArr = self.optionArray[indexPath.row-4]!;
                        for (_,val) in loopArr
                        {
                            proInfo += val["option_title"]!+" : ";
                            proInfo += val["option_value"]!+"\n";
                            proInfo += "\n";
                        }
                        
                    }
                }
                
                if(orderItemsDict[indexPath.row-4]["product_type"]! == "downloadable")
                {
                    proInfo += "\n";
                    if(self.optionArray[indexPath.row-4] != nil)
                    {
                        let loopArr = self.optionArray[indexPath.row-4]!;
                        
                        print(loopArr);
                        
                        for (key,val) in loopArr
                        {
                            print(key);
                            print(val);
                            if(val["link_title"] != nil){
                                proInfo += val["link_title"]!
                            }
                            proInfo += "\n";
                        }
                        
                    }
                }
                cell.productTitle.setTitle(title, for: .normal)
                
                cell.productDesc.text = proInfo;
                cedMageImageLoader.shared.loadImgFromUrl(urlString: orderItemsDict[indexPath.row-4]["product_image"]!,completionHandler: { (image: UIImage?, url: String) in
                    DispatchQueue.main.async {
                        cell.productImage.image = image
                    }
                })
                
                return cell;
                
                
            }
        case 2:
            if cancelOrderData["show_button"] == "true"
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: ReturnButtonTC.cancelId, for: indexPath) as! ReturnButtonTC
                cell.returnButton.setTitle(cancelOrderData["label"], for: .normal)
                cell.returnButton.addTarget(self, action: #selector(cancelOrderButtonTapped(_:)), for: .touchUpInside)
                return cell
            }
            
        default:
            //            if indexPath.row == 0 {
            //                let cell = tableView.dequeueReusableCell(withIdentifier: StorePickupTC.reuseID, for: indexPath) as! StorePickupTC
            //                return cell
            //            } else {
            if otherData["show_tracking"] == "yes"{
                if indexPath.row == 0{
                    let cell = tableView.dequeueReusableCell(withIdentifier: ReturnButtonTC.trackOrderId, for: indexPath) as! ReturnButtonTC
                    cell.returnButton.addTarget(self, action: #selector(trackOrderTapped(_:)), for: .touchUpInside)
                    cell.returnButton.setTitle("Track Your Order", for: .normal)
                    return cell
                }else{
                    let cell = tableView.dequeueReusableCell(withIdentifier: ReturnButtonTC.reuseID, for: indexPath) as! ReturnButtonTC
                    cell.returnButton.setTitle("REQUEST RETURN", for: .normal)
                    cell.returnButton.addTarget(self, action: #selector(returnButtonTapped(_:)), for: .touchUpInside)
                    return cell
                }}else{
                let cell = tableView.dequeueReusableCell(withIdentifier: ReturnButtonTC.reuseID, for: indexPath) as! ReturnButtonTC
                cell.returnButton.setTitle("REQUEST RETURN", for: .normal)
                cell.returnButton.addTarget(self, action: #selector(returnButtonTapped(_:)), for: .touchUpInside)
                return cell
            }
            
        }
        return UITableViewCell()
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        switch indexPath.section {
        case 0:
            if(indexPath.row == 0 || indexPath.row == 2 || indexPath.row == 3)
            {
                return 70;
            }
            else if(indexPath.row == 1)
            {
                return 180;
            }
            else if(indexPath.row >= 4 && indexPath.row <= 3+orderItemsDict.count)
            {
                if(orderItemsDict[indexPath.row-4]["product_type"]! == "bundle" || orderItemsDict[indexPath.row-4]["product_type"]! == "configurable")
                {
                    return 200;
                }
                
                if(orderItemsDict[indexPath.row-4]["product_type"]! == "downloadable")
                {
                    return 180;
                }
                
                return 140;
            }
            else
            {
                return UITableView.automaticDimension
            }
            
        case 2 :
            if cancelOrderData["show_button"] == "true"
            {
                return 55
            }
            else
            {
                return 0
            }
        default:
          //  if indexPath.row == 0 { return 250 }
            return 55
        }
    }
    
    
    @objc func cancelOrderButtonTapped(_ sender : UIButton)
    {
        print("cancel")
        if (cancelOrderData["show_comments"] == "true")
        {
        cancelOrderView.frame = CGRect(x:10, y: self.view.center.y - 200, width: UIScreen.main.bounds.width - 20, height: 400)
        }
        else
        {
            cancelOrderView.frame = CGRect(x:10, y: self.view.center.y - 200, width: UIScreen.main.bounds.width - 20, height: 400)
            cancelOrderView.commentsTextView.isHidden = true
            cancelOrderView.commentsHeading.isHidden = true
        }
        
        cancelOrderView.layer.borderWidth = 2.0
        cancelOrderView.layer.borderColor = UIColor.black.cgColor
        cancelOrderView.tag = 121212
        cancelOrderView.headingLabel.text = cancelOrderData["label"]
        cancelOrderView.closeButton.addTarget(self, action: #selector(closeButtonTapped(_:)), for: .touchUpInside)
        cancelOrderView.reasonsButton.layer.borderWidth = 1.0
        cancelOrderView.reasonsButton.layer.borderColor = UIColor.black.cgColor
        cancelOrderView.reasonsButton.addTarget(self, action: #selector(reasonsButtonPressed(_:)), for: .touchUpInside)
        cancelOrderView.commentsTextView.layer.borderWidth = 1.0
        cancelOrderView.commentsTextView.layer.borderColor = UIColor.black.cgColor
        
        cancelOrderView.textLabel.text = cancelOrderData["note"]
        cancelOrderView.textLabel.numberOfLines = 0
        cancelOrderView.submitButton.addTarget(self, action: #selector(submitButtonPressed(_:)), for: .touchUpInside)
        cancelOrderView.resetButton.addTarget(self, action: #selector(resetButtonPressed(_:)), for: .touchUpInside)
        self.view.addSubview(cancelOrderView)
    }
    
    @objc func submitButtonPressed(_ sender : UIButton)
    {
//        self.commentText =
//        self.optionsText =
        let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
        let hashKey = userInfoDict["hashKey"]!;
        let  customerId = userInfoDict["customerId"]!;
        let comments = cancelOrderView.commentsTextView.text
        let cancelreason = cancelOrderView.reasonsButton.currentTitle!
        let storeId = defaults.value(forKey: "storeId") as? String ?? ""
        let postString = ["hashkey":hashKey,"customer_id":customerId,"order_id":orderId!,"comment":comments!,"cancel_reasons":cancelreason,"store_id":storeId];
        print(postString)
        self.sendRequest(url: "mobiconnect/customer/cancelOrder", params: postString)
        self.view.viewWithTag(121212)?.removeFromSuperview()
    }
    
    @objc func resetButtonPressed(_ sender : UIButton)
    {
        cancelOrderView.reasonsButton.setTitle("--Select--", for: .normal)
        cancelOrderView.commentsTextView.text = ""
    }
    
    var dropDown = DropDown()
    @objc func reasonsButtonPressed(_ sender : UIButton)
    {
        dropDown.dataSource = cancelOrderReasons
        dropDown.anchorView = sender
        dropDown.bottomOffset = CGPoint(x: 0, y: sender.frame.size.height)
        dropDown.show()
        dropDown.selectionAction = { [weak self] (index: Int, item: String) in
          guard let _ = self else { return }
          sender.setTitle(item, for: .normal)
        }
    }
    
    @objc func closeButtonTapped(_ sender : UIButton)
    {
        self.view.viewWithTag(121212)?.removeFromSuperview()
    }
    
    
    @objc func returnButtonTapped(_ sender: UIButton) {
        print("return")
        let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
        guard let customerID = userInfoDict["customerId"] else { return }
        
        var param = [String:String]()
        param["customer_id"] = customerID
        param["order_id"] = orderId
        
        self.sendRequest(url: "mobiconnect/mobirma/generaterma", params: param, store: false)
    }
    
    @objc func trackOrderTapped(_ sender:UIButton){
        let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
        let hashKey = userInfoDict["hashKey"]!;
        let  customerId = userInfoDict["customerId"]!;
         let postString = ["hashkey":hashKey,"customer_id":customerId,"order_id":orderId!];
        self.sendRequest(url: "mobiconnect/customer/trackOrder/", params: postString)
    }
    func openTrackOrder(){

         bgView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
         bgView.tag = 11
         bgView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
         self.view.addSubview(bgView)
         
         
         popUpView.frame = CGRect(x: 0, y: 0, width: bgView.frame.width - 40 , height: 350)
         popUpView.center = bgView.center
         bgView.addSubview(popUpView)
         
         popUpView.closeBtn.addTarget(self, action: #selector(dismissPopup(_:)), for: .touchUpInside)
        
    }
       @objc func dismissPopup(_ sender:UIButton){
           self.bgView.removeFromSuperview()
       }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
         for view in webView.subviews as [UIView] {
             if let activityIndicator = view as? UIActivityIndicatorView {
                 activityIndicator.removeFromSuperview();
             }
         }
     }
}
