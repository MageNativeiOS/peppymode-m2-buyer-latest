//
//  HomeWidgetBannerCC.swift
//  MageNative Magento Platinum
//
//  Created by Hamza Usmani on 21/09/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class HomeWidgetBannerCC: UICollectionViewCell {
    
    //MARK: -  Properties
    
    static var reuseID:String = "HomeWidgetBannerCC"
    
    //MARK: - Views
    
    lazy var bannerImage:UIImageView = {
        let imageView = UIImageView()
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleToFill
        imageView.backgroundColor = .mageSecondarySystemBackground
        return imageView
    }()
    
    //MARK: - Life Cycle
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(bannerImage)
        bannerImage.anchor(top: topAnchor, left: leadingAnchor, bottom: bottomAnchor, right: trailingAnchor, paddingTop: 2, paddingLeft: 2, paddingBottom: 2, paddingRight: 2)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Helper functions
    
    func populate(with banner: HomeBanner) {
        bannerImage.sd_setImage(with: URL(string: banner.banner_image ?? ""), placeholderImage: UIImage(named: "placeholder"))
    }
    
}

