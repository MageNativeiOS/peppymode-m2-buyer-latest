/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */


import UIKit
import SceneKit
class cedMageHomeDefault: cedMageViewController,UITableViewDelegate,UITableViewDataSource,KIImagePagerDelegate,KIImagePagerDataSource {
  

    

    @IBOutlet weak var homeDefaultTable: UITableView!
    var featuredProducts = [[String:String]]()
    var newArrivalProducts = [[String:String]]()
    var mostViewedProducts = [[String:String]]()
    var bestSellingProducts = [[String:String]]()
    var bannerImages       = [[String:String]]()
    var banner_image_array = [String]()
    var productData     = [String:[[String:String]]]()
    var dealGroup = [[String:String]]()
    var deals     = [[String:String]]()
    var dealsPro     = [String:[[String:String]]]()
    var bounds = UIScreen.main.bounds
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    
     
        homeDefaultTable.delegate = self
        homeDefaultTable.dataSource = self
        self.sendRequest(url: "mobiconnect/index/gethomepagebanner", params: nil)
        self.sendRequest(url: "mobiconnect/index/gethomepageNewArrival", params: nil)
        self.sendRequest(url: "mobiconnect/index/gethomepagebestselling", params: nil)
        self.sendRequest(url: "mobiconnect/index/gethomepagemostview", params: nil)
        self.sendRequest(url: "mobiconnect/index/gethomepagefeaturedproduct", params: nil)
        if(cedMage.checkModule(string: "Ced_Mobiconnectdeals")){
            self.sendRequest(url: "mobiconnectdeals/mobideals/getdealgroup", params: nil)
        }
        
        //cedMageLoaders.addImageFliploader(me: self)
    }
    
    override func recieveResponse(data: Data?, requestUrl:String? , response: URLResponse?) {
        guard let json = try? JSON(data: data!) else{
            return;
        }
        if(requestUrl == "mobiconnect/index/gethomepageNewArrival"){
            self.parseJsonData(json: json, index: "new_arrival")
        }else if(requestUrl == "mobiconnect/index/gethomepagebestselling"){
            self.parseJsonData(json: json, index: "best_selling")
        }
        else if(requestUrl == "mobiconnect/index/gethomepagemostview"){
            self.parseJsonData(json: json, index: "most_view")
        }else if(requestUrl == "mobiconnect/index/gethomepagefeaturedproduct"){
            self.parseJsonData(json: json, index: "featured_products")
        }else if(requestUrl == "mobiconnect/index/gethomepagebanner"){
            self.parseJsonData(json: json, index: "banner")
            self.banner_image_array = [String]()
            for image in self.bannerImages{
                self.banner_image_array.append(image["banner_image"]!)
                
            }
        }else if(requestUrl == "mobiconnectdeals/mobideals/getdealgroup"){
            self.parseJsonData(json: json, index: "deal_products")
        }
        homeDefaultTable.reloadData()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Table View Delegate Functions
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section == 0){
            if banner_image_array.count == 0 {
                return 0
            }
            return 1
        }
        if(section == 1){
            return dealGroup.count
        }
        if(section == 2){
            if(bestSellingProducts.count != 0){
                return 1
            }
            return 0
        }else if(section == 3){
            
            if(featuredProducts.count != 0){
                return 1
            }
            return 0
        } else if(section == 4){
            if(mostViewedProducts.count != 0){
                return 1
            }
            return 0
        } else if(section == 5){
            if(newArrivalProducts.count != 0){
                return 1
            }
            return 0
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.section == 0){
            let cell = homeDefaultTable.dequeueReusableCell(withIdentifier: "bannerCell") as! homePageBannerCell
            
            cell.banner_image_array = banner_image_array
            cell.bannerPage.delegate = self
            cell.bannerPage.dataSource = self
            cell.bannerPage.slideshowTimeInterval = UInt(2.5)
            cell.bannerPage.imageCounterDisabled = true
            //cell.bannerPage.pageControl.isHidden = true
            return cell
            
        } else if(indexPath.section == 1){
            let cell = tableView.dequeueReusableCell(withIdentifier: "dealgroup") as! HomepageDealsCell
            if(cell.data){
                cell.data = false
                cell.dealtitle.text = " " + dealGroup[indexPath.row]["groupTitle"]!
                let string : String = dealGroup[indexPath.row]["dealGroupDuration"]!
                let timeinterval : TimeInterval = (string as NSString).doubleValue
                // print(timeinterval)
                let gropImageUrl = dealGroup[indexPath.row]["groupImagename"]!
                cell.dealGroupImage.contentMode = .scaleToFill
                cedMageImageLoader().loadImgFromUrl(urlString: gropImageUrl, completionHandler: {
                    image,url in
                  //  cell.dealGroupImage.image = image
                })
                cell.parent = self
                if(dealGroup[indexPath.row]["timer_status"] != "1"){
                    cell.timerStatus = "2"
                    cell.dealTimer.text = ""
                }else{
                    cell.startTime = (timeinterval)
                }
                //cell.deals = deals
                cell.deals = dealsPro[String(indexPath.row+1)]!;
            }
            return cell
        }
        
        else  if(indexPath.section == 2){
            let cell = homeDefaultTable.dequeueReusableCell(withIdentifier: "homeDefaultCell") as! cedMageDefaultHomeCell
            if(cell.data){
                print(productData)
                cell.parent = self
                cell.data = false
                cell.sbounds = bounds
                cell.headingLabel.text = "Best Seller".localizedUppercase
                cell.datasource =  self.bestSellingProducts
            }
            return cell
        }else  if(indexPath.section == 3 ){
            let cell = homeDefaultTable.dequeueReusableCell(withIdentifier: "celltwo") as! cedMageDefaultHomeCell
            if(cell.data){
                cell.data = false
                cell.parent = self
                cell.sbounds = bounds
                cell.headingLabel.text = "Featured Products".localizedUppercase
                cell.datasource = self.featuredProducts
                print("^\(self.featuredProducts)")
            }
            return cell
        }else  if(indexPath.section == 4){
            let cell = homeDefaultTable.dequeueReusableCell(withIdentifier: "cellthree") as! cedMageDefaultHomeCell
            if(cell.data){
                cell.data = false
                cell.parent = self
                cell.sbounds = bounds
                cell.headingLabel.text = "Most Viewed".localizedUppercase
                cell.datasource = self.mostViewedProducts
            }
            return cell
        }else {
            let cell = homeDefaultTable.dequeueReusableCell(withIdentifier: "cellfour") as! cedMageDefaultHomeCell
            if(cell.data){
                cell.data = false
                cell.parent = self
                cell.sbounds = bounds
                cell.headingLabel.text = "New Arrivals".localizedUppercase
                cell.datasource = self.newArrivalProducts
            }
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(UIDevice().model.lowercased() == "ipad".lowercased()){
            return bounds.height/2
        }
        if(indexPath.section == 0){
            return bounds.height/2.2
        }
        if(indexPath.section == 1){
            return   bounds.height/2.5
        }else if(indexPath.section == 2){
            return   bounds.height/2 * CGFloat(bestSellingProducts.count/2) - 50
        }else if(indexPath.section == 3){
            return   bounds.height/2 * CGFloat(featuredProducts.count/2) - 50
        }else if(indexPath.section == 4){
            return   bounds.height/2 * CGFloat(mostViewedProducts.count/2) - 50
        }
        return   bounds.height/2 * CGFloat(newArrivalProducts.count/2) - 50
    }
    
    
    
    //MARK: Delegate for KIImage Pager
    
    public func array(withImages pager: KIImagePager!) -> [Any]! {
        if banner_image_array.count == 0 {
            return ["placeholder" as AnyObject]
        }
        return banner_image_array as [AnyObject]?
    }
    
    /*func array(withImages pager: KIImagePager!) -> [AnyObject]! {
        if banner_image_array.count == 0 {
            return ["placeholder" as AnyObject]
        }
        return banner_image_array as [AnyObject]!
    }*/
    
    func contentMode(forImage image: UInt, in pager: KIImagePager!) -> UIView.ContentMode {
        return UIView.ContentMode.scaleToFill
    }
    
    func placeHolderImage(for pager: KIImagePager!) -> UIImage! {
        return UIImage(named: "placeholder")
    }
    
    func imagePager(_ imagePager: KIImagePager!, didSelectImageAt index: UInt) {
        
    }
    
    //MARK: Parse Product Data from json response
    private func parseJsonData(json:JSON,index:String){
        
        if(index == "banner"){
            for result in json["data"][index].arrayValue {
                let product_id = result["product_id"].stringValue
                let id = result["id"].stringValue
                let title = result["title"].stringValue
                let link_to = result["link_to"].stringValue
                let banner_image = result["banner_image"].stringValue
                let products_obj = ["product_id":product_id, "title":title,"link_to":link_to,"banner_image":banner_image,"id":id]
                bannerImages.append(products_obj)
                
            }
            return
                print("^\(bannerImages)")
        }
        
        if(index == "deal_products"){
            var j = 1
            for result in json["data"]["deal_products"].arrayValue {
                let dealgropstart = result["group_start_date"].stringValue
                let groupcreated = result["created_time"].stringValue
                let group_end_date = result["group_end_date"].stringValue
                let dealGroupDuration = result["deal_duration"].stringValue
                let groupImagename = result["group_image_name"].stringValue
                let group_status = result ["group_status"].stringValue
                let groupTitle  = result["title"].stringValue
                let group_id  = result["group_id"].stringValue
                let timer_status = result["timer_status"].stringValue
                let view_all_status = result["view_all_status"].stringValue
                let update_time = result["update_time"].stringValue
                let is_static  = result["is_static"].stringValue
                let deal_link  = result["deal_link"].stringValue
                let dealObject = ["dealgropstart":dealgropstart,"groupcreated":groupcreated,"group_end_date":group_end_date,"dealGroupDuration":dealGroupDuration,"groupImagename":groupImagename,"group_status":group_status,"groupTitle":groupTitle,"group_id":group_id,"timer_status":timer_status,"view_all_status":view_all_status,"update_time":update_time,"is_static":is_static,"deal_link":deal_link]
                dealGroup.append(dealObject)
                
                var i = 0
                for deal in result["content"].arrayValue{
                    let id = deal["id"].stringValue
                    let start_date = deal["start_date"].stringValue
                    let deal_image_name = deal["deal_image_name"].stringValue
                    let relative_link = deal["relative_link"].stringValue
                    let deal_type = deal["deal_type"].stringValue
                    let deal_title = deal["deal_title"].stringValue
                    let category_link = deal["category_link"].stringValue
                    let offer_text = deal["offer_text"].stringValue
                    let status  = deal["status"].stringValue
                    let product_link = deal["product_link"].stringValue
                    
                    let deaks_obj = ["id":id, "start_date":start_date,"deal_image_name":deal_image_name,"relative_link":relative_link,"deal_type":deal_type,"deal_title":deal_title,"category_link":category_link,"offer_text":offer_text,"status":status,"product_link":product_link]
                    deals.append(deaks_obj)
                    i += 1
                }
                dealsPro["\(j)"] = deals;
                deals.removeAll()
                j += 1
            }
        }
        else {
            let status = json["data"]["status"].stringValue
            for result in json["data"][index].arrayValue {
                let product_id = result["product_id"].stringValue
                let product_name = result["product_name"].stringValue
                let product_price = result["product_price"].stringValue
                let product_image = result["product_image"].stringValue
                let products_obj = ["product_id":product_id, "product_price":product_price,"product_image":product_image,"product_name":product_name,"status":status]
                if(index == "best_selling"){
                    bestSellingProducts.append(products_obj)
                }
                else if(index == "most_view"){
                    mostViewedProducts.append(products_obj)
                }
                else if(index == "new_arrival"){
                    newArrivalProducts.append(products_obj)
                }else if(index == "featured_products"){
                    
                    featuredProducts.append(products_obj)
                    print("^ \(featuredProducts)")
                }
                
            }
        }
        
        
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if(section == 1){
            
            let view = UIView()
            let label = UILabel(frame: CGRect(x: 5, y: 0, width: 364, height: 80))
            label.text = "Deals Of the Day"
            view.backgroundColor = UIColor.black
            view.addSubview(label)
            return view
        } else {
            return nil
        }
    }
    
    
    
}
