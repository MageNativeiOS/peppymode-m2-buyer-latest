//
//  cedMageCustomPayment.swift
//  MageNative Magento Platinum
//
//  Created by CEDCOSS Technologies Private Limited on 31/03/17.
//  Copyright © 2017 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

extension  cedMagePayementView {
    
    
    func sendtransactionRequest(){


        let urlString="https://test.ccavenue.com/transaction/transaction.do"//"https://test.ccavenue.com/transaction/transaction.do"//"https://secure.ccavenue.com/transaction/transaction.do"

        var cstmrID="hemangi"
        if defaults.bool(forKey: "isLogin") {
            cstmrID = userInfoDict["customerId"]!;
        }

        var parameterString="command=getJsonDataVault&currency="+self.total["currency_code"]!+"&amount="+self.total["grandtotal"]!//.dropFirst()
        parameterString+="&access_code=AVUC88GK57AB79CUBA&customer_identifier="+cstmrID
        var request=URLRequest(url: URL(string: urlString)!)
        print(urlString)
        print(parameterString)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.setValue(urlString, forHTTPHeaderField: "Referer")
        request.setValue("Mozilla/5.0 (Linux; U; Android 4.0.3; ko-kr; LG-L160L Build/IML74K) AppleWebkit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30", forHTTPHeaderField: "User-Agent")

        request.httpBody=parameterString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        request.httpMethod="POST"
        let task=URLSession.shared.dataTask(with: request) { (data, response, error) in


            if let httpStatus = response as? HTTPURLResponse , httpStatus.statusCode/100 != 2
            {

                DispatchQueue.main.async
                    {

                        print("statusCode should be 200, but is \(httpStatus.statusCode)")
                        print("response = \(String(describing: response))")
                        print(NSString(data: data!, encoding: String.Encoding.utf8.rawValue) ?? "")

                }
                return;
            }

            // code to fetch values from response :: start


            guard error == nil && data != nil else
            {
                DispatchQueue.main.async
                    {
                        print("error=\(String(describing: error))")

                }
                return ;
            }

            DispatchQueue.main.async
                {
                                    do {
                                    let json = try JSON(data: data!)
                //                    var payOptionsArray=[CCPaymentOption]()
                //                    var cardTypeArray=[CCCardType]()
                                    print("jsonjsonjsonjsonjson")
                                    print(json)
                //                    if let jsonArray=json["payOptions"].array{
                //                        for item in jsonArray{
                //
                //                            let payOptions=CCPaymentOption()
                //                            payOptions.name=item["payOptDesc"].stringValue
                //                            payOptions.payOptId=item["payOpt"].stringValue
                //                            payOptionsArray.append(payOptions)
                //                            let cardList=item["cardsList"].stringValue
                //                            if cardList != ""{
                //                                let data=cardList.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
                //
                //
                //
                //                                if let cardArray = try JSON(data: data!).array{
                //                                    for itm in cardArray{
                //                                        let cartType=CCCardType()
                //                                        cartType.cardName=itm["cardName"].stringValue
                //                                        cartType.cardType=itm["cardType"].stringValue
                //                                        cartType.dataAcceptedAt=itm["dataAcceptedAt"].stringValue
                //                                        cartType.payOptType=itm["payOptType"].stringValue
                //                                        cartType.status=itm["statusMessage"].stringValue
                //
                //                                        cardTypeArray.append(cartType)
                //                                    }
                //                                }
                //
                //
                //
                //                            }
                //
                //                        }


                             /*           let vc=UIStoryboard(name: "ccavenueStoryboard", bundle: nil).instantiateViewController(withIdentifier: "CCInitViewController") as! CCInitViewController
                                        vc.amount=String(self.total["grandtotal"]!)//.dropFirst())
                                        vc.currency=self.total["currency_code"]!
                                        vc.orderData = self.orderStatusData;
                                        vc.cstmrID=cstmrID
                                        vc.email = self.orderEmail;
                //                        vc.cardType=(cardTypeArray.first?.cardType)!
                //                        vc.cardName=(cardTypeArray.first?.cardName)!
                                        vc.order = self.orderStatusData["orderId"]!
                                        vc.orderNumber = Int(self.orderStatusData["orderId"]!)!
                //                        vc.data_accept=(cardTypeArray.first?.dataAcceptedAt)!
                //                        vc.paymentOptionId=(payOptionsArray.first?.payOptId)!
                                        self.navigationController?.pushViewController(vc, animated: true)

*/
                                        let controller=UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "CCWebViewController") as! CCWebViewController
                                         //let controller:CCWebViewController = CCWebViewController()
                                        controller.accessCode = "AVUC88GK57AB79CUBA"
                                        controller.merchantId = "237112"
                                        controller.amount = String(self.total["grandtotal"]!)
                                        controller.currency = self.total["currency_code"]!
                                        controller.orderId = self.orderStatusData["orderId"]!
                                        controller.redirectUrl = "https://peppymode.com/CCavenue/ccavResponseHandler.php"
                                        controller.cancelUrl = "https://peppymode.com/mobiccavenue/index/responseHandler.php"//cancelUrlText
                                        controller.rsaKeyUrl = "https://peppymode.com/mobiccavenue/index/getRsa"
                                        
                                        controller.cancelClosure = {
                                            print("cancelle dby user transaction failed")
                                            self.afterPayment(payment_id: "", failure: "true")
                                        }
                                        
                                        
                                        
                                        let nav = UINavigationController(rootViewController: controller)
                                        //self.navigationController?.pushViewController(nav, animated: true)
                                        self.present(nav, animated: true)
                                    //}
                                        
                                        
                                        
                                        
                                    }catch let error {
                                        print(error.localizedDescription)
                                    }
                            }

        }
        task.resume()
    }
    
    
    func getClientToken(){
        var customerd = "0"
        if defaults.bool(forKey: "isLogin") {
            if  let customerId = userInfoDict["customerId"]{
               customerd = customerId
            }
        }
       
        self.sendRequest(url: clientTokenUrl, params: ["customer_id":customerd],store:false)
    }
    
    func fetchExistingPaymentMethod(clientToken: String) {
        let actionsheet = UIAlertController(title: "Select Payment To Proceed".localized, message: nil, preferredStyle: .actionSheet)
        print("%^%^%^*(**^&%&%*)_*^&^")
        for buttons in self.previousMethods {
            let action  = UIAlertAction(title: buttons["maskedNumber"], style: UIAlertAction.Style.default,handler: {
                action -> Void in
                
                print(action.title as Any)
                
            })
            cedMageImageLoader.shared.loadImgFromUrl(urlString: "", completionHandler: {
                image,string in
                 action.setValue(image, forKey: "image")
            })
            actionsheet.addAction(action)
            
            
        }
        actionsheet.addAction(UIAlertAction(title: "Add New".localized, style: UIAlertAction.Style.cancel, handler: {
            action -> Void in
           // self.showDropIn("")
        }))
        actionsheet.addAction(UIAlertAction(title: "Cancel".localized, style: UIAlertAction.Style.cancel, handler: {
            action -> Void in
        }))
        if(UIDevice().model.lowercased() == "iPad".lowercased()){
            actionsheet.popoverPresentationController?.sourceView = self.view
        }
        actionsheet.modalPresentationStyle = .fullScreen;
        self.present(actionsheet, animated: true, completion: nil)
    }
    
    func showpreviousPaymethods(){
        
    }
  
   /*
    func showDropIn(clientTokenOrTokenizationKey: String) {
        let request =  BTDropInRequest()
        let dropIn = BTDropInController(authorization: clientTokenOrTokenizationKey, request: request)
        { (controller, result, error) in
            if (error != nil) {
                print("ERROR")
                self.afterPayment(payment_id: "", failure: "true")
            } else if (result?.isCancelled == true) {
                print("CANCELLED")
                self.afterPayment(payment_id: "", failure: "true")
            } else if let result = result {
                // Use the BTDropInResult properties to update your UI
                let out = result.paymentMethod!
                print("nonce -----\(out.nonce)")
                
                self.postNonceToServer(paymentMethodNonce: out.nonce)
            }
            controller.dismiss(animated: true, completion: nil)
        }
        dropIn?.modalPresentationStyle = .fullScreen;
        self.present(dropIn!, animated: true, completion: nil)
    }*/
    
    
    func postNonceToServer(paymentMethodNonce: String,user:Bool=false,token:String="") {
        if let currencySymbol = total["currency_symbol"]{
            if let ammount = total["grandtotal"]?.replacingOccurrences(of: currencySymbol, with: ""){
                print(ammount)
                if let order_id = orderStatusData["orderId"] {
                      var params = ["pay_amt":ammount,"order_id":order_id]
                    if user {
                        params["token"] = token
                    }else{
                        params["payment_method_nonce"] = paymentMethodNonce
                    }
                  
                    if defaults.bool(forKey:"isLogin"){
                        if let custId =  userInfoDict["customerId"] {
                            params["customer_id"] = custId
                        }
                    }
                    self.sendRequest(url: trasactionUrl, params: params,store:false)
                }
            }
        }
        
        
    }
    
    func afterPayment(payment_id:String,failure:String,order_id:String = ""){
          if let order_id = orderStatusData["orderId"] {
        self.sendRequest(url:finalOrderCheck, params: ["order_id":order_id,"additional_info":payment_id,"failure":failure])
        }
    }
 
    

    
   
    
    
}
