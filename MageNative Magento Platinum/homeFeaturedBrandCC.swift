//
//  homeFeaturedBrandCC.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 01/10/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class homeFeaturedBrandCC: UICollectionViewCell {
    //MARK: - Properties
        
        static var reuseID: String = "homeFeaturedBrandCC"
        
        //MARK: - Views
        
        let brandImage: UIImageView = {
            let img = UIImageView()
            img.clipsToBounds = true
            img.contentMode = .scaleAspectFit
            return img
        }()
        
        lazy var nameLabel:UILabel = {
            let label = UILabel()
            label.textColor = UIColor.mageLabel
            label.textAlignment = .center
            label.numberOfLines = 3
            label.font = UIFont.systemFont(ofSize: 13, weight: .semibold)
            return label
        }()
        
        //MARK: - Life Cycle
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            addSubview(brandImage)
            brandImage.anchor(top: topAnchor, left: leadingAnchor, bottom: bottomAnchor, right: trailingAnchor, paddingTop: 3, paddingLeft: 3, paddingBottom: 3, paddingRight: 3)
            
            brandImage.centerX(inView: self)
            
            addSubview(nameLabel)
            nameLabel.anchor(top: brandImage.bottomAnchor, left: leadingAnchor, right: trailingAnchor, paddingTop: 5, paddingLeft: 3, paddingRight: 3)
            
        }
        
        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        //MARK: - Helper Functions
        
        func populate(with data: HomeCategory) {
            brandImage.sd_setImage(with: URL(string: data.image ?? ""), placeholderImage: UIImage(named: "placeholder"))
            nameLabel.text = data.name
           
         //   categoryImage.layer.cornerRadius = categoryImage.frame.height/2
        }
        
    }
