//
//  homeFeaturedBlogCC.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 01/10/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class homeFeaturedBlogCC: UICollectionViewCell, WKNavigationDelegate {
    
    //MARK: - Properties
    
    static var reuseID: String = "homeFeaturedBlogCC"
    
    //MARK: - Views
    
    let blogImage: UIImageView = {
        let img = UIImageView()
        img.clipsToBounds = true
        return img
    }()
    
    lazy var nameLabel:UILabel = {
        let label = UILabel()
        label.textColor = UIColor.mageLabel
        label.textAlignment = .center
        label.numberOfLines = 3
        label.font = UIFont.systemFont(ofSize: 13, weight: .semibold)
        return label
    }()
    
    lazy var webContent:WKWebView = {
        let webView = WKWebView()
          webView.navigationDelegate = self;
        return webView
    }()
    
    //MARK: - Life Cycle
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(blogImage)
        blogImage.anchor(top: topAnchor, left: leadingAnchor, right: trailingAnchor , paddingTop: 5, paddingLeft: 2,paddingRight:  2, height: 100)
        
        blogImage.centerX(inView: self)
        
        addSubview(nameLabel)
        nameLabel.anchor(top: blogImage.bottomAnchor, left: leadingAnchor, right: trailingAnchor, paddingTop: 5, paddingLeft: 3, paddingRight: 3)
        
        addSubview(webContent)
        webContent.anchor(top: nameLabel.bottomAnchor, left: leadingAnchor, bottom: bottomAnchor, right: trailingAnchor, paddingTop: 5, paddingLeft: 2, paddingBottom: 2, paddingRight: 2)
        
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
         for view in webView.subviews as [UIView] {
             if let activityIndicator = view as? UIActivityIndicatorView {
                 activityIndicator.removeFromSuperview();
             }
         }
     }
    
      //MARK: - Helper Functions
      
      func populate(with data: FeaturedBlog) {
          blogImage.sd_setImage(with: URL(string: data.image ?? ""), placeholderImage: UIImage(named: "placeholder"))
          nameLabel.text = data.name
        do{
         let string = try NSAttributedString(data: (data.short_content?.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue)))!, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
          let prodDes = """
          <html><head><meta name='viewport' content='width=device-width,
          initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0'><link rel="stylesheet" type="text/css" href="webfile.css">
          <span style="font-family: 'Roboto-Regular'; font-size: 15px;"><font color="black">\(string.string)</span></head></html>
          """
          webContent.loadHTMLString(prodDes, baseURL: URL(fileURLWithPath: Bundle.main.path(forResource: "webfile", ofType: "css") ?? "" ))
        }catch let  err{
            print(err.localizedDescription)
        }
      }
    
}
