//
//  ShipAndReturnPolicy.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 29/07/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import Foundation

class shipAndReturnPolicy:UIView{
    
    @IBOutlet weak var shipImg: UIImageView!
    @IBOutlet weak var deliveryImg: UIImageView!

    var view : UIView!
    override init(frame: CGRect){
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        xibSetup()
    }
    func xibSetup(){
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth,UIView.AutoresizingMask.flexibleHeight]
      
        addSubview(view)
    }
    func loadViewFromNib() -> UIView{
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "ShipAndReturnPolicy", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
}
