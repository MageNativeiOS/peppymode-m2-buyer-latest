/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */


import UIKit

class HomepageDealsCell: UITableViewCell,UICollectionViewDataSource,UICollectionViewDelegate {

    @IBOutlet weak var dealGroupImage: UIImageView!
    @IBOutlet weak var dealTimer: UILabel!
    @IBOutlet weak var dealtitle: UILabel!
    @IBOutlet weak var dealsCollection: UICollectionView!
    var parent = UIViewController()

    var deals = [[String:String]]()
    var dealtime = 100
    var data = true
    var timer = Timer()
    var startTime = TimeInterval()
    var timerStatus = String()
    override func awakeFromNib() {
        super.awakeFromNib()
        dealsCollection.delegate = self
        dealsCollection.dataSource = self
       
        dealGroupImage.fontColorTool()
        dealTimer.fontColorTool()
        dealtitle.fontColorTool()
        if(timerStatus == "2"){
            dealTimer.isHidden = true
            dealTimer.text = ""
        }else{
       Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(HomepageDealsCell.update), userInfo: nil, repeats: true)
        }
        // Initialization code
    }
    @objc func update() {
        if(startTime>0){
        var time = NSInteger(startTime)
         time = time / 1000
        let seconds = time % 60
        let minutes = (time / 60) % 60
        let hours = (time / 3600)
        dealTimer.text =  String(format: "%0.2d H:%0.2d M:%0.2d S",hours,minutes,seconds)
            startTime = startTime - 1000
        }
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return deals.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = dealsCollection.dequeueReusableCell(withReuseIdentifier: "dealcell", for: indexPath as IndexPath) as! dealcell
        cell.title.text = deals[indexPath.row]["offer_text"]
        let img_url = deals[indexPath.row]["deal_image_name"]
    
        cedMageImageLoader.shared.loadImgFromUrl(urlString: img_url!,completionHandler: { (image: UIImage?, url: String) in
            DispatchQueue.main.async {
                cell.ImageView.image = image
            }
        })
        cell.cardView()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
     
    }


    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
