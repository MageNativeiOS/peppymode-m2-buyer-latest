/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */


import UIKit

class cedMageHomeNavigation: homedefaultNavigation {
  
 
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
    setViewControllers([newPeppymodeHomeController()], animated: true)
  }
  
  override func viewWillAppear(_ animated: Bool) {
    //        let layout = cedMage.getInfoPlist(fileName: "cedMage", indexString: "layout") as! String
    //        if layout == "simple2" {
    //            let viewController = UIStoryboard(name: "homeLayouts", bundle: nil).instantiateViewController(withIdentifier: "simplelayout2") as!  cedMageThemeTwo
    //            self.setViewControllers([viewController], animated: true)
    //        }else{
    //
    //            let testthemeView = UIStoryboard(name: "homeLayouts", bundle: nil).instantiateViewController(withIdentifier: "testTheme") as!  cedMageTestHomeTheme
    //                 self.setViewControllers([testthemeView], animated: true)
    //        }
    
  }
  

  
   /* @objc  func backfunc(sender:UIButton){
    let mainview = self.sideDrawerViewController?.mainViewController  as? homedefaultNavigation
    let result =  mainview?.popViewController(animated: true)
    print(result as Any)
  }*/
  
 /*@objc func toggleDrawer() {
      if let sideDrawerViewController = self.sideDrawerViewController {
          sideDrawerViewController.toggleDrawer()
      }
  } */
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
}
