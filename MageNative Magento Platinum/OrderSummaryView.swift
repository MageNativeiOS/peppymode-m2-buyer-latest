//
//  OrderSummaryView.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 30/09/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import Foundation

class OrderSummaryView: UIView{
    var view: UIView!
    //
    @IBOutlet weak var orderSummaryLbl: UILabel!
    @IBOutlet weak var productListingStack: UIStackView!
    @IBOutlet weak var productListingStackHeight: NSLayoutConstraint!
    
    //
    @IBOutlet weak var totalItemLbl: UILabel!
    @IBOutlet weak var priceView: UIView!
    @IBOutlet weak var amountToPayLbl: UILabel!
    @IBOutlet weak var shippingChargesLbl: UILabel!
    @IBOutlet weak var discountLbl: UILabel!
    @IBOutlet weak var taxPriceLbl: UILabel!
    
   //
   // @IBOutlet weak var proceedView: UIView!
    @IBOutlet weak var proceedBtn: UIButton!
    @IBOutlet weak var totalPrice: UILabel!
   // @IBOutlet weak var vatLbl: UILabel!
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        xibSetup()
    }
    func xibSetup()
    {
        view = loadViewFromNib()
        
        // use bounds not frame or it'll be offset
        view.frame = bounds
        
        // Make the view stretch with containing view
        
//        priceView.layer.cornerRadius = 10.0
//        priceView.layer.borderWidth = 1.0
//        if #available(iOS 13.0, *) {
//            priceView.layer.borderColor = UIColor.label.cgColor
//        } else {
//            // Fallback on earlier versions
//        }
        view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        proceedBtn.setTitleColor(.white, for: .normal)
        proceedBtn.setTitle("Place Order".uppercased(), for: .normal)
        proceedBtn.setThemeColor()
        proceedBtn.layer.cornerRadius = 8.0
        proceedBtn.clipsToBounds = true
        addSubview(view)
    }
    
    
    
     func loadViewFromNib() -> UIView
       {
           let bundle = Bundle(for: type(of: self))
           let nib = UINib(nibName: "OrderSummaryView", bundle: bundle)
           
           // Assumes UIView is top level and only object in CustomView.xib file
           let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
           return view
       }
}
