//
//  cedMageRazorpayPayment.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 11/06/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import Foundation

extension cedMagePayementView:RazorpayPaymentCompletionProtocol{
       func loadRazorPay(){
           // Do any additional setup after loading the view, typically from a nib.//
           razorpay = RazorpayCheckout.initWithKey("rzp_live_OVfgRcSmzxtBYk", andDelegate: self)
          showPaymentForm()////rzp_test_JmQfYqlnYHQW2O
        
        
    }
    
    
    internal func showPaymentForm(){
        
        guard let currency = total["currency_symbol"] else{return}
        guard let total = self.total["grandtotal"] else {
            return;
        }
        guard let orderId = orderStatusData["orderId"] else{return}
        guard let totalDouble = Double(total) else{return;}
        let options: [String:Any] = [
         
           
                "amount": "\(totalDouble*100.00)"
                    , //This is in currency subunits. 100 = 100 paise= INR 1.
                    "currency":  "INR",//We support more that 92 international currencies.
                   // "description": "purchase description",
                   // "order_id": orderId,
                    "image": "https://peppymode.com/pub/media/logo/default/Original_updated_transparent-01.png",
//                    "prefill": [
//                        "contact": "9797979797",
//                        "email": "foo@bar.com"
//                    ],
                    "theme": [
                        "color": "#000000"
                    ]
                ]
        let currentViewController:  UIViewController? = UIApplication.shared.keyWindow?.rootViewController
        razorpay.open(options, displayController: currentViewController ?? self)
        
    }
    override func viewDidAppear(_ animated: Bool) {
          //showPaymentForm()
      }
    func onPaymentError(_ code: Int32, description str: String) {
           print("error\(str)")
        self.view.makeToast("Payment Failed", duration: 2.0, position: .center)
        cedMage.delay(delay: 2.0) {
            self.afterPayment(payment_id: "", failure: "true")
        }
       }
       
       func onPaymentSuccess(_ payment_id: String) {
           print("success")
         self.view.makeToast("Payment Successful", duration: 2.0, position: .center)
               cedMage.delay(delay: 2.0) {
                   self.afterPayment(payment_id: payment_id, failure: "false")
               }
       }
  
}
extension UIApplication{
     func getTopViewController() -> UIViewController? {
         var topController: UIViewController? = UIApplication.shared.keyWindow?.rootViewController
         while topController?.presentedViewController != nil {
             topController = topController?.presentedViewController
         }
         return topController
     }
}
