//
//  bannerTableViewCell.swift
//  MageNative Magento Platinum
//
//  Created by Macmini on 30/10/18.
//  Copyright © 2018 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class bannerTableViewCell: UITableViewCell,FSPagerViewDataSource,FSPagerViewDelegate {
    var dataSourceImage = [String]()
    var dataSourceTextDeal = [String]()
    var bannerArray = [[String:String]]()
    var parenTView = UIViewController()
    
    
    @IBOutlet weak var pagerView: FSPagerView! {
        didSet {
            self.pagerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
            self.pagerView.itemSize = CGSize.zero
        }
    }
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        pagerView.dataSource = self
        pagerView.delegate = self
        pagerView.automaticSlidingInterval = 3.0
        pagerView.isInfinite = true
        pagerView.itemSize = CGSize.zero
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
    
    
    // MARK:- FSPagerView DataSource
    
    public func numberOfItems(in pagerView: FSPagerView) -> Int {
        return dataSourceImage.count
    }
    
    public func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
        cell.imageView?.contentMode = .scaleToFill
        cell.imageView?.clipsToBounds = true
        if let url = URL(string: dataSourceImage[index]){
            cell.imageView?.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "placeholder"))
            cell.imageView?.contentMode = .scaleToFill
        }
//        if index <= dataSourceTextDeal.count{
//             cell.textLabel?.text = dataSourceTextDeal[index]
//        }
       
        return cell
    }
    
    // MARK:- FSPagerView Delegate
    
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        let banner = bannerArray[Int(index)]
        if(banner["link_to"] == "category"){
            let viewcontoller = UIStoryboard(name: "categorylayouts", bundle: nil).instantiateViewController(withIdentifier: "cedMageDefaultCollection") as? cedMageDefaultCollection
            viewcontoller?.selectedCategory = banner["product_id"]!
            parenTView.navigationController?.pushViewController(viewcontoller!, animated: true)
        }else if (banner["link_to"] == "product"){
            let productview = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "productSinglePageViewController") as! ProductSinglePageViewController
            productview.product_id = banner["product_id"]!
            parenTView.navigationController?.pushViewController(productview
                , animated: true)
            
        }else if (banner["link_to"] == "website"){
            let view = UIStoryboard(name: "cedMageAccounts", bundle: nil)
            let viewControl = view.instantiateViewController(withIdentifier: "cmsWebView") as! cedMageCmsWebView
            let url = banner["product_id"]
            viewControl.pageUrl = url!
            parenTView.navigationController?.pushViewController(viewControl, animated: true)
        }
    }
    
    
    

  

}
