//
//  HomeDataModel.swift
//  MageNative Magento Platinum
//
//  Created by Hamza Usmani on 02/09/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import Foundation

struct HomeDataModel: Decodable {
    let title: String?
    let link_type: String?
    let brand_slider:[HomeBanner]?
    let widget: [HomeBanner]?
    let categories: [HomeCategory]?
    let products: [HomeProduct]?
    let brands: [HomeCategory]?
    let blogs: [FeaturedBlog]?
    //--edited let deal_slider: [HomeDealProduct]?
    let deal_slider: [JSON]?
    let banner: HomeBanner?
    let deal_group: HomeDeal?
    let category_id: String?
    let type_of_banner: String?
    let type_of_products: String?
    let category_type: String?
    let position: String?
    let category_shape: String?
    let banner_orientiation: String?
    let background_color: String?
    let category_name: String?
}

struct HomeBanner: Decodable {
    let product_id: String?
    let banner_image: String?
    let title: String?
    let id: String?
    let link_to: String?
}
struct FeaturedBlog: Decodable{
    let short_content : String?
    let image : String?
    let id : String?
    let url_key : String?
    let name : String?
}
struct HomeCategory: Decodable {
    let id:String?
    let image:String?
    let name:String?
}

struct HomeProduct: Decodable {
    let product_image:String?
    let product_id:String?
    let stock_status:Bool?
    let product_name:String?
    let type:String?
    //let review:Int?
    //let offer:Int?
    let wishlist_item_id:Int?
    let Inwishlist:String?
    let special_price:String?
    let regular_price:String?
    let description:String?
}

struct HomeDeal: Decodable {
    let content:[HomeDealContent]?
    let deal_duration:Int?
    let title:String?
}

struct HomeDealContent: Decodable {
    let product_id:String?
    let link_to:String?
    let deal_image_name:String?
}

struct HomeDealProduct: Decodable {
    let productName:String?
    let duration:Int?
    let image:String?
    let type:String?
    //let price:[priceModel]?
    let productId:String?
    let qty:Int?
    let stock:String?
    let offer: Int?
    
    enum CodingKeys: String, CodingKey {
        case productName = "product-name"
        case duration = "duration"
        case image = "image"
        case type = "type"
        //case price = "price"
        case productId = "product-id"
        case qty = "qty"
        case stock = "stock"
        case offer = "offer"
    }
    
    
}

struct priceModel: Decodable {
    let special_price : [Int]?
    let regular_price : [Int]?
}

//MARK:- Data type enum
enum DataType : Codable
{
    func encode(to encoder: Encoder) throws {
        
    }
    case int(Int)
    case string(String)
    case bool(Bool)
   // case ar(JSON)
    case dict(HomeDealProduct)
    
    init(from decoder: Decoder) throws{
        if let intValue = try? decoder.singleValueContainer().decode(Int.self)
        {
            self = .int(intValue)
            return
        }
        if let stringValue = try? decoder.singleValueContainer().decode(String.self)
        {
            self = .string(stringValue)
            return
        }
        if let boolValue = try? decoder.singleValueContainer().decode(Bool.self)
        {
            self = .bool(boolValue)
            return
        }
        if let dictValue = try? decoder.singleValueContainer().decode(HomeDealProduct.self)
        {
            self = .dict(dictValue)
            return
        }
       /* if let arValue = try? decoder.singleValueContainer().decode(JSON.self)
        {
           // if arValue.count != 0{
                self = .ar(arValue)
                return
            //}
           
        }*/
        throw DataError.missingValue
    }
}

enum DataError : Error
{
    case missingValue
}

extension DataType {
    var DataValue:JSON {
        switch self {
        case .int(let intvalue):
            print("intvalue = \(intvalue)")
            return JSON(intvalue)
        case .string(let stringValue):
            print("stringValue = \(stringValue)")
            return JSON(stringValue)
        case .bool(let boolValue):
            print("boolValue = \(boolValue)")
            return JSON(boolValue)
      /*  case .ar(let arValue):
            print("arValue = \(arValue)")
            return JSON(arValue)*/
        case .dict(let dictValue):
            print("dictValue = \(dictValue)")
            return JSON(dictValue)
        }
    }
}
