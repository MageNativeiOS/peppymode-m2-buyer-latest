//
//  HomeCircularCategorySliderCC.swift
//  MageNative Magento Platinum
//
//  Created by Hamza Usmani on 22/09/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class HomeCircularCategorySliderCC: UICollectionViewCell {
    
    //MARK: - Properties
    
    static var reuseID: String = "HomeCircularCategorySliderCC"
    
    //MARK: - Views
    let imageOuterView: UIView = {
        let view = UIView()
        view.layer.borderWidth = 5
        view.layer.borderColor = UIColor.init(hexString: "#ffffff")?.cgColor
        view.layer.cornerRadius = view.frame.height/2
        view.clipsToBounds = true
        view.cardView2()
        
        return view
    }()
    let categoryImage: UIImageView = {
        let img = UIImageView()
        img.clipsToBounds = true
        return img
    }()
    
    lazy var nameLabel:UILabel = {
        let label = UILabel()
        label.textColor = UIColor.mageLabel
        label.textAlignment = .center
        label.numberOfLines = 3
        label.font = UIFont.systemFont(ofSize: 12, weight: .semibold)
        return label
    }()
    
    //MARK: - Life Cycle
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(imageOuterView)
        imageOuterView.addSubview(categoryImage)//addSubview(categoryImage)
        
       imageOuterView.anchor(top: topAnchor, paddingTop: 5, width: 95, height: 95)
        categoryImage.anchor(paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0)
        imageOuterView.centerX(inView: self)
        
        addSubview(nameLabel)
        nameLabel.anchor(top: imageOuterView.bottomAnchor, left: leadingAnchor, right: trailingAnchor, paddingTop: 5, paddingLeft: 1, paddingRight: 1)
        
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        imageOuterView.layer.cornerRadius = imageOuterView.frame.height/2
        imageOuterView.clipsToBounds = true
    }
    
    //MARK: - Helper Functions
    
    func populate(with data: HomeCategory) {
        categoryImage.sd_setImage(with: URL(string: data.image ?? ""), placeholderImage: UIImage(named: "placeholder"))
        nameLabel.text = data.name
        
        imageOuterView.layer.cornerRadius = imageOuterView.frame.height/2
        imageOuterView.cardView2()
        imageOuterView.clipsToBounds = true
    }
    
}
