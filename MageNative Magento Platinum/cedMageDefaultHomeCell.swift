/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */


import UIKit

class cedMageDefaultHomeCell: UITableViewCell,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var cedMageHomeCollectCell: UICollectionView!
    var datasource = [[String:String]]()
    var data = true
    var sbounds = CGRect()
    var parent = UIViewController()
    var viewControl = UIViewController()
    override func awakeFromNib() {
        super.awakeFromNib()
      headingLabel.fontColorTool()
        cedMageHomeCollectCell.delegate = self
        cedMageHomeCollectCell.dataSource = self
        //containerView.cardView()
        headingLabel.cardView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
    //MARK:- Implementation of collectionView delegate function
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return datasource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionCell", for: indexPath) as! cedMageCollectionCell
         cell.label?.fontColorTool()
        cell.label?.text = datasource[indexPath.row]["product_name"]
        cedMageImageLoader.shared.loadImgFromUrl(urlString: datasource[indexPath.row]["product_image"]!,completionHandler: { (image: UIImage?, url: String) in
            DispatchQueue.main.async {
             cell.imageViewProduct.image = image
            }
            
        })
      //  cell.cardView()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if(UIDevice().model.lowercased() == "ipad".lowercased()){
            return CGSize(width: sbounds.width/3-10, height: sbounds.height/3)
        }
            return CGSize(width:sbounds.width/2.15 , height: sbounds.height/2.5)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            
            let productview = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "rootPageViewController") as! cedMageProductViewRoot
            productview.pageData = datasource as NSArray
            let instance = cedMage.singletonInstance
            instance.storeParameterInteger(parameter: indexPath.row)
            parent.navigationController?.pushViewController(productview
                , animated: true)
        
    }

    
}
