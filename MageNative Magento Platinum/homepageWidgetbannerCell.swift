//
//  homepageWidgetbannerCell.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 16/11/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class homepageWidgetbannerCell: UITableViewCell {

    @IBOutlet weak var widgetView: FSPagerView!
    var images:[String] = []
    var bannerData:[homepageBanner] = []{
        didSet{
            self.widgetView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
            self.widgetView.itemSize = CGSize.zero
            widgetView.delegate = self
            widgetView.dataSource = self
            widgetView.automaticSlidingInterval = 3.0
            widgetView.isInfinite = true
            widgetView.layer.cornerRadius = 15.0
        }
    }
    var parent: UIViewController!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

 

}

extension homepageWidgetbannerCell: FSPagerViewDelegate,FSPagerViewDataSource {
    
   public func numberOfItems(in pagerView: FSPagerView) -> Int {
        return images.count
    }
    
    public func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
        cell.imageView?.contentMode = .scaleAspectFit
        cell.imageView?.clipsToBounds = true
        if let url = URL(string: images[index]){
            cell.imageView?.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "login-logo"))
            cell.imageView?.contentMode = .scaleToFill
        }
        return cell
    }
    
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        
        switch bannerData[index].link_to {
        case "category":
            let vc = UIStoryboard(name: "categorylayouts", bundle: nil).instantiateViewController(withIdentifier: "cedMageDefaultCollection") as! cedMageDefaultCollection
            vc.selectedCategory = bannerData[index].product_id!
            parent.navigationController?.pushViewController(vc, animated: true)
        case "product":
            let vc = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "productSinglePageViewController") as! ProductSinglePageViewController
            vc.product_id = bannerData[index].product_id!
            parent.navigationController?.pushViewController(vc, animated: true)
        default:
            let view = UIStoryboard(name: "cedMageAccounts", bundle: nil)
            let vc = view.instantiateViewController(withIdentifier: "cmsWebView") as! cedMageCmsWebView
            let url = bannerData[index].product_id!
            vc.pageUrl = url
            parent.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
}
