/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */

import UIKit

class cedMageMyOrdersCell: UITableViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var calenderButton: UIButton!
    @IBOutlet weak var statusButton: UIButton!
    @IBOutlet weak var shiptoHeading: UILabel!
    
    @IBOutlet weak var orderIdHeading: UILabel!
    
    @IBOutlet weak var costheading: UILabel!
    
    
    @IBOutlet weak var shipTo: UILabel!
    @IBOutlet weak var orderId: UILabel!
    
    @IBOutlet weak var cost: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
   // calenderButton.setImage(UIImage(named:"calendar"), for: UIControlState.normal)
        calenderButton.fontColorTool()
        statusButton.fontColorTool()
        
        shipTo.fontColorTool()
        orderId.fontColorTool()
        
        cost.fontColorTool()
        calenderButton.imageEdgeInsets = UIEdgeInsets.init(top: 10, left: 10, bottom: 10, right: 10)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
