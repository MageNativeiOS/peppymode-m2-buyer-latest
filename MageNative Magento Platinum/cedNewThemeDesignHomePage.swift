//
//  cedNewThemeDesign.swift
//  MageNative Magento Platinum
//
//  Created by Macmini on 30/10/18.
//  Copyright © 2018 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit
class cedHomePageMenuSlider: MagenativeUIViewController,UIScrollViewDelegate{
    //vaiable decleration
    var bannerArray = [[String:String]]()
    var banner_image_array = [String]()
    var products  = [[String:String]]()
    var loadMoreData = true
    var currentpage = 1
    var flag=false
    var no_pro_check=false
    var dealTouple = (group_start_date:String(),created_time:String(),group_end_date:String(),deal_duration:String(),group_image_name:String(),group_status:String(),title:String(),group_id:String(),store_id:String(),timer_status:String(),view_all_status:String(),is_static:String(),deal_link:String(),update_time:String())
    var subDealToupleArray = [(id:"", deal_image_name: "", deal_link: "", start_date: "", relative_link: "" ,deal_type: "", end_date: "", deal_title: "", product_link:"", store_id: "", category_link: "", offer_text: "", status: "")]
    var dealTouple2nd = (group_start_date:String(),created_time:String(),group_end_date:String(),deal_duration:String(),group_image_name:String(),group_status:String(),title:String(),group_id:String(),store_id:String(),timer_status:String(),view_all_status:String(),is_static:String(),deal_link:String(),update_time:String())
    var subDealToupleArray2nd = [(id:"", deal_image_name: "", deal_link: "", start_date: "", relative_link: "" ,deal_type: "", end_date: "", deal_title: "", product_link:"", store_id: "", category_link: "", offer_text: "", status: "")]
    
    @IBOutlet weak var tabelView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getRequest(url: "mobiconnect/module/gethomepage/1/",store:true)
        self.getRequest(url: "mobiconnectdeals/getdealgroup/",store:true)
        subDealToupleArray = []
        tabelView.delegate = self
        tabelView.dataSource = self
        self.send_req_pagination(url: "mobiconnect/home/featured/page/\(currentpage)/store/", store: true)
        setArraytonil()
    }

    func setArraytonil(){
        subDealToupleArray = []
        subDealToupleArray2nd = []
    }
    
    override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
        cedMageLoaders.removeLoadingIndicator(me: self)
        guard let json = try? JSON(data: data!) else{return;}
        if(requestUrl == "mobiconnect/module/gethomepage/1/"){
            print(json)
            self.parse_product(json: json, index: "category")
        }else if(requestUrl == "mobiconnectdeals/getdealgroup/"){
            print(json)
            self.parse_product(json: json, index: "deal_products")
        }
        else {
            self.parse_product(json: json, index: "feachered")
        }
    }
    
    func parse_product(json:JSON,index:String){
        print(json)
        if(index == "category"){
            if(json[0]["data"]["banner"].arrayValue.count > 0){
                for result in json[0]["data"]["banner"].arrayValue {
                    let product_id = result["product_id"].stringValue
                    print("---\(product_id)")
                    let id = result["id"].stringValue
                    let title = result["title"].stringValue
                    let link_to = result["link_to"].stringValue
                    let banner_image = result["banner_image"].stringValue
                    banner_image_array.append(banner_image)
                    let products_obj = ["product_id":product_id, "title":title,"link_to":link_to,"banner_image":banner_image,"id":id]
                    bannerArray.append(products_obj)
                }
                
                tabelView.reloadData()
                return
            }
        }
        if(index == "deal_products"){
            
            for dealIndex in json[0]["data"]["deal_products"].arrayValue.indices{
                parseDealData(fullDealData: json[0]["data"]["deal_products"][dealIndex],index:dealIndex)
            }
           
            tabelView.reloadData()
        } else if (index == "feachered"){
            print(json)
            if(json[0].stringValue.lowercased() == "NO_PRODUCTS".lowercased()){
                no_pro_check=true
                loadMoreData=false
                tabelView.reloadData()
                return
            }
            for result in json[0]["featured_products"].arrayValue {
                let product_id = result["product_id"].stringValue;
                let regular_price = result["regular_price"].stringValue;
                let special_price = result["special_price"].stringValue;
                let product_name = result["product_name"].stringValue;
                let product_image = result["product_image"].stringValue;
                let type = result["type"].stringValue;
                let review = result["review"].stringValue;
                let show_both_price = result["show-both-price"].stringValue;
                let Inwishlist = result["Inwishlist"].stringValue
                let starting_from = result["starting_from"].stringValue;
                let from_price = result["from_price"].stringValue;
                let offerText  = result["offer"].stringValue
                
                var wishlistItemId = "-1";
                if(result["wishlist-item-id"] != nil){
                    wishlistItemId = result["wishlist-item-id"].stringValue;
                }
                self.products.append(["product_id":product_id,"regular_price":regular_price, "special_price":special_price,"product_name":product_name,"product_image":product_image,"type":type,"review":review,"show-both-price":show_both_price,"starting_from":starting_from,"from_price":from_price,"Inwishlist":Inwishlist,"wishlist-item-id":wishlistItemId,"offerText":offerText]);
                
            }
            if products.count > 0 {
                tabelView.reloadSections([3], with: .fade)
            }
        }
    }
    
    
    func parseDealData(fullDealData:JSON,index:Int) {
        if index == 0{
            subDealToupleArray = []
            for subDeals in fullDealData["content"].arrayValue{
                let value = (id:subDeals["id"].stringValue, deal_image_name: subDeals["deal_image_name"].stringValue, deal_link: subDeals["deal_link"].stringValue, start_date: subDeals["start_date"].stringValue, relative_link: subDeals["relative_link"].stringValue, deal_type: subDeals["deal_type"].stringValue, end_date: subDeals["end_date"].stringValue, deal_title: subDeals["deal_title"].stringValue, product_link: subDeals["product_link"].stringValue, store_id: subDeals["store_id"].stringValue, category_link: subDeals["category_link"].stringValue, offer_text: subDeals["offer_text"].stringValue, status: subDeals["status"].stringValue)
                subDealToupleArray.append(value)
                
            }
            
            dealTouple = (group_start_date:fullDealData["group_start_date"].stringValue,created_time:fullDealData["created_time"].stringValue,group_end_date:fullDealData["group_end_date"].stringValue,deal_duration:fullDealData["deal_duration"].stringValue,group_image_name:fullDealData["group_image_name"].stringValue,group_status:fullDealData["group_status"].stringValue,title:fullDealData["title"].stringValue,group_id:fullDealData["group_id"].stringValue,store_id:fullDealData["store_id"].stringValue,timer_status:fullDealData["timer_status"].stringValue,view_all_status:fullDealData["view_all_status"].stringValue,is_static:fullDealData["is_static"].stringValue,deal_link:fullDealData["deal_link"].stringValue,update_time:fullDealData["update_time"].stringValue)
            
            
        }else if index == 1{
            subDealToupleArray2nd = []
            print(fullDealData)
            for subDeals in fullDealData["content"].arrayValue{
                let value = (id:subDeals["id"].stringValue, deal_image_name: subDeals["deal_image_name"].stringValue, deal_link: subDeals["deal_link"].stringValue, start_date: subDeals["start_date"].stringValue, relative_link: subDeals["relative_link"].stringValue, deal_type: subDeals["deal_type"].stringValue, end_date: subDeals["end_date"].stringValue, deal_title: subDeals["deal_title"].stringValue, product_link: subDeals["product_link"].stringValue, store_id: subDeals["store_id"].stringValue, category_link: subDeals["category_link"].stringValue, offer_text: subDeals["offer_text"].stringValue, status: subDeals["status"].stringValue)
                subDealToupleArray2nd.append(value)
                
            }
            
            dealTouple2nd = (group_start_date:fullDealData["group_start_date"].stringValue,created_time:fullDealData["created_time"].stringValue,group_end_date:fullDealData["group_end_date"].stringValue,deal_duration:fullDealData["deal_duration"].stringValue,group_image_name:fullDealData["group_image_name"].stringValue,group_status:fullDealData["group_status"].stringValue,title:fullDealData["title"].stringValue,group_id:fullDealData["group_id"].stringValue,store_id:fullDealData["store_id"].stringValue,timer_status:fullDealData["timer_status"].stringValue,view_all_status:fullDealData["view_all_status"].stringValue,is_static:fullDealData["is_static"].stringValue,deal_link:fullDealData["deal_link"].stringValue,update_time:fullDealData["update_time"].stringValue)
        }
    }
}
    




extension cedHomePageMenuSlider : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
        case 0:
            //            banner images
            if banner_image_array.count > 0{
                return 1
            }
            return 0
        case 1:
            //Deals images
            if subDealToupleArray.count > 0{
                return 1
            }
            return 0
        case 2:
            if subDealToupleArray2nd.count > 0{
                return 1
            }
            return 0
            
        case 3:
            return 1
            
        default:
            return 0
        }
    }
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
       return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            // For first banner section
            let cell = tableView.dequeueReusableCell(withIdentifier: "bannerTableViewCell", for: indexPath) as? bannerTableViewCell
            cell?.dataSourceImage = banner_image_array
            cell?.bannerArray = bannerArray
            cell?.parenTView = self
            return cell!
        case 1:
            // For first Deals section
            let cell = tableView.dequeueReusableCell(withIdentifier: "DealTableViewCell", for: indexPath) as? DealTableViewCell
            
            cell?.topLabel.fontColorTool()
            cell?.topLabel.text = dealTouple.title
            let color = cedMage.getInfoPlist(fileName: "cedMage", indexString: "themeColor") as! String
            cell?.topLabel.backgroundColor = UIColor(hexString : color)
            cell?.topLabel.textColor = .white
            cell?.subDealToupleArray = subDealToupleArray
            cell?.collectionview.reloadData()
            cell?.parentView = self
            return cell!
            
            
        case 2:
            // For Secound Deals section
            let cell = tableView.dequeueReusableCell(withIdentifier: "DealBannerCell", for: indexPath) as? DealBannerCell
            cell?.topLabel.text = dealTouple2nd.title
            cell?.topLabel.fontColorTool()
            cell?.topLabel.textColor = .white
            cell?.subDealToupleArray2nd = subDealToupleArray2nd
            cell?.parentView = self
            return cell!
        case 3:
            // For Secound Deals section
            let cell = tableView.dequeueReusableCell(withIdentifier: "featuredProduct", for: indexPath) as? featuredProduct
            let color = cedMage.getInfoPlist(fileName: "cedMage", indexString: "themeColor") as! String
            cell?.topLabel.backgroundColor = UIColor(hexString : color)
            cell?.topLabel.textColor = UIColor.white
            cell?.parentViewCont = self
            cell?.products = products
            cell?.loadMoreData = true
            cell?.currentpage = 1
            cell?.flag = flag
            cell?.no_pro_check = no_pro_check
            cell?.collectionView.reloadData()
            if (indexPath.row) == (products.count/2)
            {
                flag=true
            }
            return cell!
        default:
            return UITableViewCell()
        }
    }
    
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.section {
        case 0:
            return 200
        case 1:
            if UIDevice.current.userInterfaceIdiom == .pad{
                
                if(subDealToupleArray.count == 1) || (subDealToupleArray.count == 2)
                {
                    return 260
                }
                return CGFloat(ceil(Double(subDealToupleArray.count)/3.0) * 225.0)
            }else{
                if(subDealToupleArray.count == 1) || (subDealToupleArray.count == 2)
                {
                    return 260
                }
                return CGFloat(ceil(Double(subDealToupleArray.count)/2.0) * 225.0)
            }
        case 2:
            return 240
        case 3:
            if UIDevice.current.userInterfaceIdiom == .pad{
                if(products.count == 1)
                {
                    return 330
                }
                return CGFloat(ceil(Double(products.count)/3.0) * 285.0)
            }else{
                if(products.count == 1)
                {
                    return 330
                }
                return CGFloat(ceil(Double(products.count)/2.0) * 285.0)
            }
        default:
            return 0
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if no_pro_check==false
        {
            
            let currentOffset = scrollView.contentOffset.y
            let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
            if(!loadMoreData){
                return;
            }
            if (maximumOffset - currentOffset) <= 40 {
                currentpage += 1
                self.send_req_pagination(url: "mobiconnect/home/featured/page/\(currentpage)/store/", store: true)
            }
        }
    }
}
