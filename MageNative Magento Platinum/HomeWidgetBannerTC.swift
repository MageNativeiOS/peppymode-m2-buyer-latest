//
//  HomeWidgetBannerTC.swift
//  MageNative Magento Platinum
//
//  Created by Hamza Usmani on 21/09/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class HomeWidgetBannerTC: UITableViewCell {
    
    //MARK: - Properties
    
    static var reuseID:String = "HomeWidgetBannerTC"
    var bannerType: String = ""
    var banners = [HomeBanner]() { didSet { widgetSlider.reloadData() } }
    weak var parent: UIViewController?
    var fromBrandSlider = Bool()
    lazy var brandHeading:UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.text = "Brand Slider"
         label.font = UIFont.init(fontName: "TrajanPro-Regular", fontSize: 16)
        //label.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
        return label
    }()
    
    
    //MARK: - Views
    
    lazy var widgetSlider:UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        layout.scrollDirection = bannerType == "vertical" ? .vertical : .horizontal
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = UIColor.clear
        collectionView.register(HomeWidgetBannerCC.self, forCellWithReuseIdentifier: HomeWidgetBannerCC.reuseID)
        collectionView.delegate = self
        collectionView.dataSource = self
        return collectionView
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
         super.init(style: style, reuseIdentifier: reuseIdentifier)
         selectionStyle = .none
         backgroundColor = .clear
        if fromBrandSlider{
         addSubview(brandHeading)
        brandHeading.anchor(top: topAnchor, left: leadingAnchor, right: trailingAnchor, paddingTop: 2, paddingLeft: 8, paddingRight: 8, height: 30)
        addSubview(widgetSlider)
        widgetSlider.anchor(top: brandHeading.bottomAnchor, left: leadingAnchor, bottom: bottomAnchor, right: trailingAnchor, paddingTop: 2, paddingLeft: 2, paddingBottom: 5, paddingRight: 2)
        }else{
            addSubview(widgetSlider)
            widgetSlider.anchor(top: topAnchor, left: leadingAnchor, bottom: bottomAnchor, right: trailingAnchor, paddingTop: 2, paddingLeft: 2, paddingBottom: 5, paddingRight: 2)
        }
        //scrollAutomatically(Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.scrollAutomatically), userInfo: nil, repeats: true))
        Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.scrollAutomatically), userInfo: nil, repeats: true)
     }
     
     required init?(coder: NSCoder) {
         fatalError("init(coder:) has not been implemented")
     }
    
   

    var x = 1
    @objc func scrollAutomatically() {
        if self.x < self.banners.count {
          let indexPath = IndexPath(item: x, section: 0)
          self.widgetSlider.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
          self.x = self.x + 1
        }else{
          self.x = 0
          self.widgetSlider.scrollToItem(at: IndexPath(item: 0, section: 0), at: .centeredHorizontally, animated: true)
        }
    }
//    @objc func scrollAutomatically(_ timer1: Timer) {
//
//         let coll  = widgetSlider
//            for cell in coll.visibleCells {
//                let indexPath: IndexPath? = coll.indexPath(for: cell)
//                if ((indexPath?.row)!  < self.banners.count - 1){
//                    let indexPath1: IndexPath?
//                    indexPath1 = IndexPath.init(row: (indexPath?.row)! + 2, section: (indexPath?.section)!)
//
//                    coll.scrollToItem(at: indexPath1!, at: .bottom, animated: true)
//
//                }
//                else{
//                    let indexPath1: IndexPath?
//                    indexPath1 = IndexPath.init(row: 0, section: (indexPath?.section)!)
//                    coll.scrollToItem(at: indexPath1!, at: .top, animated: true)
//                }
//        }
//
//    }
}

//MARK: - UICollectionViewDataSource

extension HomeWidgetBannerTC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return banners.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HomeWidgetBannerCC.reuseID, for: indexPath) as! HomeWidgetBannerCC
        cell.populate(with: banners[indexPath.item])
        return cell
    }
}

//MARK: - UICollectionViewDelegate / UICollectionViewDelegateFlowLayout

extension HomeWidgetBannerTC: UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch bannerType {
        case "vertical":
            return .init(width: collectionView.frame.width/2, height: collectionView.frame.height)
        default:
            return banners.count <= 1 ? .init(width: collectionView.frame.width, height: collectionView.frame.height) : .init(width: collectionView.frame.width/1.2, height: collectionView.frame.height)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(banners[indexPath.item].link_to)
        switch banners[indexPath.item].link_to {
        case "category":
            
            let vc = UIStoryboard(name: "categorylayouts", bundle: nil).instantiateViewController(withIdentifier: "cedMageDefaultCollection") as!  cedMageDefaultCollection
            vc.selectedCategory = banners[indexPath.item].product_id ?? ""
            parent?.navigationController?.pushViewController(vc, animated: true)
            
            
        case "product":
            let vc = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "productSinglePageViewController") as! ProductSinglePageViewController
            vc.product_id = banners[indexPath.item].product_id ?? ""
            
            parent?.navigationController?.pushViewController(vc, animated: true)
        default:
            let view = UIStoryboard(name: "cedMageAccounts", bundle: nil)
            let vc = view.instantiateViewController(withIdentifier: "cmsWebView") as! cedMageCmsWebView
            let url = banners[indexPath.item].product_id ?? ""
            vc.pageUrl = url
            parent?.navigationController?.pushViewController(vc, animated: true)
            
        }
    }
    
    
    
    
}
