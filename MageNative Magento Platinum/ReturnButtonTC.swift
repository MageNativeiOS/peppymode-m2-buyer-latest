//
//  ReturnButtonTC.swift
//  MageNative Magento Platinum
//
//  Created by Hamza Usmani on 21/08/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class ReturnButtonTC: UITableViewCell {
    
    static var reuseID:String = "ReturnButtonTC"
    static var trackOrderId:String = "TrackOrderTC"
    static var cancelId : String = "CancelOrderTC"
    lazy var returnButton:UIButton = {
        let button = UIButton(type: .system)
       // button.setTitle("Return Product(s)", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
        button.backgroundColor = Settings.secondaryColor;        button.translatesAutoresizingMaskIntoConstraints = false
        button.layer.cornerRadius = 5.0
        return button
    }()
    
    
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.isMultipleTouchEnabled = true
        addSubview(returnButton)
//        returnButton.topAnchor.constraint(equalTo: topAnchor, constant: 16).isActive = true
//        returnButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
//        returnButton.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 32).isActive = true
//        returnButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -32).isActive = true
        returnButton.anchor(top: topAnchor, left: leadingAnchor, bottom: bottomAnchor, right: trailingAnchor, paddingTop: 5, paddingLeft: 20, paddingBottom: 8, paddingRight: 20)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
