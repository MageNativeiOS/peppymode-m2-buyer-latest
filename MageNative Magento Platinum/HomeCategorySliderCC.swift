//
//  HomeCategorySliderCC.swift
//  MageNative Magento Platinum
//
//  Created by Hamza Usmani on 21/09/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class HomeCategorySliderCC: UICollectionViewCell {
    
    //MARK: - Properties
    
    static var reuseID: String = "HomeCategorySliderCC"
    
    //MARK: - Views
    
    lazy var container:UIView = {
        let view = UIView()
       // view.layer.cornerRadius = 5
        return view
    }()
    
    
    lazy var categoryImage:UIImageView = {
        let imageView = UIImageView()
        imageView.layer.cornerRadius = 5
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleToFill
        imageView.image = UIImage(named: "placeholder")
        
        return imageView
    }()
    
    lazy var nameLabel:UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.init(hexString: "#f2f2f2")?.withAlphaComponent(0.5)
        label.textColor = UIColor.mageLabel
        label.textAlignment = .center
        label.numberOfLines = 2
        label.font = UIFont.systemFont(ofSize: 15, weight: .semibold)
        return label
    }()
    
    
    //MARK: - Life Cycle
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(container)
        container.anchor(top: topAnchor, left: leadingAnchor, bottom: bottomAnchor, right: trailingAnchor, paddingTop: 8, paddingLeft: 8, paddingBottom: 2, paddingRight: 8)
        
        
       container.addSubview(categoryImage)
        categoryImage.anchor(top: container.topAnchor, left: container.leadingAnchor , right: container.trailingAnchor, paddingTop: 5, paddingLeft: 2, paddingRight: 2)//addConstraintsToFillView(container)
        
        container.addSubview(nameLabel)
        nameLabel.anchor(top: categoryImage.bottomAnchor ,left: container.leadingAnchor, bottom: container.bottomAnchor, right: container.trailingAnchor, paddingLeft: 2, paddingBottom: 0, paddingRight: 2, height: 40)
       
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Helper Functions
    
    func populate(with data: HomeCategory) {
        print(data.image)
        categoryImage.sd_setImage(with: URL(string: data.image ?? ""), placeholderImage: UIImage(named: "placeholder"))
        nameLabel.text = data.name
    }
    
    
}
