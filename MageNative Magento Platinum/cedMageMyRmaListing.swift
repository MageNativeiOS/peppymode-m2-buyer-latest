//
//  cedMageMyRmaListing.swift
//  MageNative Magento Platinum
//
//  Created by CEDCOSS Technologies Private Limited on 04/07/17.
//  Copyright © 2017 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class cedMageMyRmaListing: MagenativeUIViewController,UITableViewDelegate,UITableViewDataSource {
  
  @IBOutlet weak var tableView: UITableView!
  var rmaData = [[String:String]]()
  var custid = String()
  override func viewDidLoad() {
    super.viewDidLoad()
    tableView.delegate = self
    tableView.dataSource = self
    // Do any additional setup after loading the view.
    if defaults.bool(forKey: "isLogin") {
      userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
      if let custId = userInfoDict["customerId"] {
        custid = custId
        self.sendRequest(url: "mobiconnect/mobirma/listrma", params: ["customer_id":custId],store:false)
      }
    }
    else {
      self.view.makeToast("Please Login First", duration: 1.5, position: .center)
    }
    
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
    return rmaData.count
  }
  
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    if  let rma_id = rmaData[indexPath.row]["rma_id"] {
      let viewcontroll = UIStoryboard(name: "cedMageAccounts", bundle: nil).instantiateViewController(withIdentifier: "rmaView") as! cedMageRMAView
      print("RMAID")
      print(rma_id)
      viewcontroll.rma_id = rma_id
      self.navigationController?.pushViewController(viewcontroll, animated: true)
    }
  }
  
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "rmaCell") as? cedMageRMAList
    let rma = rmaData[indexPath.row]
    cell?.billTo.text = rma["bill_to"]
    cell?.date.text = rma["date"]
    cell?.orderId.text = rma["order"]
    cell?.rmaId.text = rma["rma_increment_id"]
    cell?.status.text = rma["status"]
    cell?.vendor.text = rma["vendor"]
    cell?.deleteBtnHeight.constant = 0.0
    if rma["status"] != "Cancelled" {
        cell?.deleteButton.backgroundColor = Settings.secondaryColor
      cell?.deleteButton.addTarget(self, action: #selector(cedMageMyRmaListing.cancelRMA(sender:)), for: .touchUpInside)
      cell?.deleteButton.setTitle("Cancel", for: .normal)
      cell?.deleteButton.tag = Int(rma["rma_id"]!)!
        cell?.deleteBtnHeight.constant = 30.0
    }
    //cell?.backgroundColor = .red
    cell?.cardView()
    return cell!
  }
  
  @objc func cancelRMA(sender:UIButton){
    let id = String(sender.tag)
    self.sendRequest(url: "mobiconnect/mobirma/cancelrma", params: ["rma_id":id,"customer_id":custid],store:false)
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return CGFloat(250.0)
  }
  
  
  override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
    if let data = data {
      do {
        guard var json = try? JSON(data:data) else {return;}
        json  = json[0]
        if requestUrl  == "mobiconnect/mobirma/cancelrma" {
          print(json)
          if json["data"]["success"].stringValue == "true" {
            
            self.sendRequest(url: "mobiconnect/mobirma/listrma", params: ["customer_id":custid],store:false)
          }else{
            cedMageHttpException.showAlertView(me: self, msg: json["data"]["message"].stringValue, title: "Error".localized)
          }
        }else{
          
          print(json)
          if json["data"]["success"].stringValue == "true" {
            if rmaData.count > 0 {
              rmaData.removeAll()
            }
            for rma in json["data"]["rma_info"].arrayValue {
              let rma_id = rma["rma_id"].stringValue
              let vendor = rma["vendor"].stringValue
              let status = rma["status"].stringValue
              let order = rma["order"].stringValue
              
              let date = rma["date"].stringValue
              let bill_to = rma["bill_to"].stringValue
              let rma_increment_id = rma["rma_increment_id"].stringValue
              
              self.rmaData.append(["rma_id":rma_id,"vendor":vendor,"status":status,"order":order,"date":date,"rma_increment_id":rma_increment_id,"bill_to":bill_to])
            }
            tableView.reloadData()
            
          }else{
            self.view.makeToast(json["data"]["message"].stringValue, duration: 2.0, position: .center)
          }
        }
      }catch let error {
        print(error.localizedDescription)
      }
    }
  }
}
