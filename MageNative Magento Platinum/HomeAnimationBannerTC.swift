//
//  HomeAnimationBannerTC.swift
//  MageNative Magento Platinum
//
//  Created by Hamza Usmani on 22/09/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class HomeAnimationBannerTC: UITableViewCell {
    
    //MARK: - Properties
    
    static var reuseID:String = "HomeAnimationBannerTC"
    var banner: HomeBanner! { didSet {
        animationImage.sd_setImage(with: URL(string: banner.banner_image ?? ""), placeholderImage: UIImage(named: "placeholder"))
        }
    }
    weak var parent: UIViewController?
    
    //MARK: - Views
    
    lazy var animationImage:UIImageView = {
        let imageView = UIImageView()
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleToFill
        return imageView
    }()
    
    
    //MARK: - Life Cycle
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        backgroundColor = .clear
        
        addSubview(animationImage)
        animationImage.anchor(top: topAnchor, left: leadingAnchor, bottom: bottomAnchor, right: trailingAnchor, paddingTop: 2, paddingLeft: 2, paddingBottom: 2, paddingRight: 2)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    
    
}

