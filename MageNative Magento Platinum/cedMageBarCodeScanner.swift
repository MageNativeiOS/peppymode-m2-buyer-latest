/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */

import UIKit
import AVFoundation
class cedMageBarCodeScanner: MagenativeUIViewController,AVCaptureMetadataOutputObjectsDelegate {
    
    //let cedMageSession: AVCaptureSession = AVCaptureSession()
    @IBOutlet weak var message_data: UILabel!
    
    @IBOutlet weak var imageView: UIImageView!
    var captureSession: AVCaptureSession!
    var previewLayer: AVCaptureVideoPreviewLayer!
    //    var products:Storefront.ProductConnection?=nil
    //    var imageData=[SDWebImageSource]()
    var profuctIdDigit = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        message_data.text = "No QR Code is Detected".localized
        message_data.fontColorTool()
        view.backgroundColor = UIColor.black
        captureSession = AVCaptureSession()
        
        guard let videoCaptureDevice = AVCaptureDevice.default(for: AVMediaType.video) else{
            return
        }
        let videoInput: AVCaptureDeviceInput
        
        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            return
        }
        
        if (captureSession.canAddInput(videoInput)) {
            captureSession.addInput(videoInput)
        } else {
            failed()
            return
        }
        
        let metadataOutput = AVCaptureMetadataOutput()
        
        if (captureSession.canAddOutput(metadataOutput)) {
            captureSession.addOutput(metadataOutput)
            
            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = [AVMetadataObject.ObjectType.qr]
        } else {
            failed()
            return
        }
        
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.frame = view.layer.bounds
        // previewLayer.videoGravity =
        view.layer.addSublayer(previewLayer)
        // view.bringSubview(toFront: imageView)
        view.bringSubviewToFront(message_data)
        captureSession.startRunning()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func failed() {
        let ac = UIAlertController(title: "Scanning not supported".localized, message: "Your device does not support scanning a code from an item. Please use a device with a camera.".localized, preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK".localized, style: .default))
        ac.modalPresentationStyle = .fullScreen;
        present(ac, animated: true)
        captureSession = nil
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        message_data.text = "No QR Code is Detected".localized
        message_data.fontColorTool()
        if (captureSession?.isRunning == false) {
            captureSession.startRunning()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if (captureSession?.isRunning == true) {
            captureSession.stopRunning()
        }
    }
    
    func metadataOutput(_ captureOutput: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        captureSession.stopRunning()
        
        if let metadataObject = metadataObjects.first {
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
            guard let stringValue = readableObject.stringValue else { return }
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            message_data.text = stringValue
            message_data.fontColorTool()
            found(code: stringValue)
        }
        
        dismiss(animated: true)
    }
    
    func found(code: String) {
        print(code)
        
        let fileName = code
        var fileArray = fileName.components(separatedBy: "#")
        let finalFileName = fileArray.first
        print("###3")
        
        //    print(finalFileName)
        let text = finalFileName?.components(separatedBy: "/")
        let productId = text?.last
        let storyboard = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil);
        let viewController = storyboard.instantiateViewController(withIdentifier: "productSinglePageViewController") as! ProductSinglePageViewController;
        viewController.product_id = productId!;
        
        viewController.parentView = viewController
        self.navigationController?.pushViewController(viewController, animated: true)
        
        
        
        
        
        
        //  profuctIdDigit=(code.components(separatedBy: "/"))
        //   print(profuctIdDigit.last!)
        //   gotoProduct(strn: profuctIdDigit.last!)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}


