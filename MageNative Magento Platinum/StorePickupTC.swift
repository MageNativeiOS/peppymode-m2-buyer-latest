//
//  StorePickupTC.swift
//  MageNative Magento Platinum
//
//  Created by Hamza Usmani on 21/08/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class StorePickupTC: UITableViewCell {
    
    static var reuseID: String = "StorePickupTC"
    
    var data = ["Store Name", "Store Manager Name", "Pickup Date", "Store Address", "Store City", "Store State", "Store Country", "Store Zipcode", "Store Contact"]
    
    lazy var headingLabel:UILabel = {
        let label = UILabel()
        label.text = "Store Pickup"
        label.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    
    
    let verticalStack: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.distribution = .fillProportionally
        stack.spacing = 5
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        
        addSubview(headingLabel)
        headingLabel.topAnchor.constraint(equalTo: topAnchor, constant: 8).isActive = true
        headingLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16).isActive = true
        
        
        addSubview(verticalStack)
        verticalStack.topAnchor.constraint(equalTo: headingLabel.bottomAnchor, constant: 8).isActive = true
        verticalStack.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 32).isActive = true
        verticalStack.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -32).isActive = true
        
        createLabel()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    func createLabel() {
        verticalStack.subviews.forEach({ $0.removeFromSuperview() })
        
        for heading in data {
            let label = UILabel()
            label.text = heading
            label.translatesAutoresizingMaskIntoConstraints = false
            label.heightAnchor.constraint(equalToConstant: 20).isActive = true
            label.font = .systemFont(ofSize: 13, weight: .medium)
            verticalStack.addArrangedSubview(label)
        }
        
        
    }
    
    
    
    
}
