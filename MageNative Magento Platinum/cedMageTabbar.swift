/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */


import UIKit

class cedMageTabbar: UITabBarController ,UITabBarControllerDelegate{
    
    @IBOutlet weak var tabbar: UITabBar!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        let colorString = cedMage.getInfoPlist(fileName:"cedMage",indexString: "themeColor") as! String
        //self.tabBar.barTintColor = UIColor.blueColor()
        self.tabBar.barTintColor = cedMage.UIColorFromRGB(colorCode: colorString)
        //self.tabBar.backgroundImage =   UIImage.imageFromColor(color: cedMage.UIColorFromRGB(colorCode: colorString), frame: CGRect(x: 0, y: 0, width: 64, height: 360))
        self.tabBar.shadowImage = UIImage()
        self.tabBar.isTranslucent = true
    self.tabBar.tintColor = UIColor.init(hexString: "#FFFFFF")
        self.tabBar.unselectedItemTintColor = .white
    }
    
    override func viewWillAppear(_ animated: Bool) {
        _ = self.tabBar.items as NSArray?
        for item in (self.tabBar.items)! {
            print("tabbar item--")
            print(item)
            item.title = item.title?.localized
            print(item.title)
            let unselectedItem = [NSAttributedString.Key.foregroundColor: UIColor.white]
            let selectedItem = [NSAttributedString.Key.foregroundColor: UIColor.white]
            item.setTitleTextAttributes(unselectedItem, for: .normal)
            item.setTitleTextAttributes(selectedItem, for: .selected)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        if let viewController = viewController as? UINavigationController {
            viewController.popToRootViewController(animated: false)
        }
        
    }
   

}
