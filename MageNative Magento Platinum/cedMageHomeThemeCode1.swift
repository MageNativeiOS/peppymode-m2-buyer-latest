/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */

import UIKit
import AVFoundation
class cedMageHomeThemeCode1:MagenativeUIViewController,UIGestureRecognizerDelegate,KIImagePagerDelegate,KIImagePagerDataSource {
    var categoryMainData  = [Int:[[String:String]]]()
    var categoryBannerdata = [Int:[String]]()
    var categoryId = String()
    var bannerArray = [[String:String]]()
    var product_array = [[String:String]]()
    var category_array = [[String:String]]()
    var banner_image_array = [String]()
    var selectedCategory = String()
    var dealGroup = [[String:String]]()
    var deals     = [[String:String]]()
    var dealsPro     = [String:[[String:String]]]()
    let bounds = UIScreen.main.bounds
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.basicFoundationToRenderView(topMargin: 2)
        self.getRequest(url: "mobiconnect/module/gethomepage/1/",store:true)
        self.getRequest(url: "mobiconnectdeals/getdealgroup/",store:true)
        self.sendScreenView(name: "Category")
        scrollView.bounces = false
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
        cedMageLoaders.removeLoadingIndicator(me: self)
        guard let json = try? JSON(data: data!) else{return;}
        print("------cedhomepageurl=\(String(describing: requestUrl))")
        print("------cedhomepagedata=\(data)");
        print("------response=\(response)");
        if(requestUrl == "mobiconnect/module/gethomepage/1/"){
            print(json)
            
            self.parse_product(json: json, index: "category")
        }else if(requestUrl == "mobiconnectdeals/getdealgroup/"){
            print(json)
            self.parse_product(json: json, index: "deal_products")
        }
        else {
            self.parse_product(json: json, index: "category")
        }
        self.banner_image_array = [String]()
        for image in self.bannerArray{
            self.banner_image_array.append(image["banner_image"]!)
        }
        
    }
    
    
    
    
    //Mark: Tap gesture subcategoryCard Tap
    
    func subcateTap(_ recognizer: UITapGestureRecognizer){
        
    }
    
    //Mark :Touch up outside
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
    //Mark: Deal Clicked
    
    @objc func dealClicked(_ recognizer: UITapGestureRecognizer){
        var deal = [String:String]()
        if  let view = recognizer.view as? cedMageDealsPro {
            deal = (dealsPro[String(view.dealsImage.tag)]?[view.cedMageofferText.tag])!
            print(deal)
            
        }else  if  let view = recognizer.view as? cedMageDealOver{
            deal = (dealsPro[String(view.dealTitle.tag)]?[view.dealImage.tag])!
            print(deal)
        }
        
        if(deal["deal_type"] ==  "1"){
            let productview = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "rootPageViewController") as! cedMageProductViewRoot
            productview.pageData = [["product_id":deal["relative_link"]]]
            let instance = cedMage.singletonInstance
            instance.storeParameterInteger(parameter: 0)
            self.navigationController?.pushViewController(productview
                , animated: true)
        }else if(deal["deal_type"] ==  "2"){
            if let productLink = deal["relative_link"] {
                if  productLink != "" {
                    if let contol = UIStoryboard(name: "categorylayouts", bundle: nil).instantiateViewController(withIdentifier: "cedMageDefaultCollection") as?  cedMageDefaultCollection{
                        contol.selectedCategory = productLink
                        self.navigationController?.pushViewController(contol, animated: true)
                    }
                }
            }
        }else if(deal["deal_type"] ==  "3"){
            let view = UIStoryboard(name: "cedMageAccounts", bundle: nil)
            let viewControl = view.instantiateViewController(withIdentifier: "cmsWebView") as! cedMageCmsWebView
            viewControl.pageUrl = deal["relative_link"]!
            self.navigationController?.pushViewController(viewControl, animated: true)
        }
        
    }
    
    
    
    //MARK: Delegate for KIImage Pager
    
    public func array(withImages pager: KIImagePager!) -> [Any]! {
        if banner_image_array.count == 0 {
            return ["placeholder" as AnyObject]
        }
        return banner_image_array as [AnyObject]?
    }
    
    
    func contentMode(forImage image: UInt, in pager: KIImagePager!) -> UIView.ContentMode {
        return UIView.ContentMode.scaleToFill
    }
    
    func placeHolderImage(for pager: KIImagePager!) -> UIImage! {
        return UIImage(named: "placeholder")
    }
    
    func imagePager(_ imagePager: KIImagePager!, didSelectImageAt index: UInt) {
        let banner = bannerArray[Int(index)]
        if(banner["link_to"] == "category"){
            let viewcontoller = UIStoryboard(name: "categorylayouts", bundle: nil).instantiateViewController(withIdentifier: "categoryTheme2") as? cedMageCategoryTheme2
            viewcontoller?.categoryId = banner["product_id"]!
            self.navigationController?.pushViewController(viewcontoller!, animated: true)
        }else if (banner["link_to"] == "product"){
            let productview = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "rootPageViewController") as! cedMageProductViewRoot
            productview.pageData = [["product_id":banner["product_id"]!]]
            let instance = cedMage.singletonInstance
            instance.storeParameterInteger(parameter: 0)
            self.navigationController?.pushViewController(productview
                , animated: true)
            
        }else if (banner["link_to"] == "website"){
            let view = UIStoryboard(name: "cedMageAccounts", bundle: nil)
            let viewControl = view.instantiateViewController(withIdentifier: "cmsWebView") as! cedMageCmsWebView
            let url = banner["product_id"]
            viewControl.pageUrl = url!
            self.navigationController?.pushViewController(viewControl, animated: true)
        }
    }
    
    
    
    func parse_product(json:JSON,index:String){
        print(json)
        if(index == "category"){
            print("------banner---\(json[0]["data"]["banner"][0])")
            if(json[0]["data"]["banner"].arrayValue.count > 0){
                for result in json[0]["data"]["banner"].arrayValue {
                    
                    let product_id = result["product_id"].stringValue
                    print("---\(product_id)")
                    let id = result["id"].stringValue
                    let title = result["title"].stringValue
                    let link_to = result["link_to"].stringValue
                    let banner_image = result["banner_image"].stringValue
                    let products_obj = ["product_id":product_id, "title":title,"link_to":link_to,"banner_image":banner_image,"id":id]
                    bannerArray.append(products_obj)
                }
                return
            }
            if(json[0]["data"]["category"]["data"]["categories"].count > 0){
                var i = 0
                for (_,subJson) in json[0]["data"]["category"]["data"]["categories"][0] {
                    
                    let maincatename = subJson["main_category_name"].stringValue
                    print("------\(maincatename)")
                    let maincateimage = subJson["main_category_image"].stringValue
                    let maincateId = subJson["main_category_id"].stringValue
                    category_array.append(["category_name":maincatename,"category_image":maincateimage,"categoryId":maincateId])
                    
                    var temp = [[String:String]]()
                    for subcategory in subJson["sub_cats"].arrayValue{
                        
                        var tem2 = [String:String]()
                        tem2["sub_category_id"] = subcategory["sub_category_id"].stringValue
                        tem2["sub_category_name"] = subcategory["sub_category_name"].stringValue
                        tem2["sub_category_image"] = subcategory["sub_category_image"].stringValue
                        temp.append(tem2)
                    }
                    
                    var cateBannerImages = [String]()
                    var cateBannerImagesData = [[String:String]]()
                    
                    if(subJson["category_banner"]["data"]["bannerstatus"].stringValue.lowercased() == "ENABLED".lowercased()){
                        for (_,cateBanner) in subJson["category_banner"]["data"]["banner"]{
                            cateBannerImages.append(cateBanner["banner_image"].stringValue)
                            var catData = [String:String]()
                            catData["link_to"] = cateBanner["link_to"].stringValue
                            catData["product_id"] = cateBanner["product_id"].stringValue
                            catData["link_to"] = cateBanner["link_to"].stringValue
                            cateBannerImagesData.append(catData)
                        }
                    }
                    categoryMainData[Int(maincateId)!] = temp
                    categoryBannerdata[Int(maincateId)!+1] = cateBannerImages
                    categoryMainData[Int(maincateId)!+2] = cateBannerImagesData
                    temp.removeAll()
                    cateBannerImages.removeAll()
                    cateBannerImagesData.removeAll()
                    i += 1
                    //Do something you want
                }
            }
            
        }
        if(index == "deal_products"){
            var j = 0
            for result in json[0]["data"]["deal_products"].arrayValue {
                // print(result)
                let dealgropstart = result["group_start_date"].stringValue
                let groupcreated = result["created_time"].stringValue
                let group_end_date = result["group_end_date"].stringValue
                let dealGroupDuration = result["deal_duration"].stringValue
                let groupImagename = result["group_image_name"].stringValue
                let group_status = result ["group_status"].stringValue
                let groupTitle  = result["title"].stringValue
                let group_id  = result["group_id"].stringValue
                let timer_status = result["timer_status"].stringValue
                let view_all_status = result["view_all_status"].stringValue
                let update_time = result["update_time"].stringValue
                let is_static  = result["is_static"].stringValue
                let deal_link  = result["deal_link"].stringValue
                let dealObject = ["dealgropstart":dealgropstart,"groupcreated":groupcreated,"group_end_date":group_end_date,"dealGroupDuration":dealGroupDuration,"groupImagename":groupImagename,"group_status":group_status,"groupTitle":groupTitle,"group_id":group_id,"timer_status":timer_status,"view_all_status":view_all_status,"update_time":update_time,"is_static":is_static,"deal_link":deal_link]
                dealGroup.append(dealObject)
                var i = 0
                for deal in result["content"].arrayValue{
                    let id = deal["id"].stringValue
                    let start_date = deal["start_date"].stringValue
                    let deal_image_name = deal["deal_image_name"].stringValue
                    let relative_link = deal["relative_link"].stringValue
                    let deal_type = deal["deal_type"].stringValue
                    let deal_title = deal["deal_title"].stringValue
                    let category_link = deal["category_link"].stringValue
                    let offer_text = deal["offer_text"].stringValue
                    let status  = deal["status"].stringValue
                    let product_link = deal["product_link"].stringValue
                    let deals_obj = ["id":id, "start_date":start_date,"deal_image_name":deal_image_name,"relative_link":relative_link,"deal_type":deal_type,"deal_title":deal_title,"category_link":category_link,"offer_text":offer_text,"status":status,"product_link":product_link]
                    deals.append(deals_obj)
                    i += 1
                }
                
                dealsPro["\(j)"] = deals;
                deals.removeAll()
                j += 1
            }
        }
        
        //return
    }
    
    
}

extension UIImageView{
    func returnImage()->CGSize{
        let width = self.bounds.width/(self.image?.size.width)!
        let height = self.bounds.height/(self.image?.size.height)!
        
        _ = fminf(Float(width), Float(height))
        
        return CGSize()
        
    }
}


