//
//  SliderCategoryTVCell.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 16/06/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class SliderCategoryTVCell: UITableViewCell {
    var parent    : UIViewController!
    var categories: [HomepageSliderCategoryData]? = []{
        didSet{
            collectionView.reloadData()
        }
    }

    @IBOutlet weak var collectionView: UICollectionView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        collectionView.delegate     = self
        collectionView.dataSource   = self
        self.selectionStyle         = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}

extension SliderCategoryTVCell: UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categories?.count ?? 0
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SliderCategoryCVCell", for: indexPath) as! SliderCategoryCVCell
        cell.category = categories?[indexPath.row]
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.width/4 - 1), height: collectionView.frame.height)
    }
}

extension SliderCategoryTVCell: UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let category  = categories?[indexPath.row]
        let vc = UIStoryboard(name: "categorylayouts", bundle: nil).instantiateViewController(withIdentifier: "cedMageDefaultCollection") as! cedMageDefaultCollection
        vc.selectedCategory = category?.id ?? ""
        parent.navigationController?.pushViewController(vc, animated: true)
    }
}

