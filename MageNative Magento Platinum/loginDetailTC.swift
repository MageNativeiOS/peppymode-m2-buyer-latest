//
//  loginDetailTC.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 24/07/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class loginDetailTC: UITableViewCell {

    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var userName: UITextField!
       @IBOutlet weak var password: UITextField!

    @IBOutlet weak var fbLogin: FBLoginButton!
    @IBOutlet weak var googleLogin: GIDSignInButton!
       @IBOutlet weak var registerHere: UIButton!
       @IBOutlet weak var forgetPassword: UIButton!
       @IBOutlet weak var loginButton: UIButton!
       @IBOutlet weak var orLabel: UILabel!
       @IBOutlet weak var applesignInview: UIView!
    @IBOutlet weak var skipButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
        userName.layer.cornerRadius = 8
         password.layer.cornerRadius = 8
         userName.clipsToBounds = true
         password.clipsToBounds = true
         
         
         userName.placeholder = "UserName".localized
         password.placeholder = "Password".localized
         loginButton.setTitle("Login".localized, for: .normal)
         registerHere.setTitle("Sign Up".localized, for: .normal)
         forgetPassword.setTitle("Forgot Password".localized, for: .normal)
         orLabel.text = "OR".localized
        
         loginButton.setTitleColor( .white,for: .normal)
        
       
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
