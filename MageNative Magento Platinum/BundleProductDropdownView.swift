/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */

import UIKit

class BundleProductDropdownView: UIView {

    // Our custom view from the XIB file
    var view: UIView!
    
    //outlets
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var bundleProDropdownButton: UIButton!
    @IBOutlet weak var qtyLabel: UILabel!
    @IBOutlet weak var qtyToBuy: UITextField!
    
    
    override init(frame: CGRect) {
        // 1. setup any properties here
        
        // 2. call super.init(frame:)
        super.init(frame: frame)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        // 1. setup any properties here
        
        // 2. call super.init(coder:)
        super.init(coder: aDecoder)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    func xibSetup() {
        view = loadViewFromNib()
        
        // use bounds not frame or it'll be offset
        view.frame = bounds
        
        
        topLabel.fontColorTool()
        bundleProDropdownButton.fontColorTool()
        qtyLabel.fontColorTool()
        qtyToBuy.fontColorTool()
        
        
        // Make the view stretch with containing view
        view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        
        
        //extra setup
        topLabel.font = UIFont(fontName: "HelveticaNeue-Medium", fontSize: CGFloat(15.0));
        
        bundleProDropdownButton.titleLabel!.font = UIFont(fontName: "", fontSize: CGFloat(15.0));
        bundleProDropdownButton.layer.cornerRadius = CGFloat(5);
        bundleProDropdownButton.layer.borderWidth =  CGFloat(1);
        bundleProDropdownButton.layer.borderColor = UIColor(hexString: "#3498DB")?.cgColor;
        let value=UserDefaults.standard.value(forKey: "mageAppLang") as! [String]
        if value[0]=="ar"
        {
            bundleProDropdownButton?.contentHorizontalAlignment = .right
            //accountButton?.titleLabel?.textAlignment = .right
        }
        else
        {
            bundleProDropdownButton?.contentHorizontalAlignment = .left
            //accountButton?.titleLabel?.textAlignment = .left
        }
        qtyLabel.font = UIFont(fontName: "", fontSize: CGFloat(15.0));
        qtyToBuy.font = UIFont(fontName: "", fontSize: CGFloat(15.0));
//        self.makeCard(self, cornerRadius: 2, color: UIColor.black, shadowOpacity: 0.4);
        //extra setup
        
        
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "BundleProductDropdownView", bundle: bundle)
        
        // Assumes UIView is top level and only object in CustomView.xib file
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
}
