//
//  cedMagepaymentmethodCell.swift
//  MageNative Magento Platinum
//
//  Created by CEDCOSS Technologies Private Limited on 20/07/17.
//  Copyright © 2017 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class cedMagepaymentmethodCell: UITableViewCell {
    @IBOutlet weak var paymentImage: UIImageView!
    @IBOutlet weak var title: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
