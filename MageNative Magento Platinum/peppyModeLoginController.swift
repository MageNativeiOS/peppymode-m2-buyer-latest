//
//  peppyModeLoginController.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 24/07/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit
import LocalAuthentication
import AuthenticationServices

class peppyModeLoginController: MagenativeUIViewController,LoginButtonDelegate,GIDSignInDelegate {
    
    @IBOutlet weak var animateView: UIView!
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var loginTable: UITableView!
    var email = ""
    var password = ""
    var isAnimate = false
    var total=[String:String]()
    var isFromCheckOut = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // animateImage()
        loginTable.isHidden = true
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        animateImage()
        cedMage.delay(delay: 3.0) {
            UIView.animate(withDuration: 1.0) {
                self.loginTable.alpha = 1.0
                self.loadTable()
                self.loginTable.isHidden = false
            }
            
        }
        self.navigationController?.navigationBar.isHidden = true
        self.tabBarController?.tabBar.isHidden = true
    }
    func animateImage(){
        UIView.animateKeyframes(withDuration: 2, delay: 0, options: .calculationModeCubic, animations: {
            
            UIView.animate(withDuration: 1, delay: 0, options: [.curveEaseOut],
                           animations: {
                            self.animateView.center.y -= self.view.bounds.height/2
                            self.animateView.layoutIfNeeded()
            }, completion: nil)
            
        }, completion: nil)
        
        
    }
    func loadTable(){
        loginTable.separatorStyle = .none
        loginTable.delegate = self
        loginTable.dataSource = self
        loginTable.reloadData()
    }
    override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
        
        print(NSString(data: data!, encoding: String.Encoding.utf8.rawValue) as Any)
        print("!@!@!@!@!@")
        guard let response = try? JSON(data:data!) else{return;}
        if(response[0]["data"]["customer"][0]["status"].stringValue=="success"){
            print(response)
            if response[0]["data"]["customer"][0]["isConfirmationRequired"].stringValue.lowercased() == "YES".lowercased(){
                LoginManager().logOut()
                cedMageHttpException.showAlertView(me: self, msg: response[0]["data"]["customer"][0]["message"].stringValue, title: "Error")
                return
            }
            let customer_id = response[0]["data"]["customer"][0]["customer_id"].stringValue;
            let hashKey = response[0]["data"]["customer"][0]["hash"].stringValue;
            let cart_summary = response[0]["data"]["customer"][0]["cart_summary"].intValue;
            let name=response[0]["data"]["customer"][0]["name"].stringValue;
            //saving value in NSUserDefault to use later on :: start
            let dict = ["email": email, "customerId": customer_id, "hashKey": hashKey];
            print(dict)
            print("@$@$@$@$@$@$@$@$")
            self.defaults.set(name, forKey: "name")
            self.defaults.set(true, forKey: "isLogin")
            self.defaults.set(dict, forKey: "userInfoDict");
            self.defaults.set(String(cart_summary), forKey: "items_count");
            self.setCartCount(view: self, items: String(cart_summary))
            NotificationCenter.default.post(name: NSNotification.Name("loadDrawerAgain"), object: nil)
            self.navigationController?.view.makeToast("Login Successful", duration: 2.0, position: .center, title: nil, image: nil, style: nil, completion: nil)
            cedMage.delay(delay: 2, closure: {
                NotificationCenter.default.post(name: NSNotification.Name("loadDrawerAgain"), object: nil);
                self.navigationController?.navigationBar.isHidden = false
                if !self.isFromCheckOut {
//                    let viewControl = UIStoryboard(name: "homeLayouts", bundle: nil).instantiateViewController(withIdentifier: "peppyModeHomeVC") as? peppyModeHomeVC
//                    self.navigationController?.navigationBar.isHidden = false
//                    self.navigationController?.setViewControllers([viewControl!], animated: true)
                    if let myDelegate = UIApplication.shared.delegate as? AppDelegate {
                        myDelegate.setFirstPage()
                    }
                    
                }else{
                    if(cedMage.checkModule(string:"MageNative_Mobicheckout")){
                        print("inside check")
                        let storyboard = UIStoryboard(name: "cedMageAccounts", bundle: nil);
                        let viewController = storyboard.instantiateViewController(withIdentifier: "cedMageAdvancedCheckout") as! cedMageAdvancedCheckout;
                        self.navigationController?.pushViewController(viewController, animated: true);
                    }
                    else{
                        let storyboard = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil);
                        let viewController = storyboard.instantiateViewController(withIdentifier: "checkoutStep2ViewController") as! CheckoutStep2ViewController;
                        viewController.totalOrderData = self.total
                        viewController.checkoutAs = "USER";
                        self.navigationController?.pushViewController(viewController, animated: true);
                    }
                }
                //_ =  self.navigationController?.popToRootViewController(animated: true)
            })
        }else{
            cedMageHttpException.showAlertView(me: self, msg: "Invalid Login or Password", title: "Error".localized)
        }
    }
    
}
extension peppyModeLoginController:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row{
        case 0:
            let cell =  loginTable.dequeueReusableCell(withIdentifier: "loginLogoCell", for: indexPath) as! loginLogoCell
            
            cell.layer.backgroundColor = UIColor.clear.cgColor
            cell.outerView.backgroundColor = .clear
            cell.selectionStyle = .none
            return cell
        case 1:
            let cell =  loginTable.dequeueReusableCell(withIdentifier: "pagerCell", for: indexPath) as! loginLogoCell
            cell.parent = self
            
            cell.slidingBanners = ["loginSlide1","loginSlide2","loginSlide3"]
            
            cell.layer.backgroundColor = UIColor.clear.cgColor
            cell.pagerView.backgroundColor = .clear
            cell.selectionStyle = .none
            return cell
        default:
            let cell = loginTable.dequeueReusableCell(withIdentifier: "loginDetailTC", for: indexPath) as! loginDetailTC
            self.email = cell.userName.text ?? ""
            self.password = cell.password.text ?? ""
            
            
            
            let googlesignin = GIDSignIn.sharedInstance()
            googlesignin?.presentingViewController = self;
            googlesignin?.clientID = cedMage.getInfoPlist(fileName: "GoogleService-Info", indexString: "CLIENT_ID") as? String
            googlesignin?.scopes.append("https://www.googleapis.com/auth/plus.login")
            googlesignin?.scopes.append("https://www.googleapis.com/auth/plus.me")
            cell.fbLogin.delegate = self
            cell.fbLogin.permissions = ["email","public_profile"]
            
            googlesignin?.shouldFetchBasicProfile = true
            googlesignin?.delegate = self
            cell.loginButton.addTarget(self, action: #selector(addLogin(sender:)), for: UIControl.Event.touchUpInside)
            cell.loginButton.tag = indexPath.row
            cell.layer.backgroundColor = UIColor.clear.cgColor
            self.createAppleSignInButton(cell: cell)
            cell.outerView.backgroundColor = .clear
            cell.skipButton.setTitle("Skip For Now", for: .normal)
            cell.skipButton.setThemeColor()
            cell.skipButton.setTitleColor(.white, for: .normal)
            cell.skipButton.layer.cornerRadius = 5.0
            cell.skipButton.addTarget(self, action: #selector(goToHome(_:)), for: .touchUpInside)
            
                      cell.registerHere.layer.cornerRadius = 5.0
                      cell.loginButton.layer.cornerRadius = 5.0
            cell.selectionStyle = .none
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 0
        case 1:
            return 160
        default:
            return 600
        }
    }
}
extension peppyModeLoginController{
    
    @objc func goToHome(_ sender:UIButton){
        UserDefaults.standard.set(true, forKey: "skipLogin")
               if let myDelegate = UIApplication.shared.delegate as? AppDelegate {
                   myDelegate.setFirstPage()
               }
           
    }
    
    //Mark: LOgin Function
    @objc func addLogin(sender:UIButton){
        
        let cell = loginTable.cellForRow(at: IndexPath(row: 2, section: 0)) as! loginDetailTC
        email = cell.userName.text!
        email = email.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        var password = cell.password.text!
        password = password.trimmingCharacters(in:NSCharacterSet.whitespacesAndNewlines);
        if(email == "" || password == "")
        {
            let title = "Error".localized
            let msg = "Email Or Password is Empty.".localized
            cedMageHttpException.showAlertView(me: self, msg: msg, title: title)
            return;
        }
        var postString = ["email":email,"password":password]
        if(defaults.object(forKey: "cartId") != nil)
        {
            let cart_id = defaults.object(forKey: "cartId") as? String
            postString.updateValue(cart_id!, forKey: "cart_id")
        }
        self.sendRequest(url: "mobiconnect/customer/login/", params: postString)
        
    }
    
    
    
    //mark: delegates from google and facebook login
    
    func loginButton(_ loginButton: FBLoginButton!, didCompleteWith result: LoginManagerLoginResult!, error: Error!) {
        self.navigationController?.navigationBar.isHidden = true;
        if(result == nil){
            return
        }
        let graphRequest : GraphRequest = GraphRequest(graphPath: "me", parameters: ["fields":"id,email,name,first_name,last_name"])
        graphRequest.start(completionHandler: { (connection, result, error) -> Void in
            if ((error) != nil)
            {
                print("Error: \(String(describing: error))")
            }
            else
            {
                //let user_id = AccessToken.current?.userID
                let user_id = AccessToken.current!.appID
                let firstname = (result as AnyObject).value(forKey: "first_name") as! String
                let lastname = (result as AnyObject).value(forKey: "last_name") as! String
                if let email = (result as AnyObject).value(forKey: "email") as? String {
                    let fbuser = ["firstname":firstname,"lastname":lastname,"email":email,"type":"facebook", "token":user_id]
                    print(fbuser)
                    self.doLOGIn(data: fbuser)
                }
                else {
                    let storyboard = UIStoryboard(name: "cedMageLogin", bundle: nil);
                    let viewController = storyboard.instantiateViewController(withIdentifier: "cedMageRegister") as! cedMageRegister;
                    self.navigationController?.pushViewController(viewController, animated: true);
                    viewController.Firstname=firstname
                    viewController.Lastname=lastname
                }
            }
        })
    }
    
    func doLOGIn(data:[String:String]){
        
        email=data["email"]!
        var postString = ["firstname":data["firstname"]!,"lastname":data["lastname"]!,"type":data["type"]!, "token":data["token"]! ];
        //postString += "&email="+data["email"]!
        postString.updateValue(data["email"]!, forKey: "email")
        if(defaults.object(forKey: "cartId") != nil)
        {
            let cart_id = defaults.object(forKey: "cartId") as? String;
            //postString += "&cart_id="+cart_id!;
            postString.updateValue(cart_id!, forKey: "cart_id")
        }
        print(postString);
        //        defaults.setValue(data["firstname"], forKey: "name")
        
        self.sendRequest(url:"mobiconnect/sociallogin/create/", params: postString)
    }
    
    func loginButtonDidLogOut(_ loginButton: FBLoginButton!) {
        print("User Logged Out")
        defaults.setValue("MageNative App For Magento 2",forKey: "name");
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        
    }
    
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        viewController.modalPresentationStyle = .fullScreen;
        self.present(viewController, animated: true, completion: nil)
        
    }
    
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        self.navigationController?.navigationBar.isHidden = true;
        self.dismiss(animated: true, completion: nil)
        
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        self.navigationController?.navigationBar.isHidden = true;
        
        if (error == nil) {
            let user_id=user.userID!
            // Perform any operations on signed in user here.
            let givenName = user.profile.givenName as String
            let familyName = user.profile.familyName as String
            let email = user.profile.email as String
            let user = ["firstname":givenName,"lastname":familyName,"email":email,"type":"google", "token":user_id]
            print("--------Google------")
            print(email)
            self.doLOGIn(data: user)
            // ...
        } else {
            print("\(error.localizedDescription)")
        }
    }
    
    
    /*  func animationUIView(endUIViewAttribute: UIViewAttribute, centerUIView : Bool, durationSeconds:Float, delaySeconds:Float, usingSpringWithDamping: Float, initialSpringVelocity: Float) -> Void{
     
     // Define a swift closure type.
     typealias animationClosure = () -> Void
     
     // Declare an object of above swift closure type.
     let animationClosureVar : animationClosure
     
     // Implement the closure execution code, then animation will trasform the UIView object form original state to the UIView state defined in this closure.
     animationClosureVar = {
     
     () -> Void in
     
     print("Run animationClosure. ")
     
     // Get UIView object properties when the animation stop.
     let endX = endUIViewAttribute.x
     
     let endY = endUIViewAttribute.y
     
     let endWidth = endUIViewAttribute.width
     
     let endHeight = endUIViewAttribute.height
     
     // Set the UIView object target properties.
     self.redView.frame = CGRect(x:CGFloat(endX), y:CGFloat(endY), width:CGFloat(endWidth), height:CGFloat(endHeight))
     // Set the UIView object's color to green.
     self.animateView.backgroundColor = UIColor.green
     
     }
     
     // Define a closure type when the animation complete.
     typealias animationCompletionClosure = (_ finished : Bool) -> Void
     // Implement above closure type.
     let animationCompletionClosureVar : animationCompletionClosure = {
     
     (finished) -> Void in
     
     print("Run animationCompletionClosure. ")
     
     // When the animation complete, if centerUIView parameter's value is true then center the UIView object.
     if(centerUIView){
     // get screen size object.
     let screenSize: CGRect = UIScreen.main.bounds
     
     // get screen width.
     let screenWidth = screenSize.width
     
     // get screen height.
     let screenHeight = screenSize.height
     
     self.animateView.center = CGPoint(x: screenWidth/2, y: screenHeight/2)
     
     self.animateView.backgroundColor = UIColor.red
     }
     }
     
     
     //UIView.animate(withDuration: 10, animations: animationClosureVar, completion: animationCompletionClosureVar)
     
     // Call the UIView.animate function to implement the animation.
     UIView.animate(withDuration: TimeInterval(durationSeconds), delay: TimeInterval(delaySeconds), usingSpringWithDamping: CGFloat(usingSpringWithDamping), initialSpringVelocity: CGFloat(initialSpringVelocity), options: UIView.AnimationOptions.curveEaseInOut, animations: animationClosureVar, completion: animationCompletionClosureVar)
     }*/
}
//MARK:- Apple sign in functions
extension peppyModeLoginController : ASAuthorizationControllerDelegate, ASAuthorizationControllerPresentationContextProviding
{
    func createAppleSignInButton(cell:loginDetailTC)
    {
        if #available(iOS 12.0, *) {
            let isDarkTheme = view.traitCollection.userInterfaceStyle == .dark
            if #available(iOS 13.0, *) {
                let style: ASAuthorizationAppleIDButton.Style = isDarkTheme ? .white : .black
                
                let authorizationButton = ASAuthorizationAppleIDButton(type: .default, style: style)
                cell.applesignInview.addSubview(authorizationButton)
                authorizationButton.translatesAutoresizingMaskIntoConstraints = false;
                authorizationButton.center = cell.applesignInview.center
                
                let heightConstraint = authorizationButton.heightAnchor.constraint(equalToConstant: 45)
                authorizationButton.addConstraint(heightConstraint)
                cell.applesignInview.addConstraint(NSLayoutConstraint(item: authorizationButton, attribute: .trailing, relatedBy: .equal, toItem: cell.applesignInview, attribute: .trailing, multiplier: 1, constant: 0))
                cell.applesignInview.addConstraint(NSLayoutConstraint(item: authorizationButton, attribute: .leading, relatedBy: .equal, toItem: cell.applesignInview, attribute: .leading, multiplier: 1, constant: 0))
                cell.applesignInview.layer.cornerRadius = 7;
                authorizationButton.addTarget(self, action: #selector(handleLogInWithAppleIDButtonPress), for: .touchUpInside)
            } else {
                // Fallback on earlier versions
            }
            
            // Create and Setup Apple ID Authorization Button
            
        } else {
            // Fallback on earlier versions
        }
    }
    
    
    @objc func handleLogInWithAppleIDButtonPress() {
        if #available(iOS 13.0, *) {
            let appleIDProvider = ASAuthorizationAppleIDProvider()
            let request = appleIDProvider.createRequest()
            request.requestedScopes = [.fullName, .email]
            
            let authorizationController = ASAuthorizationController(authorizationRequests: [request])
            authorizationController.delegate = self
            authorizationController.presentationContextProvider = self
            authorizationController.performRequests()
        } else {
            // Fallback on earlier versions
        }
        
    }
    
    @available(iOS 13.0, *)
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }
    
    private func performExistingAccountSetupFlows() {
        // Prepare requests for both Apple ID and password providers.
        if #available(iOS 13.0, *) {
            let requests = [ASAuthorizationAppleIDProvider().createRequest(), ASAuthorizationPasswordProvider().createRequest()]
            // Create an authorization controller with the given requests.
            let authorizationController = ASAuthorizationController(authorizationRequests: requests)
            authorizationController.delegate = self
            authorizationController.presentationContextProvider = self
            authorizationController.performRequests()
        } else {
            // Fallback on earlier versions
        }
        
        
    }
    
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        //Handle error here
        print("error = \(error.localizedDescription)")
    }
    
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        
        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
            let userName = appleIDCredential.fullName?.description
            let userEmail = appleIDCredential.email
            
            print(userName)
            print(userEmail)
            // Create an account in your system.
            print("User Id - \(appleIDCredential.user)")
            print("User Name - \(appleIDCredential.fullName?.description ?? "N/A")")
            print("User Email - \(appleIDCredential.email ?? "N/A")")
            print("Real User Status - \(appleIDCredential.realUserStatus.rawValue)")
            
            if let identityTokenData = appleIDCredential.identityToken,
                let identityTokenString = String(data: identityTokenData, encoding: .utf8) {
                print("Identity Token \(identityTokenString)")
            }
            
            if userName == "" || userEmail == nil || userEmail == ""
            {
                self.signInWithAppleRequest(params: ["token":appleIDCredential.user,"type":"apple"])
            }
            else
            {
                self.signInWithAppleRequest(params: ["token":appleIDCredential.user,"firstname":appleIDCredential.fullName?.givenName ?? "","lastname":appleIDCredential.fullName?.familyName ?? "","email":appleIDCredential.email ?? "","type":"apple"])
            }
            
            /* -- old
             self.signInWithAppleRequest(params: ["token":appleIDCredential.user,"firstname":appleIDCredential.fullName?.givenName ?? "","lastname":appleIDCredential.fullName?.familyName ?? "","email":appleIDCredential.email ?? "","type":"apple"])
             
             */
            
            //Show Home View Controller
            
        } else if let passwordCredential = authorization.credential as? ASPasswordCredential {
            // Sign in using an existing iCloud Keychain credential.
            let username = passwordCredential.user
            let password = passwordCredential.password
            
            // For the purpose of this demo app, show the password credential as an alert.
            DispatchQueue.main.async {
                let message = "The app has received your selected credential from the keychain. \n\n Username: \(username)\n Password: \(password)"
                let alertController = UIAlertController(title: "Keychain Credential Received",
                                                        message: message,
                                                        preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
    
    
    func signInWithAppleRequest(params: [String:String])
    {
        print("inside signInWithAppleRequest")
        print("my params = \(params)")
        if let emailId = params["email"]
        {
            email = emailId
        }
        print("my email : \(email)")
        self.sendRequest(url:"mobiconnect/sociallogin/create", params: params)
        /*
         rest/V1/mobiconnect/sociallogin/create
         {"parameters":{"email":"evamorgan008@gmail.com","firstname":"Eva","gender":"1","type":"google","token":"32523423423423"}}
         */
    }
    
}
