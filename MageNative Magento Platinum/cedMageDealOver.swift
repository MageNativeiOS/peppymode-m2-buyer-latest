//
//  cedMageDealOver.swift
//  MageNative Magento Platinum
//
//  Created by CEDCOSS Technologies Private Limited on 24/10/16.
//  Copyright © 2016 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class cedMageDealOver: UIView {

    @IBOutlet weak var dealImage: UIImageView!
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var dealTitle: UILabel!
    var view:UIView!
    override init(frame: CGRect)
    {
        // 1. setup any properties here
        
        // 2. call super.init(frame:)
        super.init(frame: frame)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        // 1. setup any properties here
        
        // 2. call super.init(coder:)
        super.init(coder: aDecoder)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    func xibSetup()
    {
        view = loadViewFromNib()
        
        // use bounds not frame or it'll be offset
        view?.frame = bounds
        
        // Make the view stretch with containing view
        view?.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
      //  backgroundImage.setThemeColor()
        dealImage.cardView()
        dealTitle.cardView()
        dealImage.layer.borderColor = UIColor.white.cgColor
        dealImage.layer.borderWidth = 2
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view!)
    }
    
    func loadViewFromNib() -> UIView
    {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "cedMageDealOver", bundle: bundle)
        
        // Assumes UIView is top level and only object in CustomView.xib file
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
}
