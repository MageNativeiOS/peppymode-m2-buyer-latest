/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */


import UIKit

class cedMageInfoSliderRoot: cedMageViewController, UIPageViewControllerDelegate , UIPageViewControllerDataSource {
    
    var pageViewController : UIPageViewController!
    var pageData = NSArray()
    var titleView: UIView!
    var tabbarheight:CGFloat!
    var counter = Int()
    override var prefersStatusBarHidden: Bool {
        return true
    }
    override func viewDidLoad() {
        super.viewDidLoad()
       UserDefaults.standard.set(true, forKey: "cedMageStart")
         pageData = cedMage.getInfoPlist(fileName: "cedMage",indexString: "AppDemo" )as! [[String : String]] as NSArray
        //create viewcontroller
        pageViewController = storyboard?.instantiateViewController(withIdentifier: "pageSliderRoot") as! UIPageViewController
        pageViewController.dataSource = self
        let startingViewController = self.viewControllerAtIndex(index: 0)

        let viewcontrolls = [startingViewController!]
        pageViewController.setViewControllers(viewcontrolls, direction: UIPageViewController.NavigationDirection.forward, animated: false, completion: nil)
       // print(pageViewController.)
      
        self.addChild(pageViewController)
        self.view.addSubview(pageViewController.view)
        
        let pageViewRect = self.view.bounds
        pageViewController.view.frame = CGRect(x: pageViewRect.origin.x, y: pageViewRect.origin.y , width: pageViewRect.size.width, height: pageViewRect.size.height)
        
    
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        var index = self.indexOfViewController(viewController: viewController as! cedMageInfoSlider)
        if (index == 0) || (index == NSNotFound) {
            return nil
        }
        index -= 1
        counter = index
        return viewControllerAtIndex(index: index)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {

        
                var index = indexOfViewController(viewController: viewController as! cedMageInfoSlider)
                //println(index)
                if (index == NSNotFound) {
                    return nil
                }
                index += 1;
                if (index == self.pageData.count ) {
                    return nil;
                }
                counter = index
                return viewControllerAtIndex(index: index)
    }

    func viewControllerAtIndex(index: Int) -> cedMageInfoSlider? {
        
        // println(index)
        if (pageData.count == 0) ||
            (index >= pageData.count) {
            return nil
        }
        
        let storyBoard = UIStoryboard(name: "cedMageLogin",bundle: Bundle.main)
        let dataViewController = storyBoard.instantiateViewController(withIdentifier: "pageInfo") as! cedMageInfoSlider
        dataViewController.dataObject = pageData[index] as! [String : String]
        dataViewController.currentIndex = index
        return dataViewController
    }
    
    func indexOfViewController(viewController:cedMageInfoSlider ) -> Int {
        // println(pageData)
        return pageData.index(of: viewController.dataObject)
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
