/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */


import UIKit

class ced_categoryView: UITableViewCell,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var wrapperView: UIView!
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var categoryView: UIImageView!
    var categoryDataSource = [[String:String]]()
    var viewControl = UIViewController()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        collectionView.delegate = self
        collectionView.dataSource = self
        wrapperView.cardView()
       
      
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //Mark:CollectionViewFunctions
    
    private func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categoryDataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "reusedCollection", for: indexPath as IndexPath) as! ced_newCategoryCollectioncell
         cell.cateLabel.text = categoryDataSource[indexPath.row]["sub_category_name"]
        cell.cateLabel.fontColorTool()
        //cell.imageView.image = UIImage(named: categoryDataSource[indexPath.row]["image"]!)
        cedMageImageLoader.shared.loadImgFromUrl(urlString: categoryDataSource[indexPath.row]["sub_category_image"]!,completionHandler: { (image: UIImage?, url: String) in
            DispatchQueue.main.async {
                cell.imageView.image = image
            }
        })
        cell.cardView()
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var height = CGFloat(180)
        if(UIDevice().model.lowercased() == "ipad".lowercased()){
            height = CGFloat(300)
        }
        let width = UIWindow().frame.size.width/3 - 5
        return CGSize(width:width, height:height);
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let contol = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "productListingViewController") as! ProductListingViewController
        contol.categoryId = categoryDataSource[indexPath.row]["sub_category_id"]!
        viewControl.navigationController?.pushViewController(contol, animated: true)
        
    }

}
