///**
// * CedCommerce
// *
// * NOTICE OF LICENSE
// *
// * This source file is subject to the End User License Agreement (EULA)
// * that is bundled with this package in the file LICENSE.txt.
// * It is also available through the world-wide-web at this URL:
// * http://cedcommerce.com/license-agreement.txt
// *
// * @category  Ced
// * @package   MageNative
// * @author    CedCommerce Core Team <connect@cedcommerce.com >
// * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
// * @license      http://cedcommerce.com/license-agreement.txt
// */
//
//
//import UIKit
//import AVFoundation
//class cedMageSubcategoryListing: MagenativeUIViewController,UIScrollViewDelegate,UIGestureRecognizerDelegate {
//    
//    var selectedCategory = String()
//    var subCateGories = [[String:String]]()
//    var products  = [[String:String]]()
//    var sortByArray = [String:String]();
//    var currentView = "list"
//    var searchString = ""; // variable to handle search case
//    var jsonResponse:JSON?
//    var loadMoreData = true
//    var currentpage = 1
//    var eanCode = String()
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        self.basicFoundationToRenderView(topMargin: 5)
//        scrollView.delegate = self;
//        NotificationCenter.default.addObserver(self, selector: #selector(cedMageSubcategoryListing.reloadData(_:)), name: NSNotification.Name(rawValue: "loadProductsAgainId"), object: nil);
//        self.clearViewAndVariables();
//        
//        if(searchString != ""){
//            self.makeRequestToAPI("mobiconnect/category/productSearch/",dataToPost: ["q":searchString, "page":String(currentpage)]);
//        }
//        else if(eanCode != "") {
//            
//            self.makeRequestToAPI("mobiconnect/category/productSearch",dataToPost: ["ean":eanCode, "page":String(currentpage)]);
//        }
//        else{
//            self.makeRequestToAPI("mobiconnect/category/categoryProducts/",dataToPost: ["id":selectedCategory, "page":String(currentpage)]);
//        }
//        
//        // Do any additional setup after loading the view.
//    }
//    
//    func reloadData(_ notification: NSNotification){
//        clearViewAndVariables()
//        if(searchString != ""){
//            self.makeRequestToAPI("mobiconnect/category/productSearch/",dataToPost: ["q":searchString, "page":String(currentpage)]);
//        }
//        else{
//            self.makeRequestToAPI("mobiconnect/category/categoryProducts/",dataToPost: ["id":selectedCategory, "page":String(currentpage)]);
//        }
//    }
//    
//    func clearViewAndVariables(){
//        stackView.subviews.forEach({ $0.removeFromSuperview() });
//        products = [[String:String]]();
//        sortByArray = [String:String]();
//        subCateGories = [[String:String]]();
//        currentpage = 1;
//        loadMoreData = true;
//    }
//    
//    
//    
//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//        // Dispose of any resources that can be recreated.
//    }
//    
//    
//    
//    func renderView(){
//        self.renderListingView()
//        
//    }
//    
//    //Mark: parseData
//    
//    func parseData(json: JSON, index: String){
//        jsonResponse = json
//        if(json.stringValue.lowercased() == "NO_PRODUCTS".lowercased()){
//            if(self.products.count == 0){
//                cedMage().renderNoDataImage(view:self,imageName:"noProduct");
//                return;
//            }
//            else{
//                self.loadMoreData = false;
//            }
//            return
//        }
//        for result in json["data"]["products"].arrayValue {
//            let product_id = result["product_id"].stringValue;
//            let regular_price = result["regular_price"].stringValue;
//            let special_price = result["special_price"].stringValue;
//            let product_name = result["product_name"].stringValue;
//            let product_image = result["product_image"].stringValue;
//            let type = result["type"].stringValue;
//            let review = result["review"].stringValue;
//            let show_both_price = result["show-both-price"].stringValue;
//            let Inwishlist = result["Inwishlist"].stringValue
//            let starting_from = result["starting_from"].stringValue;
//            let from_price = result["from_price"].stringValue;
//            
//            var wishlistItemId = "-1";
//            if(result["wishlist-item-id"] != nil){
//                wishlistItemId = result["wishlist-item-id"].stringValue;
//            }
//            self.products.append(["product_id":product_id,"regular_price":regular_price, "special_price":special_price,"product_name":product_name,"product_image":product_image,"type":type,"review":review,"show-both-price":show_both_price,"starting_from":starting_from,"from_price":from_price,"Inwishlist":Inwishlist,"wishlist-item-id":wishlistItemId]);
//        }
//        
//        if(subCateGories.count == 0){
//            for data in json["data"]["sub_category"].arrayValue{
//                let sucateName = data["category_name"].stringValue
//                let subcateId = data["category_id"].stringValue
//                let has_child = data["has_child"].stringValue
//                let subcateImage = data["category_image"].stringValue
//                subCateGories.append(["subCateName":sucateName,"subcateId":subcateId,"has_child":has_child,"subcatImg":subcateImage])
//                
//            }
//        }
//        for (_,val) in json["data"]["sort"]{
//            for (keyInr,valInr) in val{
//                self.sortByArray[keyInr] = valInr[0].stringValue;
//            }
//        }
//      //  stackView.subviews.forEach({ $0.removeFromSuperview() });
//        renderView()
//    }
//    
//    /*
//     // MARK: - Navigation
//     
//     // In a storyboard-based application, you will often want to do a little preparation before navigation
//     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//     // Get the new view controller using segue.destinationViewController.
//     // Pass the selected object to the new view controller.
//     }
//     */
//    
//    func productCardTapped(_ recognizer: UITapGestureRecognizer){
//       
//        if let index = recognizer.view?.tag {
//         
//            let productInfo = products[index];
//            
//            let productview = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "rootPageViewController") as! cedMageProductViewRoot;
//            productview.pageData = products as NSArray;
//            let instance = cedMage.singletonInstance;
//            instance.storeParameterInteger(parameter: index);
//            self.navigationController?.pushViewController(productview
//                , animated: true);
//        }
//    }
//    
//    
//    func switchViews(sender:UIButton){
//        if(self.currentView == "grid"){
//            self.currentView = "list"
//        }else{
//            self.currentView = "grid"
//        }
//        self.renderListingView()
//    }
//    
//    
//    
//    func renderListingView(){
//        
//        stackView.subviews.forEach({ $0.removeFromSuperview() });
//        let topView = cedMageSubCateTopSect()
////        topView.switchView.addTarget(self, action: #selector(cedMageSubcategoryListing.switchViews(sender:)), for: .touchUpInside)
////        topView.sortbyButton.addTarget(self, action: #selector(cedMageSubcategoryListing.sortbyButtonPressed(sender:)), for: .touchUpInside)
////        topView.filterButton.addTarget(self, action: #selector(cedMageSubcategoryListing.filterButtonPressed(_:)), for: .touchUpInside)
//        topView.heightAnchor.constraint(equalToConstant: 50).isActive = true;
//        stackView.addArrangedSubview(topView)
//        self.setLeadingAndTralingSpaceFormParentView(topView, parentView:stackView)
//        
//        UIView.animate(withDuration: 4, delay: 1, options: [UIViewAnimationOptions.transitionFlipFromRight,  UIViewAnimationOptions.showHideTransitionViews], animations: {
//            if(self.currentView == "grid"){
//                topView.switchImage.rotate360Degrees()
//                topView.switchImage.image = UIImage(named: "grid")
//            }else{
//                topView.switchImage.rotate360Degrees()
//                topView.switchImage.image = UIImage(named: "list")
//            }
//        }, completion: nil)
////        if(subCateGories.count != 0){
////            var subCount:Int
////            if(subCateGories.count == 1 || subCateGories.count == 2){
////                subCount = 1
////            }else if(subCateGories.count/3 == 0){
////                subCount = (subCateGories.count)/3
////            }else{
////                subCount = (subCateGories.count+2)/3
////            }
////            // print(subCount)
////            var i = 0
////            for _ in 0..<subCount{
////                let view = testView()
////                if(subCateGories.count == 1){
////                    view.productName2.text = subCateGories[i]["subCateName"]
////                    let img1Url = subCateGories[i]["subcatImg"]!
////                    if(img1Url != "false"){
////                        cedMageImageLoader.shared.loadImgFromUrl(urlString: img1Url,completionHandler: { (image: UIImage?, url: String) in
////                            DispatchQueue.main.async {
////                                view.productImage2.image = image
////                            }
////                        })
////                    }
////                    let tapGesture = UITapGestureRecognizer(target: self, action: #selector(cedMagenewCatStack.subcateTap(_:)));
////                    view.productTwo?.tag = Int(subCateGories[i]["subcateId"]!)!
////                    view.productTwo.addGestureRecognizer(tapGesture)
////                    view.productImg1.isHidden = true
////                    view.productName3.isHidden = true
////                    view.productImage3.isHidden = true
////                    view.productname1.isHidden = true
////                    
////                }else {
////                    if subCateGories[i]["subCateName"] != nil {
////                        view.productname1.text = subCateGories[i]["subCateName"]
////                        let imgUrl = subCateGories[i]["subcatImg"]!
////                        if(imgUrl != "false"){
////                            cedMageImageLoader.shared.loadImgFromUrl(urlString: imgUrl,completionHandler: { (image: UIImage?, url: String) in
////                                DispatchQueue.main.async {
////                                    view.productImg1.image = image
////                                }
////                            })
////                        }
////                        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(cedMagenewCatStack.subcateTap(_:)));
////                        view.productOne?.tag = Int(subCateGories[i]["subcateId"]!)!
////                        view.productOne.addGestureRecognizer(tapGesture)
////                        
////                    }else{
////                        view.productImg1.isHidden = true
////                        view.productname1.isHidden = true
////                    }
////                    if subCateGories.count>i+1 && subCateGories[i+1]["subCateName"] != nil {
////                        view.productName2.text = subCateGories[i+1]["subCateName"]
////                        let img1Url = subCateGories[i+1]["subcatImg"]!
////                        if(img1Url != "false"){
////                            cedMageImageLoader.shared.loadImgFromUrl(urlString: img1Url,completionHandler: { (image: UIImage?, url: String) in
////                                DispatchQueue.main.async {
////                                    view.productImage2.image = image
////                                }
////                            })
////                        }
////                        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(cedMagenewCatStack.subcateTap(_:)));
////                        view.productTwo?.tag = Int(subCateGories[i+1]["subcateId"]!)!
////                        view.productTwo.addGestureRecognizer(tapGesture)
////                    }else{
////                        view.productImage2.isHidden = true
////                        view.productName2.isHidden = true
////                    }
////                    if subCateGories.count>i+2 && subCateGories[i+2]["subCateName"] != nil {
////                        view.productName3.text = subCateGories[i+2]["subCateName"]
////                        let img2Url = subCateGories[i+2]["subcatImg"]!
////                        if(img2Url != "false"){
////                            cedMageImageLoader.shared.loadImgFromUrl(urlString: img2Url,completionHandler: { (image: UIImage?, url: String) in
////                                DispatchQueue.main.async {
////                                    view.productImage3.image = image
////                                }
////                            })
////                        }
////                        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(cedMagenewCatStack.subcateTap(_:)));
////                        view.productThree?.tag = Int(subCateGories[i+2]["subcateId"]!)!
////                        view.productThree.addGestureRecognizer(tapGesture)
////                    }else{
////                        view.productImage3.isHidden = true
////                        view.productName3.isHidden = true
////                    }
////                }
////                let cartProductListViewHeight = CGFloat(150.0);
////                view.heightAnchor.constraint(equalToConstant: cartProductListViewHeight).isActive = true;
////                stackView.addArrangedSubview(view)
////                self.setLeadingAndTralingSpaceFormParentView(view, parentView: stackView)
////                i += 3
////            }
////        }
//        
//        
//        if(currentView == "grid"){
//            if products.count != 0 {
//                var totalCount = 0
//                if(products.count/2 == 0){
//                    totalCount = products.count/2
//                }else{
//                    totalCount = (products.count+1)/2
//                }
//            
//                var counter = 0;
//                var i = 0
//                while i < totalCount{
//                    let productGridViewHeight = translateAccordingToDevice(CGFloat(352));
//                    let productView = productDetail();
//                    if(products[counter].count != 0){
//                        productView.productView1.tag = counter
//                        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ProductListingViewController.productCardTapped(_:)));
//                        productView.productView1.addGestureRecognizer(tapGesture);
//                        tapGesture.delegate=self;
//                        productView.translatesAutoresizingMaskIntoConstraints = false;
//                        stackView.addArrangedSubview(productView);
//                        productView.heightAnchor.constraint(equalToConstant: productGridViewHeight).isActive = true;
//                        if(products[counter]["Inwishlist"] == "OUT"){
//                            productView.wishList1.setImage(UIImage(named:"LikeEmpty"), for: UIControlState.normal);
//                        }
//                        else{
//                            productView.wishList1.setImage(UIImage(named:"LikeFilled"), for: UIControlState.normal);
//                        }
//                        productView.wishList1.tag = Int(counter);
//                        productView.wishList1.addTarget(self, action: #selector(cedMageSubcategoryListing.wishlistButtonPressed(_:)), for: UIControlEvents.touchUpInside);
//                        //module check
//                        if(!cedMage.checkModule(string:"Ced_MobiconnectWishlist")){
//                            productView.wishList1.isHidden = true;
//                        }
//                        let urlToRequest = products[counter]["product_image"]!;
//                        
//                            if(urlToRequest != "false"){
//                                cedMageImageLoader.shared.loadImgFromUrl(urlString: urlToRequest,completionHandler: { (image: UIImage?, url: String) in
//                                    DispatchQueue.main.async {
//                                        productView.imageView1.image = image
//                                    }
//                                })
//                            
//                        }
//                        self.setLeadingAndTralingSpaceFormParentView(productView, parentView:stackView);
//                        var productName = products[counter]["product_name"]!;
//                        productView.rating1.text = products[counter]["review"]!;
//                        ProductSinglePageViewController().colorRatingView(rating: products[counter]["review"]!,view:productView.starView1)
//                        productView.translatesAutoresizingMaskIntoConstraints = false;
//                        if(products[counter]["starting_from"] != ""){
//                            productName += "\n"+"Starting At : "+products[counter]["starting_from"]!+"\n";
//                            if(products[counter]["from_price"] != ""){
//                                productName += "As Low As : "+products[counter]["from_price"]!;
//                            }
//                        }
//                        else{
//                            productName += "\n"+products[counter]["regular_price"]!;
//                            if(products[counter]["special_price"] != "no_special"){
//                                productName += "\n"+products[counter]["special_price"]!;
//                            }
//                        }
//                        productView.productName1.text = productName
//                    }
//                    
//                    if(counter+1 >= products.endIndex){
//                        productView.productView2.isHidden = true
//                        cedMageLoaders.removeLoadingIndicator(me: self)
//                        return
//                    }
//                    if(products[counter+1].count != 0){
//                        productView.productView2.tag = counter+1
//                        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ProductListingViewController.productCardTapped(_:)));
//                        productView.productView2.addGestureRecognizer(tapGesture);
//                        tapGesture.delegate=self;
//                        if(products[counter+1]["Inwishlist"] == "OUT"){
//                            productView.wishList2.setImage(UIImage(named:"LikeEmpty"), for: UIControlState.normal);
//                        }
//                        else{
//                            productView.wishList2.setImage(UIImage(named:"LikeFilled"), for: UIControlState.normal);
//                        }
//                        productView.wishList2.tag = Int(counter+1);
//                        productView.wishList2.addTarget(self, action: #selector(cedMageSubcategoryListing.wishlistButtonPressed(_:)), for: UIControlEvents.touchUpInside);
//                        //module check
//                        if(!cedMage.checkModule(string:"Ced_MobiconnectWishlist")){
//                            productView.wishList2.isHidden = true;
//                        }
//                        let urlToRequest = products[counter+1]["product_image"]!;
//                     
//                            if(urlToRequest != "false"){
//                                cedMageImageLoader.shared.loadImgFromUrl(urlString: urlToRequest,completionHandler: { (image: UIImage?, url: String) in
//                                    DispatchQueue.main.async {
//                                        productView.imageView2.image = image
//                                    }
//                                })
//                            
//                        }
//                        self.setLeadingAndTralingSpaceFormParentView(productView, parentView:stackView);
//                        var productName = products[counter+1]["product_name"]!;
//                        productView.rating2.text = products[counter+1]["review"]!;
//                        ProductSinglePageViewController().colorRatingView(rating: products[counter]["review"]!,view:productView.starView2)
//                        productView.translatesAutoresizingMaskIntoConstraints = false;
//                        if(products[counter+1]["starting_from"] != ""){
//                            productName += "\n"+"Starting At : "+products[counter+1]["starting_from"]!+"\n";
//                            if(products[counter+1]["from_price"] != ""){
//                                productName += "As Low As : "+products[counter+1]["from_price"]!;
//                            }
//                        }
//                        else{
//                            productName += "\n"+products[counter+1]["regular_price"]!;
//                            if(products[counter+1]["special_price"] != "no_special"){
//                                productName += "\n"+products[counter+1]["special_price"]!;
//                                
//                            }
//                        }
//                        productView.productName2.text = productName
//                    }
//                    i = i+1
//                    counter = counter+2;
//                }
//            }
//            cedMageLoaders.removeLoadingIndicator(me: self)
//        } else {
//            if products.count != 0 {
//                var counter = 0;
//                print(products.count)
//                for product in  products {
//                    //                    let productGridViewHeight = translateAccordingToDevice(CGFloat(200));
//                    let productView = productImageView();
//                    productView.tag = counter
//                    let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ProductListingViewController.productCardTapped(_:)));
//                    productView.addGestureRecognizer(tapGesture);
//                    tapGesture.delegate=self;
//                    productView.translatesAutoresizingMaskIntoConstraints = false;
//                    stackView.addArrangedSubview(productView);
//                    //productView.heightAnchor.constraint(equalToConstant: productGridViewHeight).isActive = true;
//                    if(product["Inwishlist"] == "OUT"){
//                        productView.wishList.setImage(UIImage(named:"LikeEmpty"), for: UIControlState.normal);
//                    }
//                    else{
//                        productView.wishList.setImage(UIImage(named:"LikeFilled"), for: UIControlState.normal);
//                    }
//                    productView.wishList.tag = Int(product["product_id"]!)!;
//                    productView.wishList.addTarget(self, action: #selector(cedMageSubcategoryListing.wishlistButtonPressed(_:)), for: UIControlEvents.touchUpInside);
//                    //module check
//                    if(!cedMage.checkModule(string:"Ced_MobiconnectWishlist")){
//                        productView.wishList.isHidden = true;
//                    }
//                    let urlToRequest = product["product_image"]!;
//                    
//                   
////                    downloadImageFromServer(productView.imageView,urlToRequest:urlToRequest);
//                    cedMageImageLoader.shared.loadImgFromUrl(urlString: urlToRequest, completionHandler: {
//                        image,url in
//                        if let image = image {
//                            productView.imageView.image = image
//                          
//                        }
//                    })
//                    
//                    
//                    self.setLeadingAndTralingSpaceFormParentView(productView, parentView:stackView);
//                    productView.produtName.text = product["product_name"]!;
//                    productView.rating.text = product["review"]!;
//                    // productView.translatesAutoresizingMaskIntoConstraints = false;
//                    ProductSinglePageViewController().colorRatingView(rating: product["review"]!,view:productView.startView)
//                    if(products[counter]["starting_from"] != ""){
//                        productView.productPrice.text = "Starting At : "+product["starting_from"]!;
//                        if(products[counter]["from_price"] != ""){
//                            productView.productPrice2.text = "As Low As : "+product["from_price"]!;
//                        }
//                        else{
//                            productView.productPrice2.isHidden = true;
//                        }
//                    }
//                    else{
//                        
//                        if(product["special_price"] != "no_special"){
//                            let attr = [NSStrikethroughStyleAttributeName:2]
//                            let attribute = NSAttributedString(string:  product["regular_price"]!, attributes: attr)
//                            
//                            
//                            productView.productPrice.attributedText = attribute ;
//                            productView.productPrice2.text = product["special_price"]!;
//                        }
//                        else{
//                            productView.productPrice.text = product["regular_price"]!;
//                            productView.productPrice2.isHidden = true;
//                            productView.specialPriceHeight.constant = 0
//                        }
//                    }
//                    counter = counter+1;
//                }
//            }
//            
//        }
//        
//    }
//    
//    
//    //Mark: Tap gesture subcategoryCard Tap
//    
//    func subcateTap(_ recognizer: UITapGestureRecognizer){
//        let contol = UIStoryboard(name: "categorylayouts", bundle: nil).instantiateViewController(withIdentifier: "cedMageNewCate") as! cedMageSubcategoryListing
//        contol.selectedCategory = String(describing: recognizer.view!.tag)
//        self.navigationController?.pushViewController(contol, animated: true)
//    }
//    
//    
//    func filterButtonPressed(_ sender:UIButton){
//        
//        if(self.searchString != ""){
//            let msg = "No Filters Found";
//            self.view.makeToast(msg, duration: 2.0, position: .bottom, title: nil, image: nil, style: nil, completion: nil);
//            return;
//        }
//        
//        let storyboard = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil);
//        let viewController = storyboard.instantiateViewController(withIdentifier: "productFiltersViewController") as! ProductFiltersViewController;
//        viewController.jsonForFilters = self.jsonResponse;
//        self.navigationController?.pushViewController(viewController, animated: true);
//    }
//    
//    
//    //No Data Image
//    
//    
//    
//    
//    
//    override func makeRequestToAPI(_ urlToRequest:String, dataToPost:[String:String]){
//        var urlToRequest = urlToRequest
//        
//        if(defaults.object(forKey: "userInfoDict") != nil){
//            userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
//        }
//        
//        let requestHeader = cedMage.getInfoPlist(fileName:"cedMage",indexString: "requestheader") as! String
//        let baseURL = cedMage.getInfoPlist(fileName:"cedMage",indexString: "cedBaseUrl") as! String;
//        
//        let urlRequest = baseURL+urlToRequest;
//        
//        var postString = "";
//        
//        if defaults.bool(forKey: "isLogin") {
//            postString += "hashkey="+userInfoDict["hashKey"]!+"&customer_id="+userInfoDict["customerId"]!;
//            for (key,val) in dataToPost{
//                postString += "&"+key+"="+val;
//            }
//        }
//        else{
//            var firstTime = true;
//            for (key,val) in dataToPost{
//                if(firstTime){
//                    firstTime = false;
//                    postString += key+"="+val;
//                    continue;
//                }
//                postString += "&"+key+"="+val;
//            }
//        }
//        
//        if(defaults.object(forKey: "filtersToSend") != nil){
//            var filtersToSend = defaults.object(forKey: "filtersToSend") as! String;
//            filtersToSend = filtersToSend.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlHostAllowed)!;
//            defaults.removeObject(forKey: "filtersToSend");
//            postString += "&multi_filter="+filtersToSend;
//        }
//        
//        
//        /*
//         if(self.searchString != ""){
//         postString += "&query="+searchString;
//         }
//         */
//        
//        
//        var request = URLRequest(url: URL(string: "\(urlRequest)")!);
//        request.httpMethod = "POST";
//        request.httpBody = postString.data(using: String.Encoding.utf8);
//        request.setValue(requestHeader, forHTTPHeaderField: "Mobiconnectheader")
//        
//        cedMageLoaders.addDefaultLoader(me: self);
//        let task = URLSession.shared.dataTask(with: request){
//            
//            // check for fundamental networking error
//            data, response, error in
//            guard error == nil && data != nil else{
//                print("error=\(error)")
//                DispatchQueue.main.async{
//                    print(error?.localizedDescription);
//                    cedMageLoaders.removeLoadingIndicator(me: self);
//                    self.makeRequestToAPI(urlToRequest, dataToPost:dataToPost);
//                }
//                return;
//            }
//            
//            // check for http errors
//            if let httpStatus = response as? HTTPURLResponse , httpStatus.statusCode != 200{
//                
//                print("statusCode should be 200, but is \(httpStatus.statusCode)")
//                print("response = \(response)")
//                DispatchQueue.main.async{
//                    cedMageLoaders.removeLoadingIndicator(me: self);
//                    if(self.products.count == 0){
//                        cedMageHttpException.showHttpErrorImage(me: self, img: "no_module")
//                    }
//                    
//                }
//                return;
//            }
//            
//            // code to fetch values from response :: start
//            
//            let datastring = NSString(data: data!, encoding: String.Encoding.utf8.rawValue);
//            
//            if(datastring == "NO_PRODUCTS"){
//                DispatchQueue.main.async{
//                    cedMageLoaders.removeLoadingIndicator(me: self);
//                    if(self.products.count == 0){
//                        cedMage().renderNoDataImage(view:self,imageName:"noProduct");
//                        return;
//                    }
//                    else{
//                        self.loadMoreData = false;
//                    }
//                }
//                return;
//            }
//            
//            let jsonResponse = JSON(data: data!);
//            if(jsonResponse != nil){
//                DispatchQueue.main.async{
//                    //print(jsonResponse);
//                    cedMageLoaders.removeLoadingIndicator(me: self);
//                    self.parseData(json: jsonResponse, index: "category");
//                }
//            }
//        }
//        task.resume();
//    }
//    
//    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
//        
//        let currentOffset = scrollView.contentOffset.y
//        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
//       
//        if(!loadMoreData){
//            return;
//        }
//        if (maximumOffset - currentOffset) <= 40 {
//            
//            currentpage = currentpage+1;
//            if(searchString != ""){
//                self.makeRequestToAPI("mobiconnect/category/productSearch/",dataToPost: ["q":searchString, "page":String(currentpage)]);
//            }
//            else{
//                self.makeRequestToAPI("mobiconnect/category/categoryProducts/",dataToPost: ["id":selectedCategory, "page":String(currentpage)]);
//            }
//            //self.makeRequestToAPI("mobiconnect/category/categoryProducts/",dataToPost: ["id":categoryId, "page":String(currentPageNum)]);
//        }
//        
//    }
//    
//}
