//
//  cedFeaturedProductListing.swift
//  MageNative Magento Platinum
//
//  Created by CEDCOSS Technologies Private Limited on 27/01/17.
//  Copyright © 2017 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit
import AVFoundation
class cedFeaturedProductListing: MagenativeUIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIScrollViewDelegate {
    
    @IBOutlet weak var collectionView: UICollectionView!
    var selectedCategory = String()
    var subCateGories = [[String:String]]()
    var products  = [[String:String]]()
    var sortByArray = [String:String]();
    var currentView = "grid"
    var searchString = ""; // variable to handle search case
    var jsonResponse:JSON?
    var loadMoreData = true
    var currentpage = 1
    var homePage = Bool()
    var eanCode = String()
    var bannerArray = [[String:String]]()
    var product_array = [[String:String]]()
    var category_array = [[String:String]]()
    var banner_image_array = [String]()
    var dealGroup = [[String:String]]()
    var deals     = [[String:String]]()
    var dealsPro     = [String:[[String:String]]]()
    private var cache = NSCache<AnyObject, AnyObject>()
    var flag=false
    var no_pro_check=false
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.dataSource = self
        collectionView.delegate = self
        
        //self.getRequest(url: "mobiconnect/home/featured/page/\(currentpage)/store_id/", store: true)
       self.send_req_pagination(url: "mobiconnect/home/featured/page/\(currentpage)/store/", store: true)
    
        // Do any additional setup after loading the view.
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if no_pro_check==false
        {
            if flag==true{
                flag=false
                currentpage += 1
                self.send_req_pagination(url: "mobiconnect/home/featured/page/\(currentpage)/store/", store: true)
            }
        }
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    
    
    
    
    
    
    
    
    override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
        print(requestUrl as Any)
        
        
        if let data = data{
            if var json = try? JSON(data: data){
                if(requestUrl == "mobiconnect/module/gethomepage/1/"){
                    
                    self.parse_product(json: json, index: "category")
                    self.banner_image_array = [String]()
                    for image in self.bannerArray{
                        self.banner_image_array.append(image["banner_image"]!)
                    }
                }else if(requestUrl == "mobiconnectdeals/getdealgroup/"){
                    
                    self.parse_product(json: json, index: "deal_products")
                }else {
                    
                    
                    print(json)
                    json = json[0]
                    if(json.stringValue.lowercased() == "NO_PRODUCTS".lowercased()){
                        no_pro_check=true
                        if(self.products.count == 0){
                            self.renderNoDataImage(view:self,imageName:"noProduct");
                            return;
                        }
                        else{
                            self.loadMoreData = false;
                        }
                        return
                    }
                    for result in json["featured_products"].arrayValue {
                        let product_id = result["product_id"].stringValue;
                        let regular_price = result["regular_price"].stringValue;
                        let special_price = result["special_price"].stringValue;
                        let product_name = result["product_name"].stringValue;
                        let product_image = result["product_image"].stringValue;
                        let type = result["type"].stringValue;
                        let review = result["review"].stringValue;
                        let show_both_price = result["show-both-price"].stringValue;
                        let Inwishlist = result["Inwishlist"].stringValue
                        let starting_from = result["starting_from"].stringValue;
                        let from_price = result["from_price"].stringValue;
                        let offerText  = result["offer"].stringValue
                        
                        var wishlistItemId = "-1";
                        if(result["wishlist-item-id"] != nil){
                            wishlistItemId = result["wishlist-item-id"].stringValue;
                        }
                        self.products.append(["product_id":product_id,"regular_price":regular_price, "special_price":special_price,"product_name":product_name,"product_image":product_image,"type":type,"review":review,"show-both-price":show_both_price,"starting_from":starting_from,"from_price":from_price,"Inwishlist":Inwishlist,"wishlist-item-id":wishlistItemId,"offerText":offerText]);
                        
                    }
                    
                }
                self.collectionView.reloadData()
            }
            
        }
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.section == 0 {
            let height = CGFloat(170)
            let width = UIWindow().frame.size.width
            return CGSize(width:width, height:height);
            
        }else if indexPath.section < dealGroup.count + 1 {
            
            if indexPath.row == 0 {
                let height = CGFloat(50)
                let width = UIWindow().frame.size.width - 10
                return CGSize(width:width, height:height);
            }
            let height = CGFloat(170)
            let width = UIWindow().frame.size.width - 10
            return CGSize(width:width, height:height);
        }
        if self.currentView == "grid" {
            let height = CGFloat(300)
            let width = UIWindow().frame.size.width/2 - 10
            return CGSize(width:width, height:height);
        }else{
            let height = CGFloat(300)
            let width = UIWindow().frame.size.width - 10
            return CGSize(width:width, height:height);
        }
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return dealGroup.count + 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            if bannerArray.count > 0 {
                return 1
            }
            return 0
        }else if section < dealGroup.count + 1 {
            if dealGroup.count == 0 {
                return 0
            }
            if let count = dealsPro[String(section - 1)]?.count{
                return count + 1
            }
            return 0
        }
        else{
            return products.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.section > dealGroup.count  {
            //let productInfo = products[indexPath.row];
            let productview = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "rootPageViewController") as! cedMageProductViewRoot;
            productview.pageData = products as NSArray;
            let instance = cedMage.singletonInstance;
            instance.storeParameterInteger(parameter: indexPath.row);
            self.navigationController?.pushViewController(productview
                , animated: true);
        }else{
            if indexPath.row != 0 {
                _ = UIStoryboard(name: "Main", bundle: nil)
                
                if let deal = dealsPro[String(indexPath.section - 1)]?[indexPath.row - 1] {
                    
                    if(deal["deal_type"] ==  "1"){
                        let productview = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "rootPageViewController") as! cedMageProductViewRoot
                        productview.pageData = [["product_id":deal["relative_link"]]]
                        let instance = cedMage.singletonInstance
                        instance.storeParameterInteger(parameter: 0)
                        self.navigationController?.pushViewController(productview
                            , animated: true)
                    }else if(deal["deal_type"] ==  "2"){
                        let viewcontoller = UIStoryboard(name: "categorylayouts", bundle: nil).instantiateViewController(withIdentifier: "cedMageDefaultCollection") as? cedMageDefaultCollection
                        viewcontoller?.selectedCategory = deal["relative_link"]!
                        self.navigationController?.pushViewController(viewcontoller!, animated: true)
                    }else if(deal["deal_type"] ==  "3"){
                        let   StoryBoard = UIStoryboard(name: "cedMageAccounts", bundle: nil)
                        let viewControl = StoryBoard.instantiateViewController(withIdentifier: "cmsWebView") as! cedMageCmsWebView
                        viewControl.pageUrl = deal["relative_link"]!
                        self.navigationController?.pushViewController(viewControl, animated: true)
                    }
                }
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.section == 0{
            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "bannerCell", for: indexPath) as? cedMageCollectionBanner {
                cell.dataSource = banner_image_array
                cell.fullData = bannerArray
                cell.bannerImage.imageCounterDisabled = true
                cell.bannerImage.slideshowTimeInterval = 2
                cell.parent = self
                return cell
            }
            
        }  else if indexPath.section == dealGroup.count + 1{
            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionviewdefault", for: indexPath) as? defaultGridCell {
                let product = products[indexPath.row]
//                cell.subCategoryLabel.fontColorTool()
//                cell.productTitle.fontColorTool()
//                cell.rating.fontColorTool()
//                cell.wishlistButton.fontColorTool()
//                cell.offerText.fontColorTool()

                
                var productName = NSMutableAttributedString()
                productName = NSMutableAttributedString(string: product["product_name"]!)
                
                if(product["starting_from"] != ""){
                    productName.append(NSAttributedString(string:"\n"+"Starting At : ".localized+product["starting_from"]!+"\n"));
                    if(product["from_price"] != ""){
                        productName.append(NSAttributedString(string: "As Low As : ".localized+product["from_price"]!));
                    }
                }
                else{
                    
                    if(product["special_price"] != "no_special"){
                        let attr = [NSAttributedString.Key.strikethroughStyle:1]
                        if let regPrice = product["regular_price"]{
                            let attribute = NSAttributedString(string:  "\n"+regPrice, attributes: attr)
                            productName.append(attribute)
                        }
                        let attr1 = [NSAttributedString.Key.foregroundColor:UIColor.red]
                        let attribute1 = NSAttributedString(string:  product["special_price"]!, attributes: attr1)
                        productName.append(NSAttributedString(string:"\n"));
                        productName.append(attribute1)
                        
                    }else{
                        productName.append(NSAttributedString(string: "\n"+product["regular_price"]!));
                    }
                    
                }
                cell.productTitle.attributedText = productName
//                cell.offerText
                if product["offerText"] != "" {
                    cell.offerView.isHidden = false
                    cell.offerText.text = product["offerText"]! + "% OFF".localized
                }else{
                    cell.offerView.isHidden = true
                }
                if(product["Inwishlist"] != "OUT"){
                    cell.wishlistButton.setImage(UIImage(named:"LikeFilled"), for: UIControl.State.normal);
                    
                }else{
                    cell.wishlistButton.isHidden = true
                }
                cell.productImage.image = UIImage(named: "placeholder")
                if  let urlToRequest = product["product_image"]{
                    if(urlToRequest != "false"){
                        
                        SDWebImageManager.shared.loadImage(with: NSURL(string: urlToRequest) as URL?, options: .continueInBackground, progress: {
                            (receivedSize :Int, ExpectedSize :Int, newSize: URL?) in
                            
                        }, completed: {
                            (image : UIImage?, data: Data?, error : Error?, cacheType : SDImageCacheType, finished : Bool, url : URL?) in
                            let size=cell.productImage.bounds.size.width
                            if let image = image {
                                let image_up=self.resizeImage(image: image, newWidth: size)
                                cell.productImage.image=image_up
                            }
                        })
                        //cell.productImage.sd_setImage(with: URL(string: urlToRequest), placeholderImage: UIImage(named: "placeholder"))
                        
                    }else{
                        if let image = UIImage(named: "placeholder") {
                            cell.productImage.image = image
                        }
                    }
                }else{
                    if let image = UIImage(named: "placeholder") {
                    cell.productImage.image = image
                    }
                }
                cell.cardView()
                if (indexPath.row) == (products.count/2)
                {
                    flag=true
                }
                
                //                MARK : SetFont COLOR
                cell.productTitle.fontColorTool()
                
                
                return cell
            }
        }else {
            if indexPath.row ==  0{
                if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "dealTitleCell", for: indexPath) as? cedMageTestDealCell {
                    cell.dealTitle.cardView()
                    cell.dealTitle.text = dealGroup[indexPath.section - 1]["groupTitle"]?.uppercased()
                    return cell
                }
            }else{
                if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "dealCell", for: indexPath) as? cedMageCollectionCell {
                    let deal =  dealsPro[String(indexPath.section - 1)]?[indexPath.row - 1]
                    cell.label.fontColorTool()
                    cell.label.text = deal?["offer_text"]
                    if let img_url = deal?["deal_image_name"] {
                        
                        if(img_url != "false"){
                           
                            cell.imageViewProduct.sd_setImage(with: URL(string: img_url), placeholderImage: UIImage(named: "bannerPlaceHolder.gif"))
                        }
                    }
                    return cell
                }
            }
        }
        return UICollectionViewCell()
        
    }
    
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        
        //let image = UIImage(contentsOfFile: img_url)
        
        let size = (image.size).applying(CGAffineTransform(scaleX: 0.5, y: 0.5))
        let hasAlpha = false
        let scale: CGFloat = 0.0 // Automatically use scale factor of main screen
        
        UIGraphicsBeginImageContextWithOptions(size, !hasAlpha, scale)
        image.draw(in: CGRect(origin: CGPoint.zero, size: size))
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        
        return scaledImage!
        
    }
    
    
    
    
    func parse_product(json:JSON,index:String){
        // print(index)
        let json = json[0]
        print(json)
        if(index == "category"){
            if(json["data"]["banner"].arrayValue.count > 0){
                for result in json["data"]["banner"].arrayValue {
                    let product_id = result["product_id"].stringValue
                    let id = result["id"].stringValue
                    let title = result["title"].stringValue
                    let link_to = result["link_to"].stringValue
                    let banner_image = result["banner_image"].stringValue
                    let products_obj = ["product_id":product_id, "title":title,"link_to":link_to,"banner_image":banner_image,"id":id]
                    bannerArray.append(products_obj)
                }
            }
            
            
            
        }
        if(index == "deal_products"){
            var j = 0
            for result in json["data"]["deal_products"].arrayValue {
                // print(result)
                let dealgropstart = result["group_start_date"].stringValue
                let groupcreated = result["created_time"].stringValue
                let group_end_date = result["group_end_date"].stringValue
                let dealGroupDuration = result["deal_duration"].stringValue
                let groupImagename = result["group_image_name"].stringValue
                let group_status = result ["group_status"].stringValue
                let groupTitle  = result["title"].stringValue
                let group_id  = result["group_id"].stringValue
                let timer_status = result["timer_status"].stringValue
                let view_all_status = result["view_all_status"].stringValue
                let update_time = result["update_time"].stringValue
                let is_static  = result["is_static"].stringValue
                let deal_link  = result["deal_link"].stringValue
                let dealObject = ["dealgropstart":dealgropstart,"groupcreated":groupcreated,"group_end_date":group_end_date,"dealGroupDuration":dealGroupDuration,"groupImagename":groupImagename,"group_status":group_status,"groupTitle":groupTitle,"group_id":group_id,"timer_status":timer_status,"view_all_status":view_all_status,"update_time":update_time,"is_static":is_static,"deal_link":deal_link]
                dealGroup.append(dealObject)
                var i = 0
                for deal in result["content"].arrayValue{
                    let id = deal["id"].stringValue
                    let start_date = deal["start_date"].stringValue
                    let deal_image_name = deal["deal_image_name"].stringValue
                    let relative_link = deal["relative_link"].stringValue
                    let deal_type = deal["deal_type"].stringValue
                    let deal_title = deal["deal_title"].stringValue
                    let category_link = deal["category_link"].stringValue
                    let offer_text = deal["offer_text"].stringValue
                    let status  = deal["status"].stringValue
                    let product_link = deal["product_link"].stringValue
                    let deals_obj = ["id":id, "start_date":start_date,"deal_image_name":deal_image_name,"relative_link":relative_link,"deal_type":deal_type,"deal_title":deal_title,"category_link":category_link,"offer_text":offer_text,"status":status,"product_link":product_link]
                    deals.append(deals_obj)
                    i += 1
                }
                
                dealsPro["\(j)"] = deals;
                deals.removeAll()
                j += 1
            }
        }
        
        return
    }
    
    

    
}
