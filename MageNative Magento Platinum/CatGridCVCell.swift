//
//  CatGridCVCell.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 16/06/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class CatGridCVCell: UICollectionViewCell {

    @IBOutlet weak var catImage: UIImageView!
    @IBOutlet weak var catName: UILabel!
var gridCheck = 0

    var category: HomepageSubcategoryData?{
        didSet{
            
            catImage.sd_setImage(with: URL(string: (category?.image)!), placeholderImage: UIImage(named: "placeholder"))
            if gridCheck == 0{
            catName.text = ""
            }else{
                catImage.layer.cornerRadius    = catImage.frame.width * 0.5
                catName.text = category?.category_name
            }
        }
    }
}
