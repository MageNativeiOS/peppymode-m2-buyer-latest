//
//  SearchTVCell.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 16/06/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class SearchTVCell: UITableViewCell,UISearchBarDelegate {
    var parent: UIViewController!
    @IBOutlet weak var searchBar: UISearchBar!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        searchBar.delegate = self
        searchBar.backgroundImage = UIImage()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        let searchPage = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "searchView") as! cedMageSearchPage
            parent.navigationController?.pushViewController(searchPage, animated: true)
        searchBar.resignFirstResponder()
        }
    }

