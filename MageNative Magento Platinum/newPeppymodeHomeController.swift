//
//  newPeppymodeHomeController.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 30/09/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class newPeppymodeHomeController: MagenativeUIViewController {
    
    //MARK: - Properties
    
    var sections = [HomeDataModel]()
    
    //MARK: - Views
    
    lazy var homeTable:UITableView = {
        let table = UITableView()
        table.separatorStyle = .none
        table.backgroundColor = .mageSecondarySystemBackground
        table.register(HomeCategorySliderTC.self, forCellReuseIdentifier: HomeCategorySliderTC.reuseID)
        table.register(HomeCircularCategorySliderTC.self, forCellReuseIdentifier: HomeCircularCategorySliderTC.reuseID)
        table.register(HomeWidgetBannerTC.self, forCellReuseIdentifier: HomeWidgetBannerTC.reuseID)
        table.register(homeFeaturedBlogCell.self, forCellReuseIdentifier: homeFeaturedBlogCell.reuseID)
        table.register(homeFeaturedBrandTC.self, forCellReuseIdentifier: homeFeaturedBrandTC.reuseID)
        table.register(HomeAnimationBannerTC.self, forCellReuseIdentifier: HomeAnimationBannerTC.reuseID)
        table.register(homeBrandSliderTC.self, forCellReuseIdentifier: homeBrandSliderTC.reuseID)
        table.register(homeWidgetBrandBannerTC.self, forCellReuseIdentifier: homeWidgetBrandBannerTC.reuseID)
       // table.register(HomeDealSliderTC.self, forCellReuseIdentifier: HomeDealSliderTC.reuseID)
        table.delegate =  self
        table.dataSource =  self
        
        return table
    }()
    
    //MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        getHomepageData()
        configureUI()
    }
    
    //MARK: - Api calls
    
    fileprivate func getHomepageData() {
        HomeService.shared.getHomepageData(controller: self, page: "1") { homeData in
            print(homeData?.count ?? 0)
            self.sections = homeData ?? []
            
            DispatchQueue.main.async {
                self.homeTable.reloadData()
            }
        }
    }
    
    //MARK: - Selectors
    
    //MARK: - Helper Functions
    
    fileprivate func configureUI() {
        view.addSubview(homeTable)
        homeTable.addConstraintsToFillView(view)
    }
    
    func getRowsForVerticalBanner(banners: [HomeBanner]) -> Int {
        if banners.count % 2 != 0 { return banners.count / 2 + 1 }
        return banners.count / 2
    }
    
  
    
}

//MARK: - UITableViewDataSource

extension newPeppymodeHomeController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let data = sections[section]
        switch sections[section].link_type {
        case "category_slider":
            if let catCount = data.categories?.count
            {
                if catCount > 0
                {return 1 }
                else { return 0 }
            }
            else { return 0 }
            
        case "banner_slider": return 1
        case "featured_brands": return 1
        case "featured_blogs": return 1
        case "animation_banner": return 1
        case "brand_slider": return 1
       // case "product_deal_slider": return 1
        default: return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = sections[indexPath.section]
        switch sections[indexPath.section].link_type {
        case "category_slider":
            switch sections[indexPath.section].category_shape {
            case "circular":
                switch sections[indexPath.section].category_type{
                case "slider":
                    let cell = tableView.dequeueReusableCell(withIdentifier: HomeCircularCategorySliderTC.reuseID, for: indexPath) as! HomeCircularCategorySliderTC
                    cell.categoryHeading.text = data.title
                    cell.categories = data.categories ?? []
                    cell.changeLayout(.horizontal)
                    cell.parent = self
                    cell.grid = false
                    return cell
                default:
                    let cell = tableView.dequeueReusableCell(withIdentifier: HomeCircularCategorySliderTC.reuseID, for: indexPath) as! HomeCircularCategorySliderTC
                    cell.categoryHeading.text = data.title
                    cell.categories = data.categories ?? []
                    cell.parent = self
                    cell.changeLayout(.vertical)
                    cell.grid = true
                    return cell
                }
               case "square":
                              switch sections[indexPath.section].category_type{
                              case "slider":
                                  let cell = tableView.dequeueReusableCell(withIdentifier: HomeCategorySliderTC.reuseID, for: indexPath) as! HomeCategorySliderTC
                                  cell.categoryHeading.text = data.title!
                                  cell.categories = data.categories ?? []
                                  cell.changeLayout(.horizontal)
                                  cell.grid = false
                                  cell.parent = self
                                  return cell
                              default:
                                let cell = tableView.dequeueReusableCell(withIdentifier: HomeCategorySliderTC.reuseID, for: indexPath) as! HomeCategorySliderTC
                                  cell.categoryHeading.text = data.title!
                                  cell.categories = data.categories ?? []
                                  cell.changeLayout(.vertical)
                                  cell.grid = true
                                  cell.parent = self
                                  return cell
                              }
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier: HomeCategorySliderTC.reuseID, for: indexPath) as! HomeCategorySliderTC
                cell.categoryHeading.text = data.title!
                cell.categories = data.categories ?? []
                cell.changeLayout(.horizontal)
                cell.grid = true
                cell.parent = self
                return cell
            }
        case "banner_slider":
            if data.title == "Feature Brands Banner Widget"{
            let cell = tableView.dequeueReusableCell(withIdentifier: homeWidgetBrandBannerTC.reuseID, for: indexPath) as! homeWidgetBrandBannerTC
            cell.fromBrandSlider = false
            cell.bannerType = data.banner_orientiation ?? ""
            cell.banners = data.widget ?? []
            cell.parent = self
            return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: HomeWidgetBannerTC.reuseID, for: indexPath) as! HomeWidgetBannerTC
                cell.fromBrandSlider = false
                cell.bannerType = data.banner_orientiation ?? ""
                cell.banners = data.widget ?? []
                cell.parent = self
                return cell
            }
        
        case "animation_banner":
            let cell = tableView.dequeueReusableCell(withIdentifier: HomeAnimationBannerTC.reuseID, for: indexPath) as! HomeAnimationBannerTC
            cell.banner = data.banner
            cell.parent = self
            return cell
        case "featured_brands":
            let cell = tableView.dequeueReusableCell(withIdentifier: homeFeaturedBrandTC.reuseID, for: indexPath) as! homeFeaturedBrandTC
            cell.brandHeading.text = data.title
            cell.parent = self
            cell.brands = data.brands ?? []
            return cell
        case "featured_blogs":
            let cell = tableView.dequeueReusableCell(withIdentifier: homeFeaturedBlogCell.reuseID, for: indexPath) as! homeFeaturedBlogCell
            cell.parent = self
            cell.blogHeading.text = data.title
            cell.blogData = data.blogs ?? []
            return cell
        case "brand_slider":
         //   if data.title == "Feature Brands Banner Widget"{
                       let cell = tableView.dequeueReusableCell(withIdentifier: homeWidgetBrandBannerTC.reuseID, for: indexPath) as! homeWidgetBrandBannerTC
                       cell.fromBrandSlider = false
                       cell.bannerType = data.banner_orientiation ?? ""
                       cell.banners = data.brand_slider ?? []
                       cell.parent = self
                       return cell
//            }else{
//            let cell = tableView.dequeueReusableCell(withIdentifier: homeBrandSliderTC.reuseID, for: indexPath) as! homeBrandSliderTC
//
//            cell.banners = data.brand_slider ?? []
//            cell.parent = self
//            return cell
//            }
        default: return .init()
        }
    }
}

//MARK: - UITableViewDelegate

extension newPeppymodeHomeController: UITableViewDelegate {
    func returnHeightForCirGridCat(_ categ:[HomeCategory]) -> Int {
         if categ.count % 2 != 0{
           return categ.count / 3 + 1
         }
         return categ.count / 3
       }
    func returnHeightForGridCat(_ categ:[HomeCategory]) -> Int {
            if categ.count % 2 != 0{
              return categ.count / 2 + 1
            }
            return categ.count / 2
          }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch sections[indexPath.section].link_type {
        case "category_slider":
            switch sections[indexPath.section].category_shape {
            case "circular":
                switch sections[indexPath.section].category_type{
                case "slider":
                    return 175
                default:
                    return CGFloat(returnHeightForCirGridCat(sections[indexPath.section].categories ?? []) * 118)//160
                }
            case "square":
                switch sections[indexPath.section].category_type{
                case "slider":
                    return 220
                default:
                    return CGFloat(returnHeightForGridCat(sections[indexPath.section].categories ?? []) * 325)//180)//225)
                }
            default: return 260
            }
        case "banner_slider":
            switch sections[indexPath.section].banner_orientiation {
            case "vertical":
                let rows = getRowsForVerticalBanner(banners: sections[indexPath.section].widget ?? [])
                print("DEBUG Vertical banner height is \((UIScreen.main.bounds.width/2) * CGFloat(rows))")
                return UIScreen.main.bounds.width * CGFloat(rows)
                
            default:
                 let data = sections[indexPath.section]
                if data.title == "Feature Brands Banner Widget"{
                    return UIScreen.main.bounds.height/3.2//return UIScreen.main.bounds.width * (3/7)
                }else{
                    return UIScreen.main.bounds.width //UIScreen.main.bounds.height/2.8
                }
            }
        case "featured_brands":
            if(UIDevice().model.lowercased() == "ipad".lowercased()){
                return 200
            }else{
            return 170
            }//CGFloat(returnHeightForGridCat(sections[indexPath.section].brands ?? []) * 180)//250)
            //--edited return 500
        case "featured_blogs": return 300
        case "animation_banner":
            switch sections[indexPath.section].type_of_banner {
            case "small_size": return UIScreen.main.bounds.height/3 //return UIScreen.main.bounds.width - (UIScreen.main.bounds.width/2)
            case "large_size": return UIScreen.main.bounds.height/2.2  //return UIScreen.main.bounds.width - (UIScreen.main.bounds.width/4)
            default: return UIScreen.main.bounds.height/2.2 //UIScreen.main.bounds.width - (UIScreen.main.bounds.width/3)
            }
        case "product_deal_slider": return 300
        case "brand_slider":
            let data = sections[indexPath.section]
           // if data.title == "Feature Brands Banner Widget"{
                return UIScreen.main.bounds.height/3.2//return UIScreen.main.bounds.width * (3/7)
//            }else{
//             return UIScreen.main.bounds.height/3
//            }
        default: return 0
        }
        return 0
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("didSelect")
        print("animation_banner = \(sections[indexPath.section])")
        if sections[indexPath.section].link_type == "animation_banner" {
            switch sections[indexPath.section].banner?.link_to {
            case "category":
                let vc = UIStoryboard(name: "categorylayouts", bundle: nil).instantiateViewController(withIdentifier: "cedMageDefaultCollection") as!  cedMageDefaultCollection
                vc.selectedCategory = sections[indexPath.section].banner?.product_id ?? ""
                self.navigationController?.pushViewController(vc, animated: true)
            case "product":
                let vc = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "productSinglePageViewController") as! ProductSinglePageViewController
                vc.product_id = sections[indexPath.section].banner?.product_id ?? ""
                
                self.navigationController?.pushViewController(vc, animated: true)
            default:
                let view = UIStoryboard(name: "cedMageAccounts", bundle: nil)
                let vc = view.instantiateViewController(withIdentifier: "cmsWebView") as! cedMageCmsWebView
                let url = sections[indexPath.section].banner?.product_id ?? ""
                vc.pageUrl = url
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    
    
}
