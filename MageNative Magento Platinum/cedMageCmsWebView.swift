/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */


import UIKit
import SafariServices
import WebKit

class cedMageCmsWebView: cedMageViewController,WKScriptMessageHandler,SFSafariViewControllerDelegate, WKNavigationDelegate {
   
    
    
    
    var pageUrl=""
       var webView = WKWebView()
       required init?(coder aDecoder: NSCoder) {
           self.webView = WKWebView(frame: CGRect.zero)
           super.init(coder: aDecoder)
           self.webView.navigationDelegate = self
       }
       
       override func viewDidLoad() {
           super.viewDidLoad()
          // self.tracking(name: "WebView")
           let temp=URL(string: pageUrl)
           let myRequest = URLRequest(url: temp!)
           let webconfgi = WKWebViewConfiguration()
           webconfgi.userContentController.add(self,                                                                    name: "redir")
           
           let bounds = self.view.bounds
           let toplayoutguide = self.navigationController!.view.frame.origin.y ;
           let frame  = CGRect(x: 0, y: toplayoutguide, width: bounds.width, height: bounds.height)
           webView = WKWebView(frame: frame, configuration: webconfgi)
           webView.load(myRequest)
           self.view.addSubview(webView)
           webView.navigationDelegate = self
        cedMageLoaders.addDefaultLoader(me: self)
           loadPage()
           /*webView.delegate=self
           webView.loadRequest(myRequest)*/
           // Do any additional setup after loading the view.
       }
   
    func loadPage(){
        let request = URLRequest(url: URL(string: pageUrl)!)
        webView.load(request)
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        cedMageLoaders.removeLoadingIndicator(me: self)
           webView.evaluateJavaScript("document.getElementsByClassName ('placeholder-header')[0].style.display='none';", completionHandler: nil)
           webView.evaluateJavaScript("document.getElementsByClassName ('header header3')[0].style.display='none';", completionHandler: nil)
           
           webView.evaluateJavaScript("document.getElementsByClassName ('page-title-wrapper')[0].style.display='none';", completionHandler: nil)
           
           webView.evaluateJavaScript("document.getElementsByClassName ('row')[0].style.display='none';", completionHandler: nil)
           webView.evaluateJavaScript("document.getElementsByClassName ('breadcrumbs')[0].style.display='none';", completionHandler: nil)
           webView.evaluateJavaScript("document.getElementsByClassName ('ooter footer1')[0].style.display='none';", completionHandler: nil)
           webView.evaluateJavaScript("document.getElementsByClassName ('col-lg-2 col-md-2 col-sm-6 col-xs-12')[0].style.display='none';", completionHandler: nil)
           webView.evaluateJavaScript("document.getElementsByClassName ('col-lg-2 col-md-2 col-sm-6 col-xs-12')[1].style.display='none';", completionHandler: nil)
           webView.evaluateJavaScript("document.getElementsByClassName ('toolbar-sorter sorter')[0].style.display='none';", completionHandler: nil)
           //col-lg-2 col-md-2 col-sm-6 col-xs-12
           webView.evaluateJavaScript("document.getElementsByClassName ('col-lg-3 col-md-3 col-sm-6 col-xs-12')[0].style.display='none';", completionHandler: nil)
         webView.evaluateJavaScript("document.getElementsByClassName ('col-lg-2 col-md-2 col-sm-6 col-xs-12')[0].style.display='none';", completionHandler: nil)
       }
       
       func userContentController(_ userContentController:
           WKUserContentController,
                                  didReceive message: WKScriptMessage) {
           print(message.body)
       
       }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
