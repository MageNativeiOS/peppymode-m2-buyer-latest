/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */


import UIKit

extension ProductSinglePageViewController{

    func renderBundleProductOptions(){
        
        let bundleProductContainerStackView = UIStackView(); // outermost stackview
        bundleProductContainerStackView.translatesAutoresizingMaskIntoConstraints = false;
        bundleProductContainerStackView.axis  = NSLayoutConstraint.Axis.vertical;
        bundleProductContainerStackView.distribution  = UIStackView.Distribution.equalSpacing;
        bundleProductContainerStackView.alignment = UIStackView.Alignment.center;
        bundleProductContainerStackView.spacing   = 0.0;
        stackView.addArrangedSubview(bundleProductContainerStackView);
        stackView.addConstraint(NSLayoutConstraint(item: bundleProductContainerStackView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: stackView, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: padding/2));
        stackView.addConstraint(NSLayoutConstraint(item: bundleProductContainerStackView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: stackView, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: -padding/2));
        
        
        let backgroundViewForStackView = UIView(); // uiview for covering stackview
        backgroundViewForStackView.translatesAutoresizingMaskIntoConstraints = false;
        if #available(iOS 13.0, *) {
            backgroundViewForStackView.backgroundColor = UIColor.systemBackground
        } else {
            backgroundViewForStackView.backgroundColor = UIColor.white
        };
       // backgroundViewForStackView.makeCard(backgroundViewForStackView, cornerRadius: 2, color: UIColor.black, shadowOpacity: 0.4);
        bundleProductContainerStackView.addArrangedSubview(backgroundViewForStackView); // adding uiview to stackview
        bundleProductContainerStackView.addConstraint(NSLayoutConstraint(item: backgroundViewForStackView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: bundleProductContainerStackView, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0));
        bundleProductContainerStackView.addConstraint(NSLayoutConstraint(item: backgroundViewForStackView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: bundleProductContainerStackView, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0));
        
        bundleProductOptionsStackView = UIStackView(); // main actual stackview that will contain all bundle-options
        bundleProductOptionsStackView.translatesAutoresizingMaskIntoConstraints = false;
        bundleProductOptionsStackView.axis  = NSLayoutConstraint.Axis.vertical;
        bundleProductOptionsStackView.distribution  = UIStackView.Distribution.equalSpacing;
        bundleProductOptionsStackView.alignment = UIStackView.Alignment.center;
        bundleProductOptionsStackView.spacing   = 10.0;
        backgroundViewForStackView.addSubview(bundleProductOptionsStackView); // adding stackview to uiview
        backgroundViewForStackView.addConstraint(NSLayoutConstraint(item: bundleProductOptionsStackView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: backgroundViewForStackView, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: padding/2));
        backgroundViewForStackView.addConstraint(NSLayoutConstraint(item: bundleProductOptionsStackView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: backgroundViewForStackView, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: -padding/2));
        backgroundViewForStackView.addConstraint(NSLayoutConstraint(item: bundleProductOptionsStackView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: backgroundViewForStackView, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1, constant: padding/2));
        backgroundViewForStackView.addConstraint(NSLayoutConstraint(item: bundleProductOptionsStackView, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: backgroundViewForStackView, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: -padding/2));
        
        
        for counter in 0  ..< bundleddata_array.count {
            
            let showType = self.bundleddata_extraInfo_array[self.topLabelArray[counter]]!["type"]!;
            
            if(showType == "select" || showType == "radio"){
                
                // Bundle Product Dropdown View
                let bundleProductDropdownView = BundleProductDropdownView();
                bundleProductDropdownView.translatesAutoresizingMaskIntoConstraints = false;
                
                bundleProductDropdownView.topLabel.text = topLabelArray[counter];
                bundleProductDropdownView.bundleProDropdownButton.addTarget(self, action: #selector(ProductSinglePageViewController.showBundleProDropdown(_:)), for: UIControl.Event.touchUpInside);
                bundleProductDropdownView.bundleProDropdownButton.fontColorTool()
                bundleProductDropdownView.bundleProDropdownButton.setTitle("-- Select --".localized, for: UIControl.State.normal);
                bundleProductDropdownView.qtyLabel.text = "Quantity".localized.uppercased();
                
                bundleProductOptionsStackView.addArrangedSubview(bundleProductDropdownView);
                let bundleProductDropdownViewHeight = translateAccordingToDevice(CGFloat(120.0));
                bundleProductDropdownView.heightAnchor.constraint(equalToConstant: bundleProductDropdownViewHeight).isActive = true;
                self.setLeadingAndTralingSpaceFormParentView(bundleProductDropdownView,parentView:bundleProductOptionsStackView, padding:padding/4);
            }
            else{
                let bundleCheckboxViewContainerStackView = UIStackView(); // outermost stackview
                bundleCheckboxViewContainerStackView.translatesAutoresizingMaskIntoConstraints = false;
                bundleCheckboxViewContainerStackView.axis  = NSLayoutConstraint.Axis.vertical;
                bundleCheckboxViewContainerStackView.distribution  = UIStackView.Distribution.equalSpacing;
                bundleCheckboxViewContainerStackView.alignment = UIStackView.Alignment.center;
                bundleCheckboxViewContainerStackView.spacing   = 0.0;
                bundleProductOptionsStackView.addArrangedSubview(bundleCheckboxViewContainerStackView);
                bundleProductOptionsStackView.addConstraint(NSLayoutConstraint(item: bundleCheckboxViewContainerStackView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: bundleProductOptionsStackView, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: padding/4));
                bundleProductOptionsStackView.addConstraint(NSLayoutConstraint(item: bundleCheckboxViewContainerStackView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: bundleProductOptionsStackView, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: -padding/4));
                
                
                let backgroundViewForStackView = UIView(); // uiview for covering stackview
                backgroundViewForStackView.translatesAutoresizingMaskIntoConstraints = false;
                backgroundViewForStackView.backgroundColor = UIColor.white;
           //     backgroundViewForStackView.makeCard(backgroundViewForStackView, cornerRadius: 2, color: UIColor.black, shadowOpacity: 0.4);
                bundleCheckboxViewContainerStackView.addArrangedSubview(backgroundViewForStackView); // adding uiview to stackview
                bundleCheckboxViewContainerStackView.addConstraint(NSLayoutConstraint(item: backgroundViewForStackView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: bundleCheckboxViewContainerStackView, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0));
                bundleCheckboxViewContainerStackView.addConstraint(NSLayoutConstraint(item: backgroundViewForStackView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: bundleCheckboxViewContainerStackView, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0));
                
                let bundleProductCheckboxOptionsStackView = UIStackView(); // main actual stackview that will contain all bundle-checkbox-options
                bundleProductCheckboxOptionsStackView.translatesAutoresizingMaskIntoConstraints = false;
                bundleProductCheckboxOptionsStackView.axis  = NSLayoutConstraint.Axis.vertical;
                bundleProductCheckboxOptionsStackView.distribution  = UIStackView.Distribution.equalSpacing;
                bundleProductCheckboxOptionsStackView.alignment = UIStackView.Alignment.center;
                bundleProductCheckboxOptionsStackView.spacing   = 10.0;
                backgroundViewForStackView.addSubview(bundleProductCheckboxOptionsStackView); // adding stackview to uiview
                backgroundViewForStackView.addConstraint(NSLayoutConstraint(item: bundleProductCheckboxOptionsStackView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: backgroundViewForStackView, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: padding/4));
                backgroundViewForStackView.addConstraint(NSLayoutConstraint(item: bundleProductCheckboxOptionsStackView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: backgroundViewForStackView, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: -padding/4));
                backgroundViewForStackView.addConstraint(NSLayoutConstraint(item: bundleProductCheckboxOptionsStackView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: backgroundViewForStackView, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1, constant: padding/2));
                backgroundViewForStackView.addConstraint(NSLayoutConstraint(item: bundleProductCheckboxOptionsStackView, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: backgroundViewForStackView, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: -padding/2));
                let titleArray = Array(bundleddata_array[topLabelArray[counter]]!.keys);
                //var innerCounter = 0;
                
                print(self.bundleddata_extraInfo_array[self.topLabelArray[counter]] as Any);
                print(bundleddata_array[topLabelArray[counter]] as Any);
                
                let label = UILabel();
                label.fontColorTool()
                label.translatesAutoresizingMaskIntoConstraints = false;
                label.text = topLabelArray[counter];
                label.font = UIFont(fontName: "HelveticaNeue-Medium", fontSize: CGFloat(15));
                bundleProductCheckboxOptionsStackView.addArrangedSubview(label);
                let labelHeight = translateAccordingToDevice(CGFloat(30.0));
                label.heightAnchor.constraint(equalToConstant: labelHeight).isActive = true;
                self.setLeadingAndTralingSpaceFormParentView(label,parentView:bundleProductCheckboxOptionsStackView,padding:padding/4);
                
                
                for title in titleArray{
                    // Bundle Product Dropdown View
                    let bundleProductCheckboxView = BundleProductCheckboxView();
                    bundleProductCheckboxView.translatesAutoresizingMaskIntoConstraints = false;
                    
                    bundleProductCheckboxView.bundleProCheckboxButton.addTarget(self, action: #selector(ProductSinglePageViewController.showBundleProCheckboxPressed(_:)), for: UIControl.Event.touchUpInside);
                    bundleProductCheckboxView.bundleProCheckboxButton.setTitle(title, for: UIControl.State());
                    bundleProductCheckboxView.bundleProCheckboxButton.fontColorTool()
                    bundleProductCheckboxView.qtyLabel.text = "Quantity".localized.uppercased();
                    
                    let user_can_change_qty = bundleddata_array[topLabelArray[counter]]![title]!["user_can_change_qty"]!;
                    let default_qty = bundleddata_array[topLabelArray[counter]]![title]!["default_qty"]!;
                    if(user_can_change_qty == "0"){
                        bundleProductCheckboxView.qtyToBuy.isUserInteractionEnabled = false;
                        bundleProductCheckboxView.qtyToBuy.backgroundColor = UIColor.lightGray;
                    }
                    bundleProductCheckboxView.qtyToBuy.text = default_qty;
                    
                    bundleProductCheckboxOptionsStackView.addArrangedSubview(bundleProductCheckboxView);
                    let bundleProductCheckboxViewHeight = translateAccordingToDevice(CGFloat(90.0));
                    bundleProductCheckboxView.heightAnchor.constraint(equalToConstant: bundleProductCheckboxViewHeight).isActive = true;
                    self.setLeadingAndTralingSpaceFormParentView(bundleProductCheckboxView,parentView:bundleProductCheckboxOptionsStackView,padding:padding/4);
                }
            }
            
        }
        
    }
    
    @objc func showBundleProCheckboxPressed(_ sender:UIButton){
        
        let topLabel = (sender.superview?.superview?.superview as! UIStackView).subviews[0] as! UILabel;
        let key = topLabel.text!;
        let buttonText = (sender.titleLabel?.text!)!;
        let selection_price = self.bundleddata_array[key]![buttonText]!["selection_price"]!;
        
        if let bundleProductCheckboxView = sender.superview?.superview as? BundleProductCheckboxView {
            
            if(bundleProductCheckboxView.bundleProCheckboxButtonImg!.image == UIImage(named:"UncheckedCheckbox")){
                bundleProductCheckboxView.bundleProCheckboxButtonImg.image = UIImage(named: "CheckedCheckbox");
                self.priceCalculationArray[buttonText] = Float(selection_price);
            }
            else{
                bundleProductCheckboxView.bundleProCheckboxButtonImg.image = UIImage(named: "UncheckedCheckbox");
                self.priceCalculationArray[buttonText] = Float(0.0);
            }
        }
        
        self.updateProductPrice();
        
    }
    
    @objc func showBundleProDropdown(_ sender:UIButton){
        
        if let bundleProductDropdownView = sender.superview?.superview as? BundleProductDropdownView {
            
           
            var dataSource = [String]();
            
            let key = bundleProductDropdownView.topLabel.text!;
            dataSource = ["-- Select --".localized]+Array(bundleddata_array[key]!.keys);
            
            dropDown.dataSource = dataSource;
            dropDown.selectionAction = {(index, item) in
                sender.setTitle(item, for: UIControl.State());
                sender.fontColorTool()
                if(self.bundleddata_array[key]![item] == nil){
                    let msg = "Please Select Option".localized;
                    self.view.makeToast(msg, duration: 2.0, position: .bottom, title: nil, image: nil, style: nil, completion: nil);
                    return;
                }
                
                let selection_price = self.bundleddata_array[key]![item]!["selection_price"]!;
                let user_can_change_qty = self.bundleddata_array[key]![item]!["user_can_change_qty"]!;
                let default_qty = self.bundleddata_array[key]![item]!["default_qty"]!;
                
                if(user_can_change_qty == "0"){
                    bundleProductDropdownView.qtyToBuy.isUserInteractionEnabled = false;
                    bundleProductDropdownView.qtyToBuy.backgroundColor = UIColor.lightGray;
                }
                else{
                    bundleProductDropdownView.qtyToBuy.isUserInteractionEnabled = true;
                    bundleProductDropdownView.qtyToBuy.backgroundColor = UIColor.white;
                }
                bundleProductDropdownView.qtyToBuy.text = default_qty;
                
                self.priceCalculationArray[key] = Float(selection_price);
                
                self.updateProductPrice();
                
            }
            dropDown.anchorView = sender
            dropDown.bottomOffset = CGPoint(x: 0, y:sender.bounds.height)
            
            if dropDown.isHidden {
                let _ = dropDown.show();
            } else {
                dropDown.hide();
            }
            
        }
        
    }

}
