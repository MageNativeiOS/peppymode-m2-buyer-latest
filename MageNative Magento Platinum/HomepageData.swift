//
//  HomepageData.swift
//  MageNative Magento Platinum
//
//  Created by Hamza Usmani on 14/06/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import Foundation

struct HomepageData: Decodable {
    let category1:HomepageCategoryData?
    let category4:HomepageCategoryData?
    let category5:HomepageCategoryData?
    let category3:HomepageCategoryData?
    let category2:HomepageCategoryData?
    let banner1:HomepageBannerData?
    let banner4:HomepageBannerData?
    let banner2:HomepageBannerData?
    let slider_banners:[HomepageBannerData]?
    let banner3:HomepageBannerData?
    let banner5:HomepageBannerData?
    let slider_categories:[HomepageSliderCategoryData]?
}
struct HomepageCategoryData: Decodable {
    let name: String?
    let id: String?
    let sub_cateory:[HomepageSubcategoryData]?
}
struct HomepageBannerData : Decodable {
    let product_id:String?
    let title:String?
    let banner_image:String?
    let link_to:String?
    let id:String?
}
struct HomepageSliderCategoryData : Decodable {
    let id:String?
    let image:String?
    let name:String?
    let sub_cateory: [HomepageSubcategoryData]?
}
struct HomepageSubcategoryData : Decodable {
    let category_id:String?
    let category_name:String?
    let image:String?
}
