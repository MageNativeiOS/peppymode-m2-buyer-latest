//
//  checkoutMethodsView.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 28/09/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import Foundation

class checkoutMethodsView:UIView{
    var view : UIView!
    
    @IBOutlet weak var shippingMethodLbl: UILabel!
    @IBOutlet weak var shippingStack: UIStackView!
    @IBOutlet weak var shippingMethodHeight: NSLayoutConstraint!
    @IBOutlet weak var shippingLneView: UIView!
    
    
    @IBOutlet weak var paymentMethodLbl: UILabel!
    @IBOutlet weak var paymentMethodHeight: NSLayoutConstraint!
    @IBOutlet weak var paymentStack: UIStackView!
    
    @IBOutlet weak var paymentLineView: UIView!
    @IBOutlet weak var walletView: UIStackView!
    @IBOutlet weak var walletHeight: NSLayoutConstraint!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        xibSetup()
    }
    
    func xibSetup()
    {
        view = loadViewFromNib()
        
        // use bounds not frame or it'll be offset
        view.frame = bounds
        
        // Make the view stretch with containing view
        view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        
        
        addSubview(view)
    }
    
   func loadViewFromNib() -> UIView
    {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "checkoutMethodsView", bundle: bundle)
        
        // Assumes UIView is top level and only object in CustomView.xib file
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    
    
}
