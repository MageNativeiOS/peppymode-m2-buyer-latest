//
//  BrandTC.swift
//  MageNative Magento Platinum
//
//  Created by Hamza Usmani on 12/06/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import Foundation

class BrandTC: UITableViewCell {
    
    static var reuseId: String = "BrandTC"
    
    lazy var wrapperView:UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.mageSystemBackground
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 8.0
        view.layer.shadowColor = UIColor.darkGray.cgColor
        view.layer.shadowOffset = .zero
        view.layer.shadowOpacity = 0.5
        view.layer.shadowRadius = 2.0
        return view
    }()
    
    lazy var brandName:UILabel = {
        let label = UILabel()
        label.textColor = .darkGray
        label.font = .systemFont(ofSize: 18, weight: .semibold)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    lazy var brandLogo:UIImageView = {
        let imageView = UIImageView()
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    private func commonInit() {
        selectionStyle = .none
        backgroundColor = .clear
        
        addSubview(wrapperView)
        wrapperView.addSubview(brandName)
        wrapperView.addSubview(brandLogo)
        
        NSLayoutConstraint.activate([
            wrapperView.topAnchor.constraint(equalTo: topAnchor, constant: 8),
            wrapperView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8),
            wrapperView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8),
            wrapperView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -8),
            
            brandLogo.topAnchor.constraint(equalTo: wrapperView.topAnchor, constant: 8),
            brandLogo.trailingAnchor.constraint(equalTo: wrapperView.trailingAnchor, constant: -8),
            brandLogo.bottomAnchor.constraint(equalTo: wrapperView.bottomAnchor, constant: -8),
            brandLogo.widthAnchor.constraint(equalToConstant: 120),
            
            brandName.leadingAnchor.constraint(equalTo: wrapperView.leadingAnchor, constant: 8),
            brandName.trailingAnchor.constraint(equalTo: brandLogo.leadingAnchor, constant: -8),
            brandName.centerYAnchor.constraint(equalTo: wrapperView.centerYAnchor, constant: 0),
        ])
        
    }
    
    func populate(with brand: Brand?) {
        brandName.text = brand?.brand_name ?? " Name not available"
        brandLogo.sd_setImage(with: URL(string: brand?.small_image ?? ""), placeholderImage: UIImage(named: "placeholder"))
    }
    
    
    
}
