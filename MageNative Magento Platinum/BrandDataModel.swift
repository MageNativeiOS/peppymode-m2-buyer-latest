//
//  BrandDataModel.swift
//  MageNative Magento Platinum
//
//  Created by Hamza Usmani on 12/06/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import Foundation

struct BrandDataModel: Decodable {
    let brands: [Brand]?
    
    private enum CodingKeys: String,CodingKey {
        case brands = "brand_list"
    }
}

struct Brand: Decodable {
    let brand_name:String?
    let small_image:String?
    let brand_id:String?
}
