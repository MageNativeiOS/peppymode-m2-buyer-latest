//
//  BrandVC.swift
//  MageNative Magento Platinum
//
//  Created by Hamza Usmani on 12/06/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class BrandVC: MagenativeUIViewController {

    //MARK:- UIControls
    lazy var brandSearchBar:UISearchBar = {
        let searchBar = UISearchBar()
        searchBar.searchBarStyle = .minimal
        if #available(iOS 13.0, *) {
            searchBar.searchTextField.clearButtonMode = .never
        } else {
            // Fallback on earlier versions
        }
        searchBar.placeholder = "Search your Brands ... "
        searchBar.returnKeyType = .search
        searchBar.showsSearchResultsButton = true
        searchBar.setImage(UIImage(named: "searchGo"), for: .resultsList, state: .normal)
        searchBar.translatesAutoresizingMaskIntoConstraints = false
        searchBar.delegate = self
        return searchBar
    }()
    lazy var brandTable:UITableView = {
        let table = UITableView()
        table.separatorStyle = .none
        table.backgroundColor = .clear
        table.register(BrandTC.self, forCellReuseIdentifier: BrandTC.reuseId)
        table.delegate =  self
        table.dataSource =  self
        table.translatesAutoresizingMaskIntoConstraints = false
        return table
    }()
    
    //MARK:- Properties
    var model: BrandDataModel?
    var storeID: String?
    var searchKeyword: String? {
        didSet{
            self.sendRequest(url: "mobiconnect/getBrands", params: ["store_id":storeID ?? "0","keyword":searchKeyword  ?? ""])
        }
    }
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.mageSystemBackground
        setupSearchBar()
        setupBrandTable()
        sendApiRequest()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        title = "Brands"
    }
    //MARK:- Setup UIControls
    private func setupSearchBar() {
        view.addSubview(brandSearchBar)
        brandSearchBar.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        brandSearchBar.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        brandSearchBar.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
    }
    private func setupBrandTable() {
        view.addSubview(brandTable)
        brandTable.topAnchor.constraint(equalTo: brandSearchBar.bottomAnchor, constant: 8).isActive = true
        brandTable.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        brandTable.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        brandTable.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
    
    //MARK:- API Request and response
    private func sendApiRequest() {
        storeID = UserDefaults.standard.value(forKey: "storeId") as? String
        self.sendRequest(url: "mobiconnect/getBrands", params: ["store_id":storeID ?? "0"] )
    }
    override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
        guard let data = data else { print("error getting data"); return }
        do {
            let json     = try JSON(data: data)
            let finaData = try json[0].rawData(options: [])
            print(json)
            
            if json[0]["success"].boolValue {
                self.model = try JSONDecoder().decode(BrandDataModel.self, from: finaData)
                brandTable.reloadSections([0], with: .bottom)
            }
            
        } catch {
            print(error.localizedDescription)
        }
        
    }
    
}

//MARK:- UITableViewDataSource
extension BrandVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model?.brands?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: BrandTC.reuseId, for: indexPath) as! BrandTC
        cell.populate(with: model?.brands?[indexPath.row])
        return cell
    }
}

//MARK:- UITableViewDelegate
extension BrandVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = UIStoryboard(name: "categorylayouts", bundle: nil).instantiateViewController(withIdentifier: "cedMageDefaultCollection") as!  cedMageDefaultCollection
        vc.brandID = model?.brands?[indexPath.row].brand_id
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

//MARK:- UISearchBarDelegate
extension BrandVC: UISearchBarDelegate {
    
    func searchBarResultsListButtonClicked(_ searchBar: UISearchBar) {
        print(searchBar.text ?? "")
        searchKeyword = searchBar.text
    }
    
    
}






