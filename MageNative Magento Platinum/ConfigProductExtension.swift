/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */


import UIKit

extension ProductSinglePageViewController{
    
    func renderConfigurableProductSection(){
        
        let customOptionContainerStackView = UIStackView(); // outermost stackview
        customOptionContainerStackView.translatesAutoresizingMaskIntoConstraints = false;
        customOptionContainerStackView.axis  = NSLayoutConstraint.Axis.vertical;
        customOptionContainerStackView.distribution  = UIStackView.Distribution.equalSpacing;
        customOptionContainerStackView.alignment = UIStackView.Alignment.center;
        customOptionContainerStackView.spacing   = 0.0;
        stackView.addArrangedSubview(customOptionContainerStackView);
        stackView.addConstraint(NSLayoutConstraint(item: customOptionContainerStackView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: stackView, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: padding/2));
        stackView.addConstraint(NSLayoutConstraint(item: customOptionContainerStackView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: stackView, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: -padding/2));
        let backgroundViewForStackView = UIView(); // uiview for covering stackview
        backgroundViewForStackView.translatesAutoresizingMaskIntoConstraints = false;
        if #available(iOS 13.0, *) {
            backgroundViewForStackView.backgroundColor = UIColor.systemBackground
        } else {
            backgroundViewForStackView.backgroundColor = UIColor.white
        };
        //backgroundViewForStackView.makeCard(backgroundViewForStackView, cornerRadius: 2, color: UIColor.black, shadowOpacity: 0.4);
        customOptionContainerStackView.addArrangedSubview(backgroundViewForStackView); // adding uiview to stackview
        customOptionContainerStackView.addConstraint(NSLayoutConstraint(item: backgroundViewForStackView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: customOptionContainerStackView, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0));
        customOptionContainerStackView.addConstraint(NSLayoutConstraint(item: backgroundViewForStackView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: customOptionContainerStackView, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0));
        
        configOptionsStackView = UIStackView(); // main actual stackview that will contain all config-options
        configOptionsStackView.translatesAutoresizingMaskIntoConstraints = false;
        configOptionsStackView.axis  = NSLayoutConstraint.Axis.vertical;
        configOptionsStackView.distribution  = UIStackView.Distribution.equalSpacing;
        configOptionsStackView.alignment = UIStackView.Alignment.center;
        configOptionsStackView.spacing   = 10.0;
        backgroundViewForStackView.addSubview(configOptionsStackView); // adding stackview to uiview
        backgroundViewForStackView.addConstraint(NSLayoutConstraint(item: configOptionsStackView!, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: backgroundViewForStackView, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0));
        backgroundViewForStackView.addConstraint(NSLayoutConstraint(item: configOptionsStackView!, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: backgroundViewForStackView, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0));
        backgroundViewForStackView.addConstraint(NSLayoutConstraint(item: configOptionsStackView!, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: backgroundViewForStackView, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1, constant: 0));
        backgroundViewForStackView.addConstraint(NSLayoutConstraint(item: configOptionsStackView!, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: backgroundViewForStackView, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: 0));
        
        var heightOfCustomOption = CGFloat(0.0);
        
        //        MARK: THIS to done Because Dont find solotion for that
        
        
        let keysArray = Array(attributes.keys.reversed());
        print(attributes.count)
        for counter in 0  ..< attributes.count  {
            
            let customOptionDropDownView = CustomOptionDropDownView();
            customOptionDropDownView.dropDownButton.cardView()
            customOptionDropDownView.translatesAutoresizingMaskIntoConstraints = false;
            if let title = self.attributesCode[keysArray[counter]] {
                customOptionDropDownView.topLabel.text = title
            }
            customOptionDropDownView.dropDownButton.fontColorTool()
            customOptionDropDownView.dropDownButton.setTitle("-- Select --".localized, for: UIControl.State());
            customOptionDropDownView.dropDownButton.addTarget(self, action: #selector(ProductSinglePageViewController.showConfigAttributesOptionDropdown(_:)), for: UIControl.Event.touchUpInside);
            
            configOptionsStackView.addArrangedSubview(customOptionDropDownView);
            let customOptionDropDownViewHeight = translateAccordingToDevice(CGFloat(100.0));
            customOptionDropDownView.heightAnchor.constraint(equalToConstant: customOptionDropDownViewHeight).isActive = true;
            heightOfCustomOption = heightOfCustomOption+customOptionDropDownViewHeight;
            self.setLeadingAndTralingSpaceFormParentView(customOptionDropDownView,parentView:configOptionsStackView,padding:0);
            
            if(item_id == ""){
                if(counter > 0){
                    customOptionDropDownView.isHidden = true;
                }
            }
            else{
               
                customOptionDropDownView.dropDownButton.fontColorTool()
                customOptionDropDownView.dropDownButton.setTitle(self.previous_options_selected[keysArray[counter]], for: UIControl.State());
            }
        }
    
    }
    
    
    @objc func showConfigAttributesOptionDropdown(_ sender:UIButton){
        
        print(self.option_price.convtToJson())
        if let customOptionDropDownView = sender.superview?.superview as? CustomOptionDropDownView{
            self.isFixed=false
            var attributeName = customOptionDropDownView.topLabel.text!;
            if let key  = attributesCode.findKey(forValue: attributeName) {
                attributeName = key
            }
            print(attributeName)
            dropDown.anchorView = sender;
            dropDown.bottomOffset = CGPoint(x: 0, y:sender.bounds.height);
            var dataSource = [String]();
            //print(dataSource)
            if let stackView = customOptionDropDownView.superview as? UIStackView{
                if let index = stackView.arrangedSubviews.firstIndex(of: customOptionDropDownView){
                    if(index == 0){
                        if let array = config_attribute_array[attributeName]?.keys {
                            
                            var tempArray=Array(array);
                            tempArray = tempArray.sorted {$0.localizedStandardCompare($1) == .orderedAscending}
                            dataSource = tempArray
                        }
                    }
                    if(index > 0){
                        if let previousCustomOptionDropDownView = stackView.arrangedSubviews[index-1] as? CustomOptionDropDownView {
                            
                            var previousAttributeName = previousCustomOptionDropDownView.topLabel.text!;
                            if let key  = attributesCode.findKey(forValue: previousAttributeName) {
                                previousAttributeName = key
                            }
                            let previousSelectedAttribute = (previousCustomOptionDropDownView.dropDownButton.titleLabel?.text)!;
                            
                            print(previousSelectedAttribute);
                            print(previousAttributeName);
                            print(config_availability_array);
                            
                            if(config_availability_array[previousAttributeName]![previousSelectedAttribute]![attributeName] == nil){
                                let msg = "No Options Found".localized;
                                self.view.makeToast(msg, duration: 2.0, position: .bottom, title: nil, image: nil, style: nil, completion: nil);
                                return;
                            }
                            
                            let tempArr = config_availability_array[previousAttributeName]![previousSelectedAttribute]![attributeName]!;
                            
                            let tempArrKeys = Array(tempArr.values);
                            var attributeTerms = config_attribute_array[attributeName]!;
                            
                            for (key,val) in attributeTerms{
                                if(tempArrKeys.contains(val)){
                                    dataSource.append(key);
                                }
                            }
                            
                        }
                    }
                    dataSource = dataSource.sorted {$0.localizedStandardCompare($1) == .orderedAscending}
                    dropDown.dataSource = dataSource;
                }
            }
            
            
            dropDown.selectionAction = { [unowned self] (index, item) in
                sender.setTitle(item, for: UIControl.State.normal);
                sender.fontColorTool()
                print(item);
                
                
                let priceInfoArray = self.option_price[attributeName]![item]!;
                var basePrice = Float(self.productInfoArray["regular_price"]!);
                var priceToAdd = Float(0.0);
                print(priceInfoArray)
                print(basePrice)
                if(priceInfoArray["pricing-type"] == "percent"){
                    if(self.productInfoArray["special_price"] != ""){
                        basePrice = Float(self.productInfoArray["regular_price"]!);
                    }
                    priceToAdd = (basePrice!*(Float(priceInfoArray["option-price"]!)!))/(100);
                    self.isFixed=false
                    
                }
                else{
                    if(priceInfoArray["option-price"] != "no-value"){
                        priceToAdd = Float(priceInfoArray["option-price"]!)!;
                        print(Float(priceInfoArray["option-price"]!)!)
                        self.isFixed=true
                    }
                    else
                    {
                        self.isFixed=false
                        
                    }
                }
                self.priceCalculationArray[attributeName] = priceToAdd;
                print(self.priceCalculationArray)
                self.updateProductPrice();
                
                
                print("updateProductPrice :: Write code here");
                
                self.arrayToKeepTrackForAdditionalImage[attributeName] = item;
                self.fetchSelectedAttributeSpecificImage();
                
                if(!self.firstConfigAttributeSelected){
                    if let stackView = customOptionDropDownView.superview as? UIStackView{
                        for view in stackView.subviews{
                            view.isHidden = false;
                        }
                    }
                    self.firstConfigAttributeSelected = true;
                }
                
            }
            
            
            if dropDown.isHidden {
                let _ = dropDown.show();
            } else {
                dropDown.hide();
            }
            
        }
        
    }
    
    func fetchSelectedAttributeSpecificImage() {
        var attributes_to_send = "{\"attributes\":{";
        for (key,val) in self.arrayToKeepTrackForAdditionalImage {
            let keyToAdd = self.config_attribute_array[key]?[val];
            attributes_to_send += "\""+key.lowercased()+"\":"+"\""+keyToAdd!+"\",";
        }
        attributes_to_send = attributes_to_send.substring(to: attributes_to_send.index(before: attributes_to_send.endIndex));
        attributes_to_send += "}}";
        
        print("attributes_to_send");
        print(attributes_to_send);
        
        
        var urlToRequest = "mobiconnect/catalog/viewimage/";
        let requestHeader = cedMage.getInfoPlist(fileName:"cedMage",indexString: "requestheader") as! String
        let baseURL = cedMage.getInfoPlist(fileName:"cedMage",indexString: "cedBaseUrl") as! String;
        urlToRequest = baseURL+urlToRequest;
        
        var postString = "";
        let postData = ["attributes":attributes_to_send,"prodID":self.productInfoArray["product-id"]!];
        postString = ["parameters":postData].convtToJson() as String
        print(urlToRequest);
        print(postString);
        var request = URLRequest(url: URL(string: "\(urlToRequest)")!);
        request.httpMethod = "POST";
        request.httpBody = postString.data(using: String.Encoding.utf8);
        request.setValue(requestHeader, forHTTPHeaderField: "Mobiconnectheader")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        cedMageLoaders.addDefaultLoader(me: self);
        let task = URLSession.shared.dataTask(with: request){
            
            // check for fundamental networking error
            data, response, error in
            guard error == nil && data != nil else{
                print("error=\(error)")
                DispatchQueue.main.async{
                    print(error?.localizedDescription as Any);
                    cedMageLoaders.removeLoadingIndicator(me: self);
                }
                return;
            }
            
            // check for http errors
            if let httpStatus = response as? HTTPURLResponse , httpStatus.statusCode != 200{
                
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
                DispatchQueue.main.async{
                    cedMageLoaders.removeLoadingIndicator(me: self);
                    
                }
                return;
            }
            
            // code to fetch values from response :: start
            guard var jsonResponse = try? JSON(data: data!) else{return;}
            jsonResponse = jsonResponse[0]
            if(jsonResponse != nil) {
                DispatchQueue.main.async {
                    print(jsonResponse);
                    cedMageLoaders.removeLoadingIndicator(me: self);
                    
                    if(jsonResponse["data"]["success"].stringValue == "true") {
                        let url = jsonResponse["data"]["url"].stringValue;
                        print("url");
                        print(url);
                        var updatedProductImgsArray = [String]();
                        updatedProductImgsArray = self.productImgsArray;
                        updatedProductImgsArray.append(url);
                        self.scrollView.setContentOffset(
                            CGPoint(x: 0,y: -self.scrollView.contentInset.top),
                            animated: true)
                        self.productImageView.productImgsArray = updatedProductImgsArray;
                        self.scrollView.scrollsToTop = true
                        self.productImageView.productGalleryImagesCollectionView.reloadData();
                        let indexPath = NSIndexPath(item: updatedProductImgsArray.count-1, section: 0)
                        self.productImageView.pageControl.currentPage = updatedProductImgsArray.count
                        self.productImageView.productGalleryImagesCollectionView.scrollToItem(at: indexPath as IndexPath, at: UICollectionView.ScrollPosition.right, animated: true)
                    }
                }
            }
        }
        task.resume();
        
    }

}
extension Dictionary where Value: Equatable {
    func findKey(forValue val: Value) -> Key? {
        return first(where: { $1 == val })?.0
    }
}
